﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaFogSettings : MonoBehaviour 
{
	public float fogDensity = 0.0f;
	public bool fogEnabled = false;
	public Color fogColor = Color.white;

	// Use this for initialization
	void Start () 
	{
		RenderSettings.fog = fogEnabled;
		RenderSettings.fogDensity = fogDensity;
		RenderSettings.fogColor = fogColor;
	}
}
