﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class AudioManagerScript : MonoBehaviour 
{
	const float BGM_FADE_UPDATE_RATE = 0.003f;

	private AudioSource bgmSource;
	private AudioSource menuSource;
	private AudioSource attackSource;
	private AudioSource backupSource; // Used if a request is made of a source which is already in use

	float bgmFadeValue;
	AudioClip bgmNextClip;

	public static AudioManagerScript reference;

	void Awake()
	{
		if(reference == null)
		{
			reference = this;
			DontDestroyOnLoad(this);
		}
		else if(reference != this)
		{
			Destroy(gameObject);
		}

		bgmFadeValue = 1.0f;

		AudioSource[] sources = gameObject.GetComponentsInChildren<AudioSource> ();
		
		foreach(AudioSource source in sources)
		{
			if(source.name.Equals("BGM Source"))
			{
				bgmSource = source;
			}
			else if(source.name.Equals("Menu Source"))
			{
				menuSource = source;
			}
			else if(source.name.Equals("Attack Source"))
			{
				attackSource = source;
			}
			else if(source.name.Equals("Backup Source"))
			{
				backupSource = source;
			}
		}

		bgmSource.loop = true;
	}

	public void PlayMenuEffect(string name)
	{
		if(!menuSource.mute)
		{
			AudioClip clip = ResourceLoader.LoadMenuSFX(name);
			if(!CheckForRequestClash(menuSource, clip) && clip)
			{
				menuSource.loop = false;
				menuSource.clip = clip;
				menuSource.Play ();
			}
		}
	}

	public void PlayFieldEffect(string name)
	{
		AudioClip clip = ResourceLoader.LoadFieldSFX(name);
		if(!CheckForRequestClash(menuSource, clip) && clip)
		{
			menuSource.loop = false;
			menuSource.clip = clip;
			menuSource.Play ();
		}
	}

	public void PlayAudioClipEffect(AudioClip clip)
	{
		if(!CheckForRequestClash(menuSource, clip) && clip)
		{
			menuSource.loop = false;
			menuSource.clip = clip;
			menuSource.Play ();
		}
	}

	public void PlayLoopingMenuEffect(string name)
	{
		AudioClip clip = ResourceLoader.LoadMenuSFX(name);
		if(clip)
		{
			menuSource.loop = true;
			menuSource.clip = clip;
			menuSource.Play ();
		}
	}

	public void PlayAttackEffect(string name)
	{
		AudioClip clip = ResourceLoader.LoadBattleSFX(name);
		if(!CheckForRequestClash(attackSource, clip) && clip)
		{
			attackSource.clip = clip;
			attackSource.Play ();
		}
	}

	public void PlayAttackEffect(AudioClip clip)
	{
		if(!CheckForRequestClash(attackSource, clip) && clip)
		{
			attackSource.clip = clip;
			attackSource.Play ();
		}
	}

	public void PlayBGM(string name, bool useFade)
	{
		AudioClip clip = ResourceLoader.LoadAudioBGM(name);
		if(clip)
		{
			if(!clip.Equals(bgmSource.clip))
			{
				bgmSource.loop = true;

				if(!useFade)
				{
					bgmSource.clip = clip;
					bgmSource.Play ();
				}
				else
				{
					bgmNextClip = clip;
					InvokeRepeating("FadeBGMOut", 0 , BGM_FADE_UPDATE_RATE);
				}
			}
			else
			{
				Debug.Log("BGM (Audio) not played, as the same clip is already playing.");
			}
		}
		else
		{
			Debug.LogError("BGM (Audio) not found. Please check the name of the resource.");
		}
	}

	public void PlayBGMLoopWithIntro(string introName, string loopName)
	{
		AudioClip introClip = ResourceLoader.LoadAudioBGM(introName);
		if(introClip)
		{
			if(!introClip.Equals(bgmSource.clip))
			{
				bgmSource.loop = false;

				bgmSource.clip = introClip;
				bgmSource.Play ();
				bgmNextClip = ResourceLoader.LoadAudioBGM(loopName);
				InvokeRepeating("CheckForBGMIntroFinish", 0 , 0.0167f);
			}
			else
			{
				Debug.Log("BGM (Audio) not played, as the same clip is already playing.");
			}
		}
		else
		{
			Debug.LogError("BGM (Audio) not found. Please check the name of the resource.");
		}
	}

	public void PlayOneOffBGM(string name, bool useFade)
	{
		AudioClip clip = ResourceLoader.LoadAudioBGM(name);
		if(clip)
		{
			bgmSource.loop = false;

			if(!useFade)
			{
				bgmSource.clip = clip;
				bgmSource.Play ();
			}
			else
			{
				bgmNextClip = clip;
				InvokeRepeating("FadeBGMOut", 0 , BGM_FADE_UPDATE_RATE);
			}
		}
		else
		{
			Debug.LogError("One-off BGM (Audio) not found. Please check the name of the resource.");
		}
	}

	public void StopAllSFXLooping()
	{
		menuSource.loop = false;
		attackSource.loop = false;
		backupSource.loop = false;
	}

	private bool CheckForRequestClash(AudioSource requestedSource, AudioClip clip)
	{
		if(requestedSource.isPlaying)
		{
			if(clip && !requestedSource.clip.Equals(clip))
			{
				backupSource.clip = clip;
				backupSource.Play ();
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	private void CheckForBGMIntroFinish()
	{
		if (!bgmSource.isPlaying)
		{
			if(bgmNextClip)
			{
				if(!bgmNextClip.Equals(bgmSource.clip))
				{
					bgmSource.clip = bgmNextClip;
					bgmSource.loop = true;
					bgmSource.Play ();
					bgmNextClip = null;
				}
			}
		}
	}

	private void FadeBGMIn()
	{
		bgmFadeValue += Time.smoothDeltaTime;
		if(PlayerPrefs.GetInt("playBGM", 0) == 0)
		{
			bgmSource.volume = Mathf.SmoothStep (0.1f, 0.5f, bgmFadeValue);
		}

		if(bgmFadeValue >= 1.0f)
		{
			bgmFadeValue = 1.0f;
			bgmNextClip = null;
			CancelInvoke("FadeBGMIn");
		}
	}

	private void FadeBGMOut()
	{
		bgmFadeValue -= Time.smoothDeltaTime;
		if(PlayerPrefs.GetInt("playBGM", 0) == 0)
		{
			bgmSource.volume = Mathf.SmoothStep (0.1f, 0.5f, bgmFadeValue);
		}

		if(bgmFadeValue <= 0.0f)
		{
			bgmFadeValue = 0.0f;

			if(bgmNextClip)
			{
				if(!bgmNextClip.Equals(bgmSource.clip))
				{
					bgmSource.clip = bgmNextClip;
					bgmSource.Play ();
					InvokeRepeating("FadeBGMIn", 0, BGM_FADE_UPDATE_RATE);
					bgmNextClip = null;
				}
			}
			CancelInvoke("FadeBGMOut");
		}
	}

	public void SetMenuSourceOn(bool isOn)
	{
		menuSource.mute = !isOn;
	}

	public void SetBGMSourceLoopingState(bool state)
	{
		bgmSource.loop = state;
	}

	public bool GetBGMFinishedState()
	{
		return !bgmSource.isPlaying;
	}

	public AudioSource GetBGMSource()
	{
		return bgmSource;
	}
}
