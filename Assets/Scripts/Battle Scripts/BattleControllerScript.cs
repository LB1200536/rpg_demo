﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Linq;

public class BattleControllerScript : MonoBehaviour 
{
	private Vector3 CHARACTER_1_START_POSITION = new Vector3(-3.0f, 0.0f, -3.5f);
	private Vector3 CHARACTER_2_START_POSITION = new Vector3(-1.0f, 0.0f, -4.0f);
	private Vector3 CHARACTER_3_START_POSITION = new Vector3(1.0f, 0.0f, -4.5f);
	private Vector3 CHARACTER_4_START_POSITION = new Vector3(3.0f, 0.0f, -5.0f);
	
	private Vector3 BOSS_ENEMY_START_POSITION = new Vector3(0.0f, 0.0f, 5.0f);
	
	private Vector3 ENEMY_1_START_POSITION = new Vector3(3.0f, 0.0f, 3.5f);
	private Vector3 ENEMY_2_START_POSITION = new Vector3(0.0f, 0.0f, 3.5f);
	private Vector3 ENEMY_3_START_POSITION = new Vector3(-3.0f, 0.0f, 3.5f);
	
	public const int ENEMY_SPAWN_MAX = 3;
	public const int PARTY_SIZE_MAX = 4;
	private const float DELAY_BETWEEN_ACTIONS = 2.0f;
	private const float DELAY_BEFORE_FIRST_ACTION = 1.5f;

	public float BATTLE_SPEED;

	public GameObject currentPlayerIndicator;
	private GameObject battlefield;

	public int enemySpawnCount;

	List<BattleAction> actionList;
	public List<CharacterScript> playerCharacters;
	public List<EnemyScript> enemies;
	private int[] enemyIds;
	List<int> readyCharacters; // Stores character ids
	private int currentCharacter = -1;

	// State flags // 
	public bool battleFlow = false;
	public bool battleComplete = false;
	private bool choosingTarget = false;
	private bool targetEnemies = true;
	public int deadCharacterCount = 0;
	public int deadEnemyCount = 0;
	private bool isRunnable;
	private bool isBossBattle = false;

	public GameObject battleUI;
	private Canvas commandMenu;
	private Canvas characterDisplay;
	private Canvas enemyDisplay;
	private Canvas skillDisplay;
	private Canvas itemDisplay;
	public Canvas infoPopUp;
	private Button nextButton;
	private Button backButton;
	private Text skillButtonText;

	public static BattleControllerScript reference;

	Thread characterUpdateThread;
	Thread enemyUpdateThread;

	GameObject winningsTransferObject;
	GameObject stateTransferObject;

	BattleEntranceType entranceType;

	void Awake()
	{
		if(!reference)
		{
			reference = this;
		}
		else if(reference != this)
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		currentPlayerIndicator.SendMessage("Disable");
		BATTLE_SPEED = PlayerPrefs.GetFloat ("battleSpeed", 0.075f);
		actionList = new List<BattleAction> ();
		playerCharacters = new List<CharacterScript> ();
		enemies = new List<EnemyScript> ();
		readyCharacters = new List<int> ();

		GenerateBattle ();
		InitialiseCanvases ();
		characterDisplay.gameObject.SetActive (false);

		winningsTransferObject = new GameObject ();
		winningsTransferObject.AddComponent<BattleToWinningsDataHolder> ();
		winningsTransferObject.name = "BattleEarnings";

		stateTransferObject = new GameObject ();
		stateTransferObject.AddComponent<BattleToFieldDataHolder> ();
		stateTransferObject.name = "LastBattleState";

		int[] enemyIds = new int[enemies.Count];
		for(int i = 0; i < enemies.Count; i++)
		{
			enemyIds[i] = enemies[i].id;
		}
		stateTransferObject.GetComponent<BattleToFieldDataHolder> ().SetEnemyIds (enemyIds);

		//particleLoader = new AssetBundleLoader ();
		//particleLoader.Initialize ();

		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "BeginCamera", this.gameObject);
	}

	void BeginCamera()
	{
		CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.005f);

		switch(entranceType)
		{
		case BattleEntranceType.PreEmptive:
			CameraManagerScript.reference.SetArrivalMethod (this.gameObject, "StartPreEmptiveBattle");
			break;
		case BattleEntranceType.SurpriseAttack:
			CameraManagerScript.reference.SetArrivalMethod (this.gameObject, "StartSurpriseAttackBattle");
			break;
		case BattleEntranceType.Normal:
			CameraManagerScript.reference.SetArrivalMethod (this.gameObject, "StartNormalBattle");
			break;
		}
	}

	void OnApplicationQuit() 
	{
		// Allows threads to end even if the application is closed prematurely
		battleComplete = true;
		//BattleAssetBundleLoader.reference.Unload ();
		DatabaseHandler.CloseAllDatabases();
	}
	
	void StartNormalBattle()
	{
		characterDisplay.gameObject.SetActive (true);
		battleFlow = true;
		StartUpdateThreads ();
	}

	void StartPreEmptiveBattle()
	{
		infoPopUp.SendMessage ("StartDisplay", "PreEmptive Strike!");
		for(int i = 0; i < playerCharacters.Count; i++)
		{
			playerCharacters[i].SetTurnTime(98);
		}
		for(int j = 0; j < enemies.Count; j++)
		{
			enemies[j].SetTurnTime(0);
		}
		Invoke ("StartNormalBattle", 1.0f);
	}

	void StartSurpriseAttackBattle()
	{
		infoPopUp.SendMessage ("StartDisplay", "Surprise Attack...");
		for(int i = 0; i < playerCharacters.Count; i++)
		{
			playerCharacters[i].SetTurnTime(0);
		}
		for(int j = 0; j < enemies.Count; j++)
		{
			enemies[j].SetTurnTime(95);
		}
		Invoke ("StartNormalBattle", 1.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(battleFlow)
		{
			UpdateUI();

			// Check for a new ready character to select
			if(readyCharacters.Count > 0 && currentCharacter == -1)
			{
				ToNextReadyCharacter();
			}

			if(readyCharacters.Count > 1)
			{
				if(!nextButton.IsActive())
				{
					EnableNextButton(true);
				}
			}
			else
			{
				if(nextButton.IsActive())
				{
					EnableNextButton(false);
					currentPlayerIndicator.SendMessage("Disable");
				}
			}
		}
	}
	
	void StartUpdateThreads()
	{
		characterUpdateThread = new Thread (UpdateCharacters);
		characterUpdateThread.Start ();

		enemyUpdateThread = new Thread (UpdateEnemies);
		enemyUpdateThread.Start ();
	}

	void UpdateUI()
	{
		if(choosingTarget && !enemyDisplay.enabled)
		{
			enemyDisplay.enabled = true;
		}
	}

	public void AddEnemyToDeadCount(EnemyScript enemy)
	{
		deadEnemyCount++;
		winningsTransferObject.GetComponent<BattleToWinningsDataHolder>().AddExperienceEarned (enemy.GetTotalExperiencePoints ());
		winningsTransferObject.GetComponent<BattleToWinningsDataHolder>().AddGoldEarned (enemy.GetGoldDrop ());

		if(enemy.GetItemDrop() != null)
		{
			if(Random.Range(0, 100) <= enemy.GetItemDropRate())
			{
				winningsTransferObject.GetComponent<BattleToWinningsDataHolder>().AddItemToEarnings (enemy.GetItemDrop ());
			}
		}

		ValidateActionTargets (enemy);

		if(enemyDisplay.enabled)
		{
			SetTargetButtons();
		}

		if(deadEnemyCount == enemySpawnCount)
		{
			actionList = new List<BattleAction>();
			CancelInvoke();

			BattleVictory();
		}
	}

	void BattleVictory()
	{
		if(battleFlow == true)
		{
			currentPlayerIndicator.SendMessage("Disable");
			battleFlow = false;
			
			commandMenu.enabled = false;
			enemyDisplay.enabled = false;
			skillDisplay.enabled = false;
			characterDisplay.gameObject.SetActive (false);
			
			foreach(CharacterScript character in playerCharacters)
			{
				character.SetSelected(false);
				character.SetVictorious();
			}

			if(isBossBattle)
			{
				// Set the boss to defeated in the current game state
				foreach (SerializableArea serializableArea in PartyDataHolderScript.reference.GetCurrentGameStateData().alteredAreaList)
				{
					if (serializableArea.areaId == PartyDataHolderScript.reference.areaId)
					{
						SerializableBattleEncounter encounter = serializableArea.battleEncounters.Find (x => x.enemyIds.Equals (enemyIds));
						if (encounter != null)
						{
							encounter.hasBeenDefeated = true;
							stateTransferObject.GetComponent<BattleToFieldDataHolder>().SetBossDefeated(true);
						}
						break;
					}
				}
			}
			
			battleComplete = true;
			//AudioManagerScript.reference.SetBGMSourceLoopingState(false);
			AudioManagerScript.reference.PlayBGMLoopWithIntro ("BattleComplete", "WinTheme");
			//AudioManagerScript.reference.PlayOneOffBGM("BattleComplete", false);
			CameraManagerScript.reference.SetDefaultCameraLocation(0);
			CameraManagerScript.reference.SetDelay(0);
			CameraManagerScript.reference.SetDelayOnArrival(120.0f);
			CameraManagerScript.reference.SetDefaultTargetLocation(1, 0.005f);
			PartyDataHolderScript.reference.UpdateParty(playerCharacters);

			Invoke("BeginWinFadeout", 3.128f);
		}
	}

	void BattleFled()
	{
		currentPlayerIndicator.SendMessage("Disable");
		battleFlow = false;
		
		commandMenu.enabled = false;
		enemyDisplay.enabled = false;
		skillDisplay.enabled = false;
		characterDisplay.gameObject.SetActive (false);

		battleComplete = true;

		CameraManagerScript.reference.SetDefaultCameraLocation(0);
		CameraManagerScript.reference.SetDelay(0);
		CameraManagerScript.reference.SetDelayOnArrival(120.0f);
		CameraManagerScript.reference.SetDefaultTargetLocation(1, 0.005f);
		PartyDataHolderScript.reference.UpdateParty(playerCharacters);

		Invoke("BeginWinFadeout", 3.128f);
	}

	void ValidateActionTargets(BaseBattleCharacter target)
	{
		for(int i = 0; i < actionList.Count; i++)
		{
			BattleAction action = actionList[i];

			if(action.GetTargets().Contains(target.gameObject))
			{
				action.GetTargets().Remove(target.gameObject);

				if(action.GetTargets().Count == 0)
				{
					if(target.GetComponent<EnemyScript>() && enemies.Count >= 1)
					{
						if(enemies.Count >= 1)
						{
							action.SetTarget(enemies[0].gameObject);
						}
					}
					else if(target.GetComponent<CharacterScript>())
					{
						if(action.GetUser().GetComponent<CharacterScript>())
						{
							action.SetTarget(action.GetUser());
						}
						else
						{
							bool newTargetChosen = false;
							// Ensure there are still players to choose from
							if(deadCharacterCount != playerCharacters.Count)
							{
								while(!newTargetChosen)
								{
									int newTarget = Random.Range(0, 4);
									if(!playerCharacters[newTarget].GetDead())
									{
										actionList[0].SetTarget(playerCharacters[newTarget].gameObject);
										newTargetChosen = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void BeginWinFadeout()
	{
		FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black, "LoadWinScenario", this.gameObject);
		//BattleAssetBundleLoader.reference.Unload ();
	}

	void LoadWinScenario()
	{
		SceneManager.LoadScene ("end_scene_win");
	}

	public void AddCharacterToDeadCount(CharacterScript character)
	{
		deadCharacterCount++;

		if(actionList.Contains(character.GetAction()))
		{
			actionList.Remove(character.GetAction());
		}

		if(character.selected)
		{
			character.selected = false;
			ToNextReadyCharacter();
		}

		if(readyCharacters.Contains(character.GetId()))
		{
			readyCharacters.Remove(character.GetId());
		}

		if(deadCharacterCount == playerCharacters.Count)
		{
			// FIXME: Add camera sweep of battle before moving to the lose screen
			battleComplete = true;
			actionList = new List<BattleAction>();
			CancelInvoke();
			SceneManager.LoadScene ("end_scene_lose");
		}
		else
		{
			ValidateActionTargets(character);
		}
	}

	public void RemoveCharacterFromDeadCount()
	{
		deadCharacterCount--;
	}

	public void StartAction()
	{
		if(battleFlow && actionList.Count > 0)
		{
			if(actionList[0].GetActionType() != ActionType.Run)
			{
				if(actionList[0].GetTargets().Count >= 1)
				{
					actionList [0].GetUser ().GetComponent<BaseBattleCharacter> ().StartAction (actionList [0]);
				}
				
				if(actionList[0].GetActionType() == ActionType.Skill)
				{
					infoPopUp.SendMessage("StartDisplay", actionList [0].GetSkill().GetName());
				}
				
				CameraManagerScript.reference.SetArrivalMethod (actionList [0].GetTargets (), "ReactToAction");
			}
			else // Flee action
			{
				battleFlow = false;
				AudioManagerScript.reference.PlayAttackEffect(actionList[0].GetSoundEffect());

				if(actionList[0].GetUser().GetComponent<EnemyScript>())
				{
					CameraManagerScript.reference.SetCurrentPositionWithLookat (new Vector3(0.0f, 5.0f, 0.0f), actionList[0].GetUser().transform);
					CameraManagerScript.reference.SetDelay (20.0f);
					CameraManagerScript.reference.SetDefaultTargetLocation (0, 0.01f);
					CameraManagerScript.reference.SetArrivalMethod (BattleControllerScript.reference.gameObject, "EndAction");
					actionList[0].GetUser().SendMessage("BeginFleeAnimation");
					infoPopUp.SendMessage("StartDisplay", actionList[0].GetUser().GetComponent<EnemyScript>().GetName() + " has fled.");
				}
				else if(actionList[0].GetUser().GetComponent<CharacterScript>())
				{
					if(actionList[0].GetUser().GetComponent<CharacterScript>().CalculateFleeAttempt())
					{
						infoPopUp.SendMessage("StartDisplay", "Fled from battle!");
						foreach(CharacterScript character in playerCharacters)
						{
							if(character.GetTargetHP() > 0)
							{
								character.SendMessage("BeginFleeAnimation");
							}
						}
						BattleFled();
					}
					else
					{
						infoPopUp.SendMessage("StartDisplay", "Could not run...");
						actionList[0].GetUser().SendMessage("BeginEmptyAction", SendMessageOptions.DontRequireReceiver);
						EndAction();
					}
				}
			}
		}
		else // Wait until the battle is flowing again
		{
			Invoke("StartAction", DELAY_BETWEEN_ACTIONS);
		}
	}

	void EndAction()
	{
		if(actionList.Count > 0)
		{
			battleFlow = true;
			actionList.Remove (actionList[0]);
			
			if(!battleComplete && actionList.Count > 0)
			{
				// Small delay between the end of one action, and the start of another
				Invoke ("StartAction", DELAY_BETWEEN_ACTIONS);
			}
		}
	}

	public void EnemyFled(EnemyScript enemy)
	{
		RemoveDeadEnemyActionFromList(enemy);
		enemies.Remove(enemy);
		deadEnemyCount++;

		if(enemies.Count == 0)
		{
			BattleVictory();
		}
	}

	public void RemoveDeadEnemyActionFromList(EnemyScript enemy)
	{
		if(actionList.Contains(enemy.GetAction()))
		{
			actionList.Remove(enemy.GetAction());
		}
		enemies.Remove (enemy);

		if(enemies.Count == 0 ||
		   enemies.Count == 1 && enemies[0].IsBoss())
		{
			actionList = new List<BattleAction>();
			commandMenu.enabled = false;
		}
	}

	void UpdateCharacters()
	{
		while(!battleComplete)
		{
			if(battleFlow)
			{
				foreach(CharacterScript character in playerCharacters)
				{
					character.UpdateTurn();
				}

				Thread.Sleep(50);
			}
		}
	}

	void UpdateEnemies()
	{
		while(!battleComplete)
		{
			if(battleFlow)
			{
				foreach(EnemyScript enemy in enemies)
				{
					enemy.UpdateTurn();
				}
				
				Thread.Sleep(50);
			}
		}
	}

	public void AddEnemyActionToList(BattleAction action)
	{
		//action.SetTarget(playerCharacters[action.GetUser().GetComponent<EnemyScript>().GetCurrentTarget()].gameObject);
		actionList.Add (action);
		action.GetUser ().GetComponent<EnemyScript> ().SetAction (action);
		if(actionList.Count == 1)
		{
			Invoke ("StartAction", DELAY_BEFORE_FIRST_ACTION);
		}
	}

	void AddCharacterActionToList(int character)
	{
		if (currentCharacter == character)
		{
			battleFlow = true;
			AudioManagerScript.reference.PlayMenuEffect("MenuConfirm");
			enemyDisplay.enabled = false;
			commandMenu.enabled = false;
			choosingTarget = false;
			EnableBackButton (false);

			playerCharacters [character].SetSelected(false);
			currentPlayerIndicator.SendMessage("Disable");
			readyCharacters.Remove(character);
			currentCharacter = -1;
		}

		actionList.Add (playerCharacters [character].GetAction ());
		playerCharacters [character].SetReady (false);

		if(actionList.Count == 1)
		{
			Invoke ("StartAction", DELAY_BEFORE_FIRST_ACTION);
		}
	}

	public void ToNextReadyCharacter()
	{
		if(readyCharacters.Count > 0)
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuButton");

			if(currentCharacter != -1)
			{
				playerCharacters[currentCharacter].SetSelected(false);
			}

			// Swap current character for next in the ready list
			if(readyCharacters.Count > 1)
			{
				int temp = readyCharacters[0];
				readyCharacters.RemoveAt(0);
				currentCharacter = readyCharacters[0];
				readyCharacters.Add(temp);
			}
			else
			{
				currentCharacter = readyCharacters[0];
			}

			playerCharacters[currentCharacter].SetSelected(true);
			skillButtonText.text = playerCharacters[currentCharacter].GetSkillType().ToString();
			currentPlayerIndicator.SendMessage("Enable", playerCharacters[currentCharacter].gameObject.transform);
			commandMenu.enabled = true;
		}
	}

	public void StartCharacterAttack()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		choosingTarget = true;
		targetEnemies = true;
		//openItemList = false;
		//openSkillList = false;
		EnableNextButton (false);
		EnableBackButton (true);

		ToggleBattleFlow (false);

		SetButtonsWithContent (enemyDisplay);
		enemyDisplay.enabled = true;
		enemyDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
		skillDisplay.enabled = false;
		skillDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
		itemDisplay.enabled = false;
		itemDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);

		BattleAction action = new BattleAction (playerCharacters[currentCharacter].gameObject);

		string assetFilename = playerCharacters [currentCharacter].GetCurrentWeapon ().GetWeaponType ().ToString() + "Attack";
		#if UNITY_ANDROID
			action.SetSoundEffect (ResourceLoader.LoadBattleSFX (assetFilename));
			action.SetParticleEffect (ResourceLoader.LoadAttackParticles (assetFilename));
		#else
			action.SetSoundEffect (BattleAssetBundleLoader.reference.LoadSoundEffectAsync (assetFilename));
			action.SetParticleEffect (BattleAssetBundleLoader.reference.LoadParticlesAsync (assetFilename));
		#endif

		playerCharacters[currentCharacter].SetAction (action);
		playerCharacters[currentCharacter].SetActionType (ActionType.Attack);
	}

	public void StartCharacterSkill(int index)
	{
		if(playerCharacters[currentCharacter].GetSkillList()[index].GetMPCost() <= playerCharacters[currentCharacter].GetCurrentMP())
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuButton");

			BattleAction action = new BattleAction (playerCharacters[currentCharacter].gameObject);

			string assetFilename = playerCharacters [currentCharacter].GetSkillList () [index].GetName ();
			#if UNITY_ANDROID
				action.SetSoundEffect (ResourceLoader.LoadBattleSFX (assetFilename));
				action.SetParticleEffect (ResourceLoader.LoadAttackParticles (assetFilename));
			#else
				action.SetSoundEffect (BattleAssetBundleLoader.reference.LoadSoundEffectAsync (assetFilename));
				action.SetParticleEffect (BattleAssetBundleLoader.reference.LoadParticlesAsync (assetFilename));
			#endif
				
			playerCharacters[currentCharacter].SetAction (action);
			playerCharacters[currentCharacter].SetActionType (ActionType.Skill);
			playerCharacters[currentCharacter].GetAction().SetSkill(playerCharacters[currentCharacter].GetSkillList()[index]);
			
			choosingTarget = true;
			ToggleBattleFlow(false);
	
			skillDisplay.enabled = false;
			skillDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
			//openSkillList = false;
			
			if(playerCharacters[currentCharacter].GetAction().GetSkill().GetElement() == Element.Heal)
			{
				if(targetEnemies)
				{
					SwapTarget();
				}
			}
			else
			{
				if(!targetEnemies)
				{
					SwapTarget();
				}
			}

			SetButtonsWithContent (enemyDisplay);
			enemyDisplay.enabled = true;
			enemyDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}
	
	public void StartCharacterItem(int index)
	{
		Item chosenItem = itemDisplay.GetComponentsInChildren<ItemBattleMenuButtonScript> () [index].GetAssociatedItem();

		if(chosenItem != null && chosenItem.GetUsable())
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
			choosingTarget = true;
			EnableNextButton (false);
			EnableBackButton (true);
			ToggleBattleFlow(false);
			
			BattleAction action = new BattleAction (playerCharacters[currentCharacter].gameObject);
			action.SetSoundEffect (ResourceLoader.LoadBattleSFX (chosenItem.GetName()));
			action.SetParticleEffect (ResourceLoader.LoadAttackParticles (chosenItem.GetName()));
			
			playerCharacters[currentCharacter].SetAction (action);
			playerCharacters[currentCharacter].SetActionType (ActionType.Item);
			playerCharacters [currentCharacter].GetAction ().SetItem (chosenItem);
			
			if(playerCharacters[currentCharacter].GetAction().GetItem().GetItemType() == ItemType.HP_Healing
			   || playerCharacters[currentCharacter].GetAction().GetItem().GetItemType() == ItemType.MP_Healing)
			{
				if(targetEnemies)
				{
					SwapTarget();
				}
			}
			else
			{
				if(!targetEnemies)
				{
					SwapTarget();
				}
			}
			
			SetButtonsWithContent (enemyDisplay);
			enemyDisplay.enabled = true;
			enemyDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
			itemDisplay.enabled = false;
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuError");
		}
	}

	public void StartCharacterRun()
	{
		if (isRunnable) {
			//FIXME: Perform calculation to determine if the flee attempt is successful
			BattleAction action = new BattleAction (playerCharacters [currentCharacter].gameObject);
			playerCharacters [currentCharacter].GetAction ().SetActionType (ActionType.Run);
			
			AddCharacterActionToList (currentCharacter);
		} 
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
			infoPopUp.SendMessage("StartDisplay", "Cannot run from this fight!");
		}
	}

	public void SetActionTarget(int buttonId)
	{
		if(targetEnemies)
		{
			if(buttonId < enemies.Count)
			{
				playerCharacters[currentCharacter].SetActionTarget(enemies[buttonId].gameObject);
				AddCharacterActionToList(currentCharacter);
			}
			else
			{
				AudioManagerScript.reference.PlayMenuEffect("MenuError");
			}
		}
		else
		{
			if(buttonId < playerCharacters.Count)
			{
				if(playerCharacters[buttonId].GetTargetHP() > 0)
				{
					playerCharacters[currentCharacter].SetActionTarget(playerCharacters[buttonId].gameObject);
					AddCharacterActionToList(currentCharacter);
				}
				else if(playerCharacters[buttonId].GetTargetHP() <= 0)
				{
					bool newTargetFound = false;
					while(!newTargetFound)
					{
						int newTarget = Random.Range (0, playerCharacters.Count);
						if(playerCharacters[newTarget].GetTargetHP() > 0)
						{
							playerCharacters[currentCharacter].SetActionTarget(playerCharacters[buttonId].gameObject);
							newTargetFound = true;
						}
					}

					AddCharacterActionToList(currentCharacter);
				}
			}
			else
			{
				AudioManagerScript.reference.PlayMenuEffect("MenuError");
			}
		}
	}
	
	public void SetActionTargetAll()
	{
		if(playerCharacters[currentCharacter].GetAction().GetActionType() == ActionType.Skill
		   && (playerCharacters[currentCharacter].GetAction().GetSkill().GetTargetType().ToString().Contains("Group")
		    || playerCharacters[currentCharacter].GetAction().GetSkill().GetTargetType().ToString().Contains("Both")))
		{
			List<GameObject> targetList = new List<GameObject> ();
			if(targetEnemies)
			{
				foreach(EnemyScript enemy in enemies)
				{
					if(enemy.GetCurrentHP() > 0)
					{
						targetList.Add(enemy.gameObject);
					}
				}
			}
			else
			{
				foreach(CharacterScript character in playerCharacters)
				{
					if(character.GetTargetHP() > 0)
					{
						targetList.Add(character.gameObject);
					}
				}
			}
			
			playerCharacters [currentCharacter].SetActionTargetList (targetList);
			AddCharacterActionToList(currentCharacter);
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public void SwapTarget()
	{
		if(playerCharacters[currentCharacter].GetAction().GetActionType() != ActionType.Skill
		   || (playerCharacters[currentCharacter].GetAction().GetActionType() == ActionType.Skill
		   && playerCharacters[currentCharacter].GetAction().GetSkill().GetTargetType().ToString().Contains("Toggle")))
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			if(targetEnemies)
			{
				targetEnemies = false;
			}
			else
			{
				targetEnemies = true;
			}
			
			SetButtonsWithContent (enemyDisplay);
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	void GenerateBattle()
	{
		FieldToBattleDataHolder battleData = GameObject.Find ("BattleData").GetComponent<FieldToBattleDataHolder>();
		entranceType = battleData.GetBattleEntranceType ();
		isRunnable = battleData.GetIsRunnable ();

		battlefield = ResourceLoader.LoadBattlefield (battleData.GetBattlefieldName ());
		battlefield = (GameObject)Instantiate(battlefield, battlefield.transform.position, battlefield.transform.rotation);

		if(AudioManagerScript.reference)
		{
			AudioManagerScript.reference.PlayBGM (battleData.GetBGMName(), true);
		}
		
		enemyIds = battleData.GetEnemyIds ();

		GenerateEnemies ();
		GenerateCharacters ();
	}

	public void ActivateSecondaryMenu(Canvas menuDisplay)
	{
		// Turn off battle flow
		ToggleBattleFlow (false);

		// Play the menu sound effect
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");

		// Set the extra buttons for the menu
		EnableNextButton (false);
		EnableBackButton (true);

		// Turn off the command canvas
		commandMenu.enabled = false;
		commandMenu.GetComponentInChildren<Animator>().SetBool("Open", false);

		// Turn off all the secondary canvases 
		enemyDisplay.enabled = false;
		enemyDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
		itemDisplay.enabled = false;
		itemDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
		skillDisplay.enabled = false;
		skillDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);

		// Turn on the desired canvas
		SetButtonsWithContent (menuDisplay);
		menuDisplay.enabled = true;
		menuDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
	}

	void InitialiseCanvases()
	{
		Canvas[] uiMenus = battleUI.GetComponentsInChildren<Canvas> ();
		
		foreach(Canvas canvas in uiMenus)
		{
			if(canvas.CompareTag("UI_Commands"))
			{
				commandMenu = canvas;
				commandMenu.enabled = false;
				
				Button[] tempList = commandMenu.GetComponentsInChildren<Button>();
				
				foreach(Button temp in tempList)
				{
					if(temp.name.Equals("Next"))
					{
						nextButton = temp;
					}
					else if(temp.name.Equals("Back"))
					{
						backButton = temp;
					}
				}
				
				EnableNextButton(false);
				EnableBackButton(false);
				skillButtonText = commandMenu.GetComponentsInChildren<Text> () [1];
			}
			else if(canvas.CompareTag("UI_CharacterDisplay"))
			{
				characterDisplay = canvas;
				//characterDisplay.gameObject.SetActive (false);
			}
			else if(canvas.name.Equals("EnemyDisplay"))
			{
				enemyDisplay = canvas;
				enemyDisplay.enabled = false;
			}
			else if(canvas.name.Equals("SkillDisplay"))
			{
				skillDisplay = canvas;
				skillDisplay.enabled = false;
			}
			else if(canvas.name.Equals("ItemDisplay"))
			{
				itemDisplay = canvas;
				itemDisplay.enabled = false;
			}
		}
	}

	private void AddCharacterToReadyList(int id)
	{
		readyCharacters.Add (id);

		if(readyCharacters.Count == 1)
		{
			ToNextReadyCharacter();

			AudioManagerScript.reference.PlayMenuEffect("MenuConfirm");
			currentCharacter = id;
			playerCharacters[currentCharacter].SetSelected(true);
			currentPlayerIndicator.SendMessage("Enable", playerCharacters[currentCharacter].gameObject.transform);
		}
	}

	private void EnableNextButton(bool enable)
	{
		nextButton.gameObject.SetActive (enable);
	}
	
	private void EnableBackButton(bool enable)
	{
		//backButton.gameObject.SetActive (enable);
	}

	// Called by 'back' button on UI
	public void CancelCurrentMenu()
	{
		commandMenu.enabled = true;
		commandMenu.GetComponentInChildren<Animator> ().SetBool ("Open", true);

		if(enemyDisplay.enabled)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
			choosingTarget = false;
			enemyDisplay.enabled = false;
			enemyDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
			
			if(playerCharacters[currentCharacter].GetAction().GetActionType() == ActionType.Skill)
			{
				battleFlow = false;
				skillDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
				skillDisplay.enabled = true;
			}
			else if(playerCharacters[currentCharacter].GetAction().GetActionType() == ActionType.Item)
			{
				battleFlow = false;
				itemDisplay.GetComponentInChildren<Animator>().SetBool("Open", true);
				itemDisplay.enabled = true;
			}
			else
			{
				EnableBackButton(false);
				battleFlow = true;
			}
			
			playerCharacters[currentCharacter].SetAction(new BattleAction(playerCharacters[currentCharacter].gameObject));
		}
		else if(skillDisplay.enabled)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
			skillDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
			skillDisplay.enabled = false;
			battleFlow = true;
			EnableBackButton(false);
		}
		else if(itemDisplay.enabled)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
			itemDisplay.GetComponentInChildren<Animator>().SetBool("Open", false);
			itemDisplay.enabled = false;
			battleFlow = true;
			EnableBackButton(false);
		}
	}

	private void SetTargetButtons()
	{
		BattleAction action = playerCharacters [currentCharacter].GetAction ();

		if(action.GetActionType() == ActionType.Skill)
		{
			if(action.GetSkill().GetTargetType().ToString().Contains("Other"))
			{
				targetEnemies = true;
			}
			else if(action.GetSkill().GetTargetType().ToString().Equals("Self") ||
			        action.GetSkill().GetTargetType().ToString().Contains("Same"))
			{
				targetEnemies = false;
			}
		}

		Button[] buttonList = enemyDisplay.GetComponentsInChildren<Button>();
		foreach(Button button in buttonList)
		{
			if(button.CompareTag("Untagged"))
			{
				button.enabled = false;
				button.GetComponentInChildren<Text>().text = "";
				button.GetComponentInChildren<Text>().enabled = false;
			}
		}

		List<BaseBattleCharacter> targets = new List<BaseBattleCharacter>();
		if(!targetEnemies)
		{
			targets = playerCharacters.Cast<BaseBattleCharacter> ().ToList();
		}
		else if(targetEnemies)
		{
			targets = enemies.Cast<BaseBattleCharacter> ().ToList();
		}

		for(int i = 0; i < targets.Count; i++)
		{
			foreach(Button button in buttonList)
			{
				if(button.name.Equals(i.ToString()) && button.CompareTag("Untagged"))
				{
					if(!targets[i].GetDead())
					{
						if(action.GetActionType() == ActionType.Skill &&
						   action.GetSkill().GetTargetType().ToString().Equals("Self"))
						{
							if(i == currentCharacter)
							{
								button.enabled = true;
								button.GetComponentInChildren<Text>().text = targets[i].GetName();
								button.GetComponentInChildren<Text>().enabled = true;
							}
						}
						else
						{
							button.enabled = true;
							button.GetComponentInChildren<Text>().text = targets[i].GetName();
							button.GetComponentInChildren<Text>().enabled = true;
						}
					}
				}
			}
		}
	}

	public void ToggleBattleFlow(bool battleFlow)
	{
		if(PlayerPrefs.GetInt("waitMode") == 0)
		{
			this.battleFlow = battleFlow;
		}
	}

	public void DisplayMessage(string message)
	{
		infoPopUp.SendMessage ("StartDisplay", message);
	}

	void SetButtonsWithContent(Canvas canvasGroup)
	{
		if(canvasGroup.name.Contains("Item"))
		{
			canvasGroup.SendMessage("UpdateButtons");
		}
		else if(canvasGroup.name.Contains("Skill"))
		{
			canvasGroup.SendMessage("SetSelectedCharacter", playerCharacters [currentCharacter]);
		}
		else if (canvasGroup.name.Contains("Enemy"))
		{
			SetTargetButtons();
		}
	}

	void GenerateCharacters()
	{
		for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			GameObject newCharacter;
			GameObject temp;
			switch(i)
			{
			case 0:
				temp = ResourceLoader.LoadCharacter("Vaan");
				newCharacter = (GameObject)Instantiate(temp, CHARACTER_1_START_POSITION, temp.transform.rotation);
				playerCharacters.Add(newCharacter.GetComponent<CharacterScript>());
				break;
			case 1:
				temp = ResourceLoader.LoadCharacter("Ashe");
				newCharacter = (GameObject)Instantiate(temp, CHARACTER_2_START_POSITION, temp.transform.rotation);
				playerCharacters.Add(newCharacter.GetComponent<CharacterScript>());
				break;
			case 2:
				temp = ResourceLoader.LoadCharacter("Rinoa");
				newCharacter = (GameObject)Instantiate(temp, CHARACTER_3_START_POSITION, temp.transform.rotation);
				playerCharacters.Add(newCharacter.GetComponent<CharacterScript>());
				break;
			case 3:
				temp = ResourceLoader.LoadCharacter("Vivi");
				newCharacter = (GameObject)Instantiate(temp, CHARACTER_4_START_POSITION, temp.transform.rotation);
				playerCharacters.Add(newCharacter.GetComponent<CharacterScript>());
				break;
			}

			playerCharacters[i].SetupStats(PartyDataHolderScript.reference.characters[i]);
			playerCharacters[i].SetId(i);

			GameObject characterDisplay = GameObject.Find("CharacterDisplay " + i.ToString());
			if(characterDisplay)
			{
				characterDisplay.GetComponent<CharacterBattleUIScript>().SetCharacter(playerCharacters[i]);
			}
		}
	}

	void GenerateEnemies()
	{
		for(int i = 0; i < enemyIds.Length; i++)
		{
			if (enemyIds [i] != -1)
			{
				GameObject enemyObject = ResourceLoader.LoadEnemy(enemyIds[i].ToString());
				switch(i)
				{
				case 0:
					if(enemyIds[i] > 100)
					{
						isBossBattle = true;
						enemyObject = (GameObject)Instantiate(enemyObject, BOSS_ENEMY_START_POSITION, enemyObject.transform.rotation);
					}
					else
					{
						enemyObject = (GameObject)Instantiate(enemyObject, ENEMY_1_START_POSITION, enemyObject.transform.rotation);
					}
					break;
				case 1: 
					enemyObject = (GameObject)Instantiate(enemyObject, ENEMY_2_START_POSITION, enemyObject.transform.rotation);
					break;
				case 2:
					enemyObject = (GameObject)Instantiate(enemyObject, ENEMY_3_START_POSITION, enemyObject.transform.rotation);
					break;
				}

				enemies.Add(enemyObject.GetComponent<EnemyScript>());
			}
		}
		
		if(enemies.Count == 0)
		{
			Debug.LogError("No enemies generated");
		}

		enemySpawnCount = enemies.Count;
	}

	public List<BattleAction> GetActionList()
	{
		return actionList;
	}

	public BattleToWinningsDataHolder GetBattleWinnings()
	{
		return winningsTransferObject.GetComponent<BattleToWinningsDataHolder> ();
	}

	public BattleToFieldDataHolder GetBattleEndState()
	{
		return stateTransferObject.GetComponent<BattleToFieldDataHolder> ();
	}
}
