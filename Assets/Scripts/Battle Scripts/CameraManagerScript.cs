﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraManagerScript : MonoBehaviour 
{
	private const float LERP_REFRESH_RATE = 0.01f;
	public const int CAMERA_ATTACK_DELAY = 50;
	public const int CAMERA_MAGIC_CAST_DELAY = 100;

	public Vector3 currentPosition;
	public Vector3 currentRotation;

	public Vector3 targetPosition;
	public Vector3 targetRotation;

	private float locationLerp;
	private float movementSpeed;
	private float timeOfLastMovement;

	private float delay;
	private float delayOnArrival;

	private string callOnArrival;
	private List<GameObject> objectsToCall;

	private Vector3[] defaultPositions = new Vector3[4];
	private Vector3[] defaultRotations = new Vector3[4];

	public bool finishedMovement;
	public bool startedMovement;

	public static CameraManagerScript reference;
	public ScreenShakeScript shakeScript;

	void Awake()
	{
		if(reference == null)
		{
			reference = this;
		}
		else if(reference != this)
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		shakeScript = GetComponentInChildren<ScreenShakeScript> ();
		locationLerp = 0;
		movementSpeed = 0;
		delay = 100.0f;
		delayOnArrival = 0;
		finishedMovement = true;
		startedMovement = false;
		objectsToCall = new List<GameObject> ();

		// Default
		//defaultPositions[0].Set(-9.0f, 4.75f, 0.5f);
		//defaultPositions[0].Set(6.5f, 3.5f, -7.0f);
		//defaultPositions[0].Set(-3.5f, 3.0f, -8.0f);
		defaultPositions[0].Set(-3.5f, 3.0f, -8.5f);
		//defaultRotations[0].Set(30.0f, 90.0f, 0.0f);
		//defaultRotations[0].Set(12.0f, -30.0f, 0.0f);
		defaultRotations[0].Set(15.0f, 40.0f, 0.0f);
		
		// In front of characters
		defaultPositions[1].Set(0.0f, 1.75f, 0.5f);
		defaultRotations[1].Set(0.0f, 180.0f, 0.0f);
		
		// Full battlefield (flipped)
		defaultPositions[2].Set(12.0f, 4.75f, 0.5f);
		defaultRotations[2].Set(30.0f, 270.0f, 0.0f);
		
		// In front of enemies
		defaultPositions[3].Set(0.0f, 1.75f, -1.0f);
		defaultRotations[3].Set(12.0f, 0.0f, 0.0f);

		// Starting position
		defaultPositions[3].Set(-5.0f, 3.5f, -1.0f);
		defaultRotations[3].Set(15.0f, 40.0f, 0.0f);

		currentPosition = defaultPositions [3];
		currentRotation = defaultRotations [3];
		targetPosition = currentPosition;
		targetRotation = currentRotation;
		transform.localPosition = currentPosition;
		transform.localEulerAngles = currentRotation;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(delay > 0)
		{
			delay -= 1.0f;
			if(delay < 0)
			{
				delay = 0;
			}
		}
		else
		{
			if(Time.time - timeOfLastMovement > 10.0f) 
			{
				// Do a random movement
			}
		}

		//Debug.Log ("CameraRotation: (x = " + transform.eulerAngles.x + 
			//", y = " + transform.eulerAngles.y + 
			//", z = " + transform.eulerAngles.z + ") at " + Time.timeSinceLevelLoad);
	}

	public void SetCurrentLocation(Vector3 currentPosition, Vector3 currentRotation)
	{
		this.currentPosition = currentPosition;
		this.currentRotation = currentRotation;

		// Keep the rotation within a range, which keeps the camera from flipping-out when performing motions 
		if(this.currentRotation.y > 180)
		{
			this.currentRotation.y = 360 - this.currentRotation.y;
		}
		else if(this.currentRotation.y < -180)
		{
			this.currentRotation.y = this.currentRotation.y + 360;
		}

		this.targetPosition = this.currentPosition;
		this.targetRotation = this.currentRotation;

		this.startedMovement = true;
		this.finishedMovement = true;
		transform.localPosition = this.currentPosition;
		transform.localEulerAngles = this.currentRotation;
		shakeScript.SetOriginRotation (transform.localRotation);
	}

	public void SetTargetLocation(Vector3 targetPosition, Vector3 targetRotation, float movementSpeed)
	{
		// Set the old target location as the current location
		this.currentPosition = this.targetPosition;
		this.currentRotation = this.targetRotation;

		// Set the target location to the new location
		this.targetPosition = targetPosition;
		this.targetRotation = targetRotation;

		this.movementSpeed = movementSpeed;
		this.finishedMovement = false;
		this.startedMovement = true;
		if(!IsInvoking("LerpMovement"))
		{
			InvokeRepeating ("LerpMovement", 0.0f, LERP_REFRESH_RATE);
		}
	}

	public void SetCurrentPositionWithLookat(Vector3 position, Transform lookatPoint)
	{
		this.currentPosition = position;
		transform.localPosition = this.currentPosition;
		transform.LookAt (lookatPoint.position);
		this.currentRotation = transform.eulerAngles;

		// Keep the rotation within a range, which keeps the camera from flipping-out when performing motions 
		if(this.currentRotation.y > 180)
		{
			this.currentRotation.y = 360 - this.currentRotation.y;
		}
		else if(this.currentRotation.y < -180)
		{
			this.currentRotation.y = this.currentRotation.y + 360;
		}
		
		this.targetPosition = this.currentPosition;
		this.targetRotation = this.currentRotation;
		
		this.startedMovement = true;
		this.finishedMovement = true;

		shakeScript.SetOriginRotation (transform.localRotation);
	}

	public void SetDefaultTargetLocation(int defaultNumber, float movementSpeed)
	{
		// Set the old target location as the current location
		this.currentPosition = this.targetPosition;
		this.currentRotation = this.targetRotation;
		
		// Set the target location to the new location
		this.targetPosition = defaultPositions[defaultNumber];
		this.targetRotation = defaultRotations[defaultNumber];
		
		this.movementSpeed = movementSpeed;
		this.finishedMovement = false;
		this.startedMovement = true;
		if(!IsInvoking("LerpMovement"))
		{
			InvokeRepeating ("LerpMovement", 0.0f, LERP_REFRESH_RATE);
		}
	}

	public void SetDefaultCameraLocation(int defaultNumber)
	{
		currentPosition = defaultPositions [defaultNumber];
		currentRotation = defaultRotations [defaultNumber];
		targetPosition = currentPosition;
		targetRotation = currentRotation;

		transform.localPosition = currentPosition;
		transform.localEulerAngles = currentRotation;
		shakeScript.SetOriginRotation (transform.localRotation);

		finishedMovement = true;
		startedMovement = false;
	}

	public void SetCameraToRandomDefaultLocation()
	{
		int randomInt = Random.Range(0, defaultPositions.Length);
		currentPosition = defaultPositions[randomInt];
		currentRotation = defaultRotations[randomInt];
		targetPosition = currentPosition;
		targetRotation = currentRotation;

		transform.localPosition = defaultPositions[randomInt];
		transform.localEulerAngles = defaultRotations[randomInt];
		shakeScript.SetOriginRotation (transform.localRotation);

		finishedMovement = true;
	}

	public void SetTargetToRandomDefaultLocation(float movementSpeed)
	{
		int randomInt = Random.Range(0, defaultPositions.Length);

		targetPosition = defaultPositions[randomInt];
		targetRotation = defaultRotations[randomInt];

		this.movementSpeed = movementSpeed;
		this.finishedMovement = false;
		this.startedMovement = true;
		if(!IsInvoking("LerpMovement"))
		{
			InvokeRepeating ("LerpMovement", 0.0f, LERP_REFRESH_RATE);
		}
	}

	public float GetDelay()
	{
		return delay;
	}

	public void SetDelay(float delay)
	{
		this.delay = delay;
	}

	public float GetDelayOnArrival()
	{
		return delayOnArrival;
	}

	public void SetDelayOnArrival(float delayOnArrival)
	{
		this.delayOnArrival = delayOnArrival;
	}

	public Vector3 GetTargetRotation()
	{
		return targetRotation;
	}

	public void SetArrivalMethod(List<GameObject> targets, string methodName)
	{
		for(int i = 0; i < targets.Count; i++)
		{
			objectsToCall.Add(targets[i]);
		}
		callOnArrival = methodName;
	}

	public void SetArrivalMethod(GameObject target, string methodName)
	{
		objectsToCall.Add(target);
		callOnArrival = methodName;
	}

	private void LerpMovement()
	{
		if(delay <= 0)
		{
			locationLerp += movementSpeed;

			Vector3 newPosition = new Vector3();
			newPosition.x = Mathf.SmoothStep(currentPosition.x, targetPosition.x, locationLerp);
			newPosition.y = Mathf.SmoothStep(currentPosition.y, targetPosition.y, locationLerp);
			newPosition.z = Mathf.SmoothStep(currentPosition.z, targetPosition.z, locationLerp);

			transform.localPosition = newPosition;

			Vector3 newRotation = new Vector3();
			newRotation.x = Mathf.SmoothStep(currentRotation.x, targetRotation.x, locationLerp);
			newRotation.y = Mathf.SmoothStep(currentRotation.y, targetRotation.y, locationLerp);
			newRotation.z = Mathf.SmoothStep(currentRotation.z, targetRotation.z, locationLerp);
			
			transform.localEulerAngles = newRotation;

			//transform.localPosition = Vector3.Lerp(currentPosition, targetPosition, locationLerp);
			//transform.eulerAngles = Vector3.Lerp(currentRotation, targetRotation, locationLerp);

			if(locationLerp >= 1.0f)
			{
				if(objectsToCall.Count > 0 && callOnArrival != "")
				{
					for(int i = 0; i < objectsToCall.Count; i++)
					{
						objectsToCall[i].SendMessage(callOnArrival);
					}
				}

				callOnArrival = "";
				objectsToCall = new List<GameObject>();
				delay = delayOnArrival;
				delayOnArrival = 0;
				timeOfLastMovement = Time.time;
				locationLerp = 0;
				finishedMovement = true;
				CancelInvoke("LerpMovement");
			}
		}
	}
}
