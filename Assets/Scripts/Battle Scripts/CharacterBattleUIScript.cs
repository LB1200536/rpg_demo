﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterBattleUIScript : MonoBehaviour 
{
	private CharacterScript associatedCharacter;
	private Text nameText;
	private Text hpText;
	private Text mpText;
	private Image atbGaugeImage;
	private float atbGaugeFullWidth = -1.0f;
	private float atbGaugeFlashTime = 0.0f;

	// Use this for initialization
	void Start () 
	{
		Text[] textComponents = GetComponentsInChildren<Text> ();
		nameText = textComponents[0];
		hpText = textComponents [1];
		mpText = textComponents [2];
		atbGaugeImage = GetComponentsInChildren<Image> () [1];

		//StartCoroutine (UpdateElements());
		//InvokeRepeating ("UpdateElements", 0, 0.0167f);
	}

	void OnEnable() 
	{
		StartCoroutine (UpdateElements());
	}

	IEnumerator UpdateElements () 
	{
		while(true)
		{
			if(associatedCharacter != null && associatedCharacter.GetAction() != null)
			{
				UpdateText ();

				if(associatedCharacter.GetReady())
				{
					FlashATBGauge();
				}
				else if(associatedCharacter.GetAction().GetActionType() == ActionType.None)
				{
					UpdateATBGauge ();
				}
			}

			yield return new WaitForSeconds(0.0167f);
		}

	}

	public void SetCharacter(CharacterScript character)
	{
		associatedCharacter = character;
		nameText.text = associatedCharacter.GetName ();
		hpText.text = associatedCharacter.GetCurrentHP ().ToString();
		mpText.text = associatedCharacter.GetCurrentMP ().ToString();

		if(atbGaugeFullWidth == -1)
		{
			atbGaugeFullWidth = atbGaugeImage.rectTransform.sizeDelta.x;
		}
		if(atbGaugeImage.color != Color.yellow)
		{
			atbGaugeImage.color = Color.yellow;
		}
		UpdateATBGauge ();
	}

	private void UpdateText()
	{
		// HP //
		if(((int)associatedCharacter.GetCurrentHP() * 100)/(int)associatedCharacter.GetMaxHP() <= 30)
		{
			hpText.color = Color.red;
		}
		else if (((int)associatedCharacter.GetCurrentHP() * 100)/(int)associatedCharacter.GetMaxHP() <= 50)
		{
			hpText.color = Color.yellow;
		}
		else
		{
			hpText.color = Color.white;
		}
		hpText.text = associatedCharacter.GetCurrentHP ().ToString();

		// MP //
		if(((int)associatedCharacter.GetCurrentMP() * 100)/(int)associatedCharacter.GetMaxMP() <= 30)
		{
			mpText.color = Color.red;
		}
		else if (((int)associatedCharacter.GetCurrentMP() * 100)/(int)associatedCharacter.GetMaxMP() <= 50)
		{
			mpText.color = Color.yellow;
		}
		else
		{
			mpText.color = Color.white;
		}
		mpText.text = associatedCharacter.GetCurrentMP ().ToString();

		// Name //
		if(associatedCharacter.selected)
		{
			if(nameText.color != Color.yellow)
			{
				nameText.color = Color.yellow;
			}
		}
		else
		{
			if(nameText.color == Color.yellow)
			{
				nameText.color = Color.white;
			}
		}
	}

	private void UpdateATBGauge()
	{
		if (associatedCharacter.GetStatuses ().Contains (Status.Berserk))
		{
			atbGaugeImage.color = Color.red;
		}
		else
		{
			atbGaugeImage.color = Color.yellow;
		}
	
		// Perform ATB functionality
		if(atbGaugeFlashTime > 0)
		{
			atbGaugeFlashTime = 0;
			//atbGaugeImage.color = Color.yellow;
		}
		
		float percentage = (atbGaugeFullWidth * 0.01f) * associatedCharacter.GetCurrentTurnTime();
		atbGaugeImage.rectTransform.sizeDelta = new Vector2(percentage, atbGaugeImage.rectTransform.sizeDelta.y);
	}

	private void FlashATBGauge()
	{
		if(atbGaugeFlashTime < 1.0f)
		{
			atbGaugeFlashTime += 0.01f;
			atbGaugeImage.color = Color.Lerp(Color.white, Color.green, atbGaugeFlashTime);
		}
	}
}
