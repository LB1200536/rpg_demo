﻿using UnityEngine;
using System.Collections;

public class CharacterIndicatorScript : MonoBehaviour 
{
	private Vector3 eulerAngles;
	private Renderer[] renderers;

	public float rotationRate = 0.0167f;

	// Use this for initialization
	void Start () 
	{
		eulerAngles = transform.localEulerAngles;
		renderers = GetComponentsInChildren<Renderer> ();
	}
	
	// Update is called once per frame
	void Update() 
	{
		if(renderers[0].enabled)
		{
			eulerAngles.y += rotationRate;
			transform.localEulerAngles = eulerAngles;

			if(eulerAngles.y >= 360)
			{
				eulerAngles.y = 0;
			}
		}
	}

	void Disable()
	{
		CancelInvoke ();
		transform.parent = null;
		renderers[0].enabled = false;
		renderers[1].enabled = false;
	}

	void Enable(Transform parentTransform)
	{
		transform.parent = parentTransform;
		Vector3 playerPosition = new Vector3(0.0f, (parentTransform.gameObject.GetComponent<Collider>().bounds.extents.y * 2) + 1.0f, 0.0f);
		transform.localPosition = playerPosition;
		renderers[0].enabled = true;
		renderers[1].enabled = true;
	}
}
