﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageDisplayScript : MonoBehaviour 
{
	private const int DEFAULT_FONT_SIZE = 38;
	private Canvas canvas;
	private Text damageText;

	private float lerpTime;

	public bool started;

	private Vector3 defaultPosition;
	private Vector3 defaultEulerAngles;
	private Color damageColor;

	// Use this for initialization
	void Start () 
	{
		canvas = gameObject.GetComponentInChildren<Canvas> ();
		damageText = canvas.GetComponentInChildren<Text> ();
		damageColor = Color.white;
		damageText.color = damageColor;
		damageText.fontSize = DEFAULT_FONT_SIZE;
		canvas.enabled = false;

		defaultPosition = damageText.rectTransform.localPosition;
		defaultEulerAngles = gameObject.transform.localEulerAngles;
		//damageText.rectTransform.localEulerAngles = new Vector3(0, 180, 0);

		lerpTime = 0;
		started = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!CameraManagerScript.reference.finishedMovement)
		{
			Vector3 distance = (CameraManagerScript.reference.transform.localPosition - gameObject.transform.localPosition);
			damageText.fontSize = DEFAULT_FONT_SIZE + ((int)distance.magnitude)*4;
		}
	}

	private void LerpDisplay()
	{
		lerpTime += 0.0075f;

		Vector3 newPosition = new Vector3 ();
		newPosition.y = Mathf.SmoothStep (defaultPosition.y, defaultPosition.y + 100.0f, lerpTime);
		damageText.rectTransform.localPosition = newPosition;
		damageText.color = Color.Lerp (damageColor, Color.clear, lerpTime);

		if(lerpTime >= 1.0f)
		{
			lerpTime = 0;
			started = false;
			canvas.enabled = false;
			CancelInvoke("LerpDisplay");
		}
	}

	public void SetCanvasRotation(Vector3 rotation)
	{
		if(canvas.enabled)
		{
			canvas.transform.localEulerAngles = rotation;
		}
	}

	public void Begin(string text, Color textColour)
	{
		damageColor = textColour;
		/*
		if(hpDisplay)
		{
			if(damage)
			{
				damageColor = Color.white;
			}
			else // Healing
			{
				damageColor = Color.green;
			}
		}
		else // MP
		{
			if(damage)
			{
				damageColor = Color.magenta;
			}
			else // Healing
			{
				damageColor = Color.blue;
			}
		}
		*/
		damageText.text = text;
		canvas.enabled = true;
		started = true;
		InvokeRepeating ("LerpDisplay", 0.0f, 0.01f);
	}
}
