﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPopUpScript : MonoBehaviour 
{
	private const float DISPLAY_TIME = 1.25f;
	Canvas canvas;
	Text infoText;

	// Use this for initialization
	void Start () 
	{
		canvas = GetComponent<Canvas> ();
		infoText = GetComponentInChildren<Text> ();

		canvas.enabled = false;
	}
	
	void StartDisplay(string text)
	{
		if(!IsInvoking())
		{
			infoText.text = text;
			canvas.enabled = true;
			
			Invoke ("EndDisplay", DISPLAY_TIME);
		}
	}

	void EndDisplay()
	{
		infoText.text = "";
		canvas.enabled = false;
	}
}
