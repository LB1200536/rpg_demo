﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum ActionType {Attack, Skill, Item, Run, None};

public class BattleAction
{	
	bool finished;
	bool started;
	List<GameObject> targets;
	GameObject user;
	ActionType type;
	Skill skill;
	Item item;
	ParticleSystem particleEffect;
	AudioClip soundEffect;

	public BattleAction(GameObject user)
	{
		finished = false;
		started = false;
		targets = new List<GameObject>();
		this.user = user;
		type = ActionType.None;
	}

	public BattleAction(GameObject user, GameObject target, ActionType type)
	{
		this.user = user;
		this.targets[0] = target;
		this.type = type;
	}

	public bool GetFinishedState()
	{
		return finished;
	}

	public bool GetStartedState()
	{
		return started;
	}

	public List<GameObject> GetTargets()
	{
		return targets;
	}

	public GameObject GetUser()
	{
		return user;
	}

	public ActionType GetActionType()
	{
		return type;
	}

	public Skill GetSkill()
	{
		return skill;
	}

	public Item GetItem()
	{
		return item;
	}

	public ParticleSystem GetParticleEffect()
	{
		return particleEffect;
	}

	public AudioClip GetSoundEffect()
	{
		return soundEffect;
	}

	public void SetFinishedState(bool finished)
	{
		this.finished = finished;
	}

	public void SetStartedState(bool started)
	{
		this.started = started;
	}

	public void SetActionType(ActionType type)
	{
		this.type = type;
	}

	public void SetParticleEffect(ParticleSystem effect)
	{
		particleEffect = effect;
	}

	public void SetSoundEffect(AudioClip effect)
	{
		soundEffect = effect;
	}

	public void SetSkill(Skill skill)
	{
		this.skill = skill;
	}

	public void SetItem(Item item)
	{
		this.item = item;
	}

	public void SetTarget(GameObject target)
	{
		targets = new List<GameObject> ();
		this.targets.Add(target);
	}

	public void SetTargetParty(List<GameObject> targets)
	{
		this.targets = targets;
	}

	public void RemoveTargetFromList(GameObject target)
	{
		if(targets.Contains(target))
		{
			targets.Remove(target);
		}

		if(targets.Count == 0)
		{
			SetFinishedState(true);
			user.SendMessage("ActionComplete");
			BattleControllerScript.reference.SendMessage ("EndAction");
		}
	}
}
