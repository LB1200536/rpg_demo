﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum ArmourType {Empty, Helmet, Hood, Cap, Gauntlet, Bracer, Glove, Chestplate, Robe, Clothes};

[System.Serializable]
public class Armour : Equipment
{
	private int physicalDefence;
	private int magicalDefence;
	private int evasion;
	private Element element;
	private ArmourType type;

	public Armour (int id) : base(id)
	{

	}

	public int GetPhysicalDefence()
	{
		return physicalDefence;
	}

	public int GetMagicalDefence()
	{
		return magicalDefence;
	}

	public int GetEvasion()
	{
		return evasion;
	}

	public Element GetElement()
	{
		return element;
	}

	public ArmourType GetArmourType()
	{
		return type;
	}

	public void SetPhysicalDefence(int defence)
	{
		physicalDefence = defence;
	}

	public void SetMagicalDefence(int defence)
	{
		magicalDefence = defence;
	}

	public void SetEvasion(int evasion)
	{
		this.evasion = evasion;
	}

	public void SetElement(Element element)
	{
		this.element = element;
	}

	public void SetArmourType(ArmourType type)
	{
		this.type = type;
	}

	public bool IsHeadgear()
	{
		if(type == ArmourType.Helmet ||
		   type == ArmourType.Hood ||
		   type == ArmourType.Cap)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public bool IsHandgear()
	{
		if(type == ArmourType.Gauntlet ||
		   type == ArmourType.Bracer ||
		   type == ArmourType.Glove)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public bool IsBodyArmour()
	{
		if(type == ArmourType.Chestplate ||
		   type == ArmourType.Robe ||
		   type == ArmourType.Clothes)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	override public Sprite GetMenuImage()
	{
		return ResourceLoader.LoadSprite (type.ToString ());
	}
}
