﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

// Extends the BaseCharacter class, to encapsulate more battle-oriented variables and functionality.

public class BaseBattleCharacter : BaseCharacter 
{
	public const float TURN_TIME_MAX = 100.0f;
	protected const float FLEE_MOVEMENT_MULTIPLIER = 4.0f;
	
	protected float healthLerpTime = 0.0f;
	protected float defeatedLerpTime = 0.0f;
	public float currentTurnTime = 0.0f;

	protected float targetHP;
	protected float targetMP;

	public int id;
	
	protected bool dead;
	protected bool ready;
	protected bool damaged;
	protected bool attacking;
	protected bool casting;

	protected Quaternion defaultRotation;
	protected Quaternion targetRotation;

	protected List<Renderer> materialRenderers = new List<Renderer>();
	private List<Material> baseMaterials = new List<Material>();
	protected Material fleeMaterial;
	public float fleeAmount;

	public DamageDisplayScript damageDisplay;
	protected ParticleSystem battleParticleSystem;
	protected ParticleSystem statusParticleSystem;
	protected Animator animator;
	new protected Collider collider;

	protected BattleAction currentAction;
	protected Element weakElement;

	// Use this for initialization
	protected void Start () 
	{
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer> ();
		for(int i = 0; i < renderers.Length; i++)
		{
			//if (renderers[i].material.HasProperty("_Color"))
			{
				materialRenderers.Add(renderers[i]);
				baseMaterials.Add (renderers[i].material);
			}
		}

		defaultRotation = transform.rotation;

		fleeMaterial = ResourceLoader.LoadMaterial ("TransparentFlee");

		fleeAmount = 0;

		animator = GetComponentInChildren<Animator> ();
		collider = GetComponentInChildren<Collider>();

		currentAction = new BattleAction(this.gameObject);
		currentTurnTime = Random.Range (0.0f, 35.0f);
		attacking = false;
		damaged = false;
		ready = false;
		dead = false;

		ApplyStatusEffect ();
	}
	
	// Update is called once per frame
	protected void Update () 
	{
		if(!currentStatus.Contains(Status.Okay))
		{
			if(!IsInvoking("ApplyStatusEffect"))
			{
				Invoke("ApplyStatusEffect", 0);
			}
		}
		else
		{
			if(IsInvoking("ApplyPoisonEffect"))
			{
				statusParticleSystem = null;
				CancelInvoke("ApplyPoisonEffect");
			}
		}
	}

	protected void ApplyStatusEffect()
	{
		Vector3 systemPosition = transform.position;

		for (int i = 0; i < currentStatus.Count; i++)
		{
			switch (currentStatus [i])
			{
			case Status.Poison:
				if(!IsInvoking("ApplyPoisonEffect"))
				{
					systemPosition.y = GetComponent<Collider>().bounds.max.y;
					statusParticleSystem = (ParticleSystem)Instantiate (ResourceLoader.LoadStatusParticles(currentStatus.ToString ()));
					statusParticleSystem.transform.position = systemPosition;
					statusParticleSystem.transform.parent = this.transform;
					InvokeRepeating("ApplyPoisonEffect", 0, 2.5f);
				}
				break;
			case Status.Berserk:
				systemPosition.y = GetComponent<Collider>().bounds.max.y;
				statusParticleSystem = (ParticleSystem)Instantiate (ResourceLoader.LoadStatusParticles(currentStatus.ToString ()));
				statusParticleSystem.transform.position = systemPosition;
				statusParticleSystem.transform.parent = this.transform;
				break;
			}
		}
	}

	protected void UpdateTurn()
	{
		if(!dead && !ready)
		{
			if(currentTurnTime < TURN_TIME_MAX)
			{
				float newTime = 0.1f + (speed * 0.1f);
				currentTurnTime = currentTurnTime + newTime;
			}
		}
	}

	// Method used to set the stats of a new BaseBattleCharacter using a predefined BaseCharacter object
	public void SetupStats(BaseCharacter baseCharacter)
	{
		characterName = baseCharacter.GetName ();
		skillType = baseCharacter.GetSkillType ();
		currentLevel = baseCharacter.GetCurrentLevel ();
		maxHP = baseCharacter.GetMaxHP ();
		maxMP = baseCharacter.GetMaxMP ();
		currentHP = baseCharacter.GetCurrentHP();
		currentMP = baseCharacter.GetCurrentMP();
		attack = baseCharacter.GetAttack ();
		defence = baseCharacter.GetDefence ();
		magicAttack = baseCharacter.GetMagicAttack ();
		magicDefence = baseCharacter.GetMagicDefence ();
		speed = baseCharacter.GetSpeed ();
		currentLevel = baseCharacter.GetCurrentLevel ();
		currentExperiencePoints = baseCharacter.GetCurrentExperiencePoints ();
		totalExperiencePoints = baseCharacter.GetTotalExperiencePoints ();
		skillList = baseCharacter.GetSkillList ();
		currentWeapon = baseCharacter.GetCurrentWeapon ();
		currentHeadgear = baseCharacter.GetCurrentHeadgear ();
		currentBodyArmour = baseCharacter.GetCurrentBodyArmour ();
		currentHandgear = baseCharacter.GetCurrentHandgear ();
	}

	protected void TakeDamage()
	{
		healthLerpTime += 0.001f;
		if (currentHP > 0 && (int)currentHP <= (int)maxHP) 
		{
			currentHP = Mathf.SmoothStep (currentHP, targetHP, healthLerpTime);
			if (currentHP > maxHP) 
			{
				currentHP = maxHP;
			}
		}
		if((int)currentHP <= 0)
		{
			ready = false;
			ApplyStatus(Status.Okay);
			targetHP = 0;
			currentHP = 0;
			currentTurnTime = 0;
		}
		else if((int)currentHP == (int)targetHP)
		{
			healthLerpTime = 0;
		}
	}

	public void ActionComplete()
	{
		targetRotation = defaultRotation;
		InvokeRepeating ("RotationLerp", 0, 0.0167f);
		currentAction = new BattleAction(this.gameObject);
		currentTurnTime = 0;
		ready = false;
		attacking = false;
	}

	protected virtual void PlayMagicEffect (ParticleSystem particleEffect) {}

	// Method used by an action's user
	public void StartAction(BattleAction action)
	{
		Vector3 cameraPosition = transform.localPosition;
		Vector3 cameraRotation = transform.localEulerAngles;

		if(PlayerPrefs.GetInt("waitMode") == 0)
		{
			BattleControllerScript.reference.battleFlow = false;
		}

		RotateToTarget ();
		switch(action.GetActionType())
		{
		case ActionType.Attack:
			SetAttacking(true);
			SetCameraForAttack();
			break;
		case ActionType.Skill:
			if (targetMP >= action.GetSkill ().GetMPCost ())
			{
				SetCasting (true);
				// Dim the lights
				//GameControllerScript.reference.mainLight.enabled = false;
				PlayMagicEffect (ResourceLoader.LoadAttackParticles ("PrepareMagic"));
				AudioManagerScript.reference.PlayAttackEffect ("PrepareMagic");
				SetCameraForSkill ();
				action.SetStartedState (true);
			}
			else
			{
				BattleControllerScript.reference.DisplayMessage("Not enough MP...");
				SendMessage("BeginEmptyAction", SendMessageOptions.DontRequireReceiver);
			}
			break;
		case ActionType.Item:
			if (PartyDataHolderScript.reference.inventory.CheckInventoryForItem (action.GetItem ()))
			{
				GetComponent<CharacterScript> ().SetUsingItem (true);
				SetCameraForItem ();
			}
			else
			{
				BattleControllerScript.reference.DisplayMessage("Item unavailable...");
				SendMessage("BeginEmptyAction", SendMessageOptions.DontRequireReceiver);
			}
			break;
		}
	}

	// Method used by an action's target
	public void ReactToAction()
	{
		BattleAction action = BattleControllerScript.reference.GetActionList () [0];

		switch(action.GetActionType())
		{
		case ActionType.Item:
			AudioManagerScript.reference.PlayAttackEffect (action.GetSoundEffect ());
			PlayMagicEffect (action.GetParticleEffect ());
			ReactToItem(action.GetItem ());
			break;
		case ActionType.Skill:
			AudioManagerScript.reference.PlayAttackEffect (action.GetSoundEffect ());
			PlayMagicEffect (action.GetParticleEffect ());
			ReactToSkill(action, action.GetUser().GetComponent<BaseBattleCharacter>());
			break;
		case ActionType.Attack:
			ReactToAttack(action, action.GetUser().GetComponent<BaseBattleCharacter>());
			break;
		}

		action.RemoveTargetFromList (this.gameObject);
	}

	protected void ReactToItem(Item item)
	{
		switch(item.GetItemType())
		{
		case ItemType.HP_Healing:
			SetTargetHP((int)targetHP + item.GetEffectAmount());
			StartDamageDisplay(true, item.GetEffectAmount());
			break;
		case ItemType.MP_Healing:
			SetTargetMP((int)targetMP + item.GetEffectAmount());
			StartDamageDisplay(false, item.GetEffectAmount());
			break;
		case ItemType.Status_Healing:
			RemoveStatus (item.GetStatus ());
			break;
		case ItemType.Attacking:
			SetTargetHP((int)targetHP + item.GetEffectAmount());
			StartDamageDisplay(true, item.GetEffectAmount());
			break;
		}

		PartyDataHolderScript.reference.inventory.DeductItemFromTotal(item, 1);
	}

	protected void ReactToSkill(BattleAction action, BaseBattleCharacter user)
	{
		int calculatedDamage = CalculateDamage(action, user.GetMagicAttack(), GetMagicDefence());
		SetTargetHP((int)targetHP + calculatedDamage);

		if(action.GetSkill().GetAssociatedStatus() != Status.None)
		{
			if(action.GetSkill().GetElement() != Element.Heal)
			{
				// Check to see if the status should take effect (percentage chance)
				if(Random.Range(0, 100) < action.GetSkill().GetStatusPercentageChance())
				{
					ApplyStatus(action.GetSkill().GetAssociatedStatus());
				}
			}
			else
			{
				if(currentStatus.Contains(action.GetSkill().GetAssociatedStatus()))
				{
					RemoveStatus(action.GetSkill().GetAssociatedStatus());
				}
			}
		}
		else
		{
			if(action.GetSkill().GetName().Equals("Steal"))
			{
				AttemptItemTheft(this);
			}
		}

		if(PlayerPrefs.GetInt("oneMPCost") == 0)
		{
			user.SendMessage("SetTargetMP", user.GetTargetMP() - 1);
		}
		else
		{
			user.SendMessage("SetTargetMP", user.GetTargetMP() - action.GetSkill().GetMPCost());
		}

		if(calculatedDamage != 0)
		{
			StartDamageDisplay(true, calculatedDamage);
		}

		if(calculatedDamage < 0)
		{
			SetDamaged (true);
			CameraManagerScript.reference.shakeScript.BeginShake();
		}
	}
	
	protected void ReactToAttack(BattleAction action, BaseBattleCharacter user)
	{
		int evasionChance = Random.Range (0, 90 + GetEvasion());
		bool isCritical = false;
		int calculatedDamage = 0;

		if(user.GetAccuracy() - evasionChance > 0) // If the hit connects
		{
			isCritical = CalculateCriticalHit (user);
			if (isCritical)
			{
				calculatedDamage = CalculateDamage (action, user.GetAttack () * 2, 0);
			}
			else
			{
				calculatedDamage = CalculateDamage (action, user.GetAttack (), GetDefence ());
			}

			SetTargetHP ((int)targetHP + calculatedDamage);
			
			if(calculatedDamage < 0)
			{
				if (isCritical)
				{
					AudioManagerScript.reference.PlayAttackEffect (
						ResourceLoader.LoadBattleSFX ("Critical" + action.GetSoundEffect ().name));
				}
				else
				{
					AudioManagerScript.reference.PlayAttackEffect (action.GetSoundEffect ());
				}

				PlayMagicEffect (action.GetParticleEffect ());
				SetDamaged (true);

				float normalisedDamagePercentage = ((maxHP - targetHP) / (maxHP * 0.01f)) * 0.01f; // Screen shake is more pronounced when the damage is a larger chunk of the character's health
				if (isCritical)
				{
					normalisedDamagePercentage *= 1.5f; // Critical hits result in a bigger screen shake
				}


				CameraManagerScript.reference.shakeScript.BeginShake (Mathf.Clamp(normalisedDamagePercentage, 0.2f, 0.8f));
			}
		}
			
		StartDamageDisplay(true, calculatedDamage, isCritical);
	}

	private bool CalculateCriticalHit(BaseBattleCharacter user)
	{
		int criticalHitChance = Random.Range(user.speed + user.luck, 200);

		// Higher chance to avoid critical hit
		int criticalAvoidChance = Random.Range ((speed + luck )* 2, 200);

		return criticalHitChance > criticalAvoidChance;
	}

	protected void BeginFleeAnimation()
	{
		animator.SetTrigger ("Running");
		transform.Rotate (new Vector3 (0, 180, 0));
		InvokeRepeating ("Flee", 0, (1.0f * 0.06f));
		// Begin invoke of flee method
	}

	virtual protected void Flee()
	{
		fleeAmount += Time.smoothDeltaTime;
		
		for(int i = 0; i < materialRenderers.Count; i++)
		{
			materialRenderers[i].material.Lerp(baseMaterials[i], fleeMaterial, fleeAmount);
		}
	}
	
	private int CalculateDamage(BattleAction action, float attack, float defence)
	{
		int calculatedDamage = 1;
		// 10% leeway for both attack and defence
		float calculatedDefence = Random.Range (defence - ((defence / 100) * 10), defence + ((defence / 100) * 10));
		float calculatedAttack = Random.Range (attack - ((attack / 100) * 10), attack + ((attack / 100) * 10));

		if(action.GetActionType() == ActionType.Attack)
		{
			if(action.GetUser().GetComponent<BaseBattleCharacter>().GetStatuses().Contains(Status.Berserk))
			{
				// Berserk status gives a bonus to attack damage
				calculatedAttack *= 1.5f;
			}
				
			calculatedDamage = (int)calculatedDefence - (int)calculatedAttack;
			
			if(calculatedDamage > -1)
			{
				calculatedDamage = -1;
			}
		}
		else if(action.GetActionType() == ActionType.Skill)
		{
			if (action.GetSkill ().GetElement () != Element.Heal && action.GetSkill ().GetBaseDamage () > 0) // Damaging Skill
			{
				calculatedDamage = ((action.GetSkill ().GetBaseDamage () * -1) / 2) + ((int)calculatedDefence - (int)calculatedAttack);
				
				if (action.GetSkill ().GetElement () == weakElement)
				{
					calculatedDamage += calculatedDamage / 2; // Add 50% damage to successfully matched weakness
				}
				// FIXME: Add elemental resistance
				
				if (calculatedDamage > -1) // If calculated damage goes below the lowest amount of damage applicable
				{
					calculatedDamage = -1;
				}
			}
			else if (action.GetSkill ().GetElement () == Element.Heal && action.GetSkill ().GetBaseDamage () > 0) // Healing skill
			{
				calculatedDamage = action.GetSkill ().GetBaseDamage () / 2 + (int)calculatedAttack;
			}
			else // Skill does not affect HP or MP
			{
				calculatedDamage = 0;
			}

			// Lower the amount of damage dealt based on how many targets were selected, if the damage is above the minimum
			if(calculatedDamage > 1 || calculatedDamage < -1)
			{
				calculatedDamage = calculatedDamage / action.GetTargets().Count;
			}
		}

		return calculatedDamage;
	}

	public void StartDamageDisplay(bool hpDisplay, int value, bool critical = false)
	{
		//int deltaDamage = 0;
		
		if(hpDisplay)
		{
			//deltaDamage = (int)currentHP - (int)targetHP;
			if(value < 0) // Damage
			{
				value *= -1;
				if (critical)
				{
					damageDisplay.Begin (value.ToString (), Color.yellow);
				}
				else
				{
					damageDisplay.Begin (value.ToString(), Color.white);
				}
			}
			else if(value > 0) // Healing
			{
				damageDisplay.Begin (value.ToString(), Color.green);
			}
		}
		else // MP Display
		{
			//deltaDamage = (int)currentMP - (int)targetMP;
			if(value < 0) // Damage
			{
				value *= -1;
				damageDisplay.Begin (value.ToString(), Color.blue);
			}
			else if(value > 0) // Healing
			{
				damageDisplay.Begin (value.ToString(), Color.magenta);
			}
		}
			
		if(value == 0)
		{
			damageDisplay.Begin ("Miss", Color.white);
			AudioManagerScript.reference.PlayAttackEffect(ResourceLoader.LoadBattleSFX("AttackMiss"));
		}
	}

	/* Camera Positioning Methods */
	protected void SetCameraForAttack()
	{
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = defaultRotation.eulerAngles;
		if(transform.position.z < 0) // Player characters are at a negative z-position
		{
			cameraPosition.x -= 1.0f;
			cameraPosition.z += ((collider.bounds.size.z) * 1.75f);
			cameraRotation.y += 135.0f;
		}
		else // Enemy characters are at a positive z-position
		{
			cameraPosition.z -= ((collider.bounds.size.z) * 1.25f);
			cameraRotation.y += 170.0f;
		}
		cameraPosition.y = collider.bounds.max.y * 0.65f;
		//cameraRotation.y += 165.0f;
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDelay(CameraManagerScript.CAMERA_ATTACK_DELAY);
		CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);
		CameraManagerScript.reference.SetDelayOnArrival(CameraManagerScript.CAMERA_ATTACK_DELAY);
	}

	protected void SetCameraForSkill()
	{
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = defaultRotation.eulerAngles;
		
		if(transform.position.z < 0)
		{
			cameraPosition.z += ((collider.bounds.size.z) * 1.5f);
		}
		else
		{
			cameraPosition.z -= ((collider.bounds.size.z) * 1.25f);
		}
		cameraPosition.y = collider.bounds.max.y/2;
		cameraRotation.y += 180.0f;
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDelay(GetParticleSystem().main.duration * CameraManagerScript.CAMERA_ATTACK_DELAY);
		CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);
	}

	protected void SetCameraForItem()
	{
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = defaultRotation.eulerAngles;
		if(transform.position.z < 0)
		{
			cameraPosition.z += ((collider.bounds.size.z) * 1.5f);
		}
		else
		{
			cameraPosition.z -= ((collider.bounds.size.z) * 1.25f);
		}
		cameraPosition.y = collider.bounds.max.y/2;
		cameraRotation.y += 175.0f;
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDelay(CameraManagerScript.CAMERA_ATTACK_DELAY);
		CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);
	}

	protected void RotateToTarget()
	{
		Vector3 averagePosition = Vector3.zero;
		for (int i = 0; i < currentAction.GetTargets ().Count; i++)
		{
			averagePosition += new Vector3(currentAction.GetTargets () [i].transform.position.x, 
											this.transform.position.y, 
											currentAction.GetTargets () [i].transform.position.z) ;
		}

		averagePosition = averagePosition / currentAction.GetTargets ().Count;
		targetRotation = Quaternion.LookRotation (averagePosition);
		InvokeRepeating ("RotationLerp", 0, 0.0167f);
		//transform.LookAt (averagePosition);
	}

	protected void RotationLerp()
	{
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 10);
		if (transform.rotation == targetRotation)
		{
			CancelInvoke ("RotationLerp");
		}
	}

	/* Apply Status Effects */
	protected void ApplyPoisonEffect()
	{
		if(BattleControllerScript.reference.battleFlow)
		{
			statusParticleSystem.Play();
			SetTargetHP (GetTargetHP() - ((int)((maxHP / 100) * 5))); // 5% of maximum health
			//StartDamageDisplay(true);
		}
	}

	protected void AttemptItemTheft(BaseBattleCharacter target)
	{
		EnemyScript enemyTarget = target.GetComponent<EnemyScript> ();
		if(enemyTarget)
		{
			if(enemyTarget.GetItemSteal() != null)
			{
				if(Random.Range((speed + luck), 100) <= enemyTarget.GetItemStealRate())
				{
					BattleControllerScript.reference.GetBattleWinnings().AddItemToEarnings(enemyTarget.GetItemSteal());
					BattleControllerScript.reference.infoPopUp.SendMessage("StartDisplay", "Stole " + enemyTarget.GetItemSteal().GetName() + ".");
					enemyTarget.SetItemSteal(-1, 0);
				}
				else
				{
					BattleControllerScript.reference.infoPopUp.SendMessage("StartDisplay", "Failed steal attempt.");
				}
			}
			else
			{
				BattleControllerScript.reference.infoPopUp.SendMessage("StartDisplay", "Nothing to steal.");
			}
		}
		else // Targeting the player characters
		{
			// FIXME: Add code for allowing player items to be stolen
		}
	}

	/* Getters */
	public int GetTargetHP()
	{
		return (int)targetHP;
	}

	public int GetTargetMP()
	{
		return (int)targetMP;
	}

	public ParticleSystem GetParticleSystem()
	{
		return battleParticleSystem;
	}

	public BattleAction GetAction()
	{
		return currentAction;
	}

	public bool GetReady()
	{
		return ready;
	}

	public bool GetAttacking()
	{
		return true;
	}

	public bool GetDead()
	{
		return dead;
	}

	public bool GetDamaged()
	{
		return damaged;
	}

	public int GetId()
	{
		return id;
	}

	public float GetCurrentTurnTime()
	{
		return currentTurnTime;
	}

	public virtual int GetAccuracy() { return 0; }

	public virtual int GetEvasion() { return 0; }

	/* Setters */
	public void SetReady(bool ready)
	{
		this.ready = ready;
	}
	
	public void SetAction(BattleAction action)
	{
		currentAction = action;
	}

	public void SetTargetHP(int targetHP)
	{
		this.targetHP = targetHP;
	}
	
	public void SetTargetMP(int targetMP)
	{
		this.targetMP = targetMP;
	}

	public void SetDamaged(bool damaged)
	{
		if(damaged && !this.damaged)
		{
			animator.SetTrigger("Damaged");
			this.damaged = true;
		}
		else if(!damaged && this.damaged)
		{
			if(gameObject.layer == 9)
			{
				animator.rootPosition = new Vector3(0.0f, 0.0f);
				animator.rootRotation = new Quaternion(0.0f, 0.0f,0.0f, 0.0f);
			}
			
			this.damaged = false;
		}
	}

	virtual public void SetAttacking(bool attacking)
	{
		if(attacking && !this.attacking)
		{
			Vector3 tempPosition = gameObject.transform.localPosition;
			tempPosition.y = 0;
			animator.rootPosition = tempPosition;
			animator.SetTrigger("Attacking");
			this.attacking = true;
		}
		else if(!attacking && this.attacking)
		{
			this.attacking = false;
		}
	}

	public void SetCasting(bool casting)
	{
		if(casting && !this.casting)
		{
			Vector3 tempPosition = gameObject.transform.localPosition;
			tempPosition.y = 0;
			animator.rootPosition = tempPosition;
			animator.Play("Cast");
			animator.SetBool("Casting", true);
			this.casting = true;
		}
		else if(!casting && this.casting)
		{
			animator.SetBool("Casting", false);
			this.casting = false;
		}
	}

	public void SetTurnTime(int turnTime)
	{
		currentTurnTime = (float)turnTime;
	}
}
