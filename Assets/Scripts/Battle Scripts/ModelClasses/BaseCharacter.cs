﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum Status {Okay, Poison, Sleep, Silence, Petrify, Berserk, Death, None};

// The base class of all characters in the game. Handles basic stats and functionality.
public class BaseCharacter : MonoBehaviour 
{
	private float levelUpModifier = 0.04f;
	protected int characterId;
	protected int currentLevel = 1;
	protected string characterName;
	protected float maxHP;
	protected float currentHP;
	protected float maxMP;
	protected float currentMP;
	protected int attack;
	protected int defence;
	protected int magicAttack;
	protected int magicDefence;
	protected int speed;
	protected int luck;
	protected int totalExperiencePoints = 0;
	protected int currentExperiencePoints = 0;
	protected int experienceForNextLevel = 100;
	protected List<Status> currentStatus = new List<Status>();
	protected List<Skill> skillList = new List<Skill>();
	protected SkillType skillType;

	protected Weapon currentWeapon;
	protected Armour currentHeadgear;
	protected Armour currentBodyArmour;
	protected Armour currentHandgear;

	private SerializableCharacter serializable = new SerializableCharacter ();

	// Method used by the SavedDataManager to fill 'current session' character with saved data
	public void FillDataWithSerializable(SerializableCharacter serializable)
	{
		this.serializable = serializable;
		characterId = serializable.characterId;
		currentLevel = serializable.currentLevel;
		characterName = serializable.characterName;
		maxHP = serializable.maxHP;
		currentHP = serializable.currentHP;
		maxMP = serializable.maxMP;
		currentMP = serializable.currentMP;
		attack = serializable.attack;
		defence = serializable.defence;
		magicAttack = serializable.magicAttack;
		magicDefence = serializable.magicDefence;
		speed = serializable.speed;
		luck = serializable.luck;
		levelUpModifier = serializable.levelUpModifier;
		currentExperiencePoints = serializable.currentExperiencePoints;
		totalExperiencePoints = serializable.totalExperiencePoints;
		experienceForNextLevel = serializable.experienceForNextLevel;
		skillList = serializable.skillList;
		skillType = serializable.skillType;
		currentWeapon = serializable.currentWeapon;
		currentHeadgear = serializable.currentHeadgear;
		currentBodyArmour = serializable.currentBodyArmour;
		currentHandgear = serializable.currentHandgear;
		currentStatus = serializable.currentStatus;
	}

	public void CheckForLevelUp()
	{
		if(currentExperiencePoints >= experienceForNextLevel)
		{
			SetCurrentLevel(currentLevel + 1);
			SetCurrentExperiencePoints(0);
			ApplyLevelRewards();
		}
	}

	public void GenerateNextLevelRequirement()
	{
		int nextLevelSquared = (int)Math.Pow ((currentLevel + 1), 2);
		SetExperienceForNextLevel((int)(nextLevelSquared / levelUpModifier));
	}

	public void ApplyLevelRewards()
	{
		PartyDataHolderScript.reference.levelUpParser.ParseNewLevel (this);
		GenerateNextLevelRequirement();
	}

	public void DeltaHP(int deltaHP)
	{
		currentHP += deltaHP;
	}

	/* Getters */
	public int GetCharacterId()
	{
		return characterId;
	}

	public int GetCurrentLevel()
	{
		return currentLevel;
	}

	public string GetName()
	{
		return characterName;
	}

	public int GetMaxHP()
	{
		return (int)maxHP;
	}

	public int GetCurrentHP()
	{
		return (int)currentHP;
	}

	public int GetMaxMP()
	{
		return (int)maxMP;
	}

	public int GetCurrentMP()
	{
		return (int)currentMP;
	}

	public virtual int GetAttack()
	{
		return attack;
	}
	
	public virtual int GetDefence()
	{
		return defence;
	}
	
	public virtual int GetMagicAttack()
	{
		return magicAttack;
	}
	
	public virtual int GetMagicDefence()
	{
		return magicDefence;
	}

	public int GetLuck()
	{
		return luck;
	}

	public int GetSpeed()
	{
		return speed;
	}

	public int GetCurrentExperiencePoints()
	{
		return currentExperiencePoints;
	}

	public int GetTotalExperiencePoints()
	{
		return totalExperiencePoints;
	}

	public int GetExperiencePointsForNextLevel()
	{
		return experienceForNextLevel;
	}

	public List<Status> GetStatuses()
	{
		return currentStatus;
	}

	public List<Skill> GetSkillList()
	{
		return skillList;
	}

	public SkillType GetSkillType()
	{
		return skillType;
	}

	public string GetClassName()
	{
		switch(skillType)
		{
		case SkillType.Thievery:
			return "Thief";
		case SkillType.Gallantry:
			return "Knight";
		case SkillType.WhiteMagic:
			return "White Mage";
		case SkillType.BlackMagic:
			return "Black Mage";
		}

		return "None";
	}

	public Weapon GetCurrentWeapon()
	{
		return currentWeapon;
	}

	public Armour GetCurrentHeadgear()
	{
		return currentHeadgear;
	}
	public Armour GetCurrentBodyArmour()
	{
		return currentBodyArmour;
	}

	public Armour GetCurrentHandgear()
	{
		return currentHandgear;
	}

	public SerializableCharacter GetSerializable()
	{
		return serializable;
	}

	public int GetPhysicalWeaponDamage()
	{
		if(currentWeapon != null)
		{
			return currentWeapon.GetPhysicalDamage ();
		}
		else
		{
			return 0;
		}
	}

	public int GetMagicalWeaponDamage()
	{
		if(currentWeapon != null)
		{
			return currentWeapon.GetMagicalDamage ();
		}
		else
		{
			return 0;
		}
	}

	public int GetWeaponAccuracy()
	{
		if(currentWeapon != null)
		{
			return currentWeapon.GetAccuracy ();
		}
		else
		{
			return 95;
		}
	}

	public int GetPhysicalArmourDefence()
	{
		int totalDefence = 0;
		if(currentHandgear != null)
		{
			totalDefence += currentHandgear.GetPhysicalDefence ();
		}
		if(currentHeadgear != null)
		{
			totalDefence += currentHeadgear.GetPhysicalDefence ();
		}
		if(currentBodyArmour != null)
		{
			totalDefence += currentBodyArmour.GetPhysicalDefence ();
		}

		return totalDefence;
	}

	public int GetMagicalArmourDefence()
	{
		int totalDefence = 0;
		if(currentHandgear != null)
		{
			totalDefence += currentHandgear.GetMagicalDefence ();
		}
		if(currentHeadgear != null)
		{
			totalDefence += currentHeadgear.GetMagicalDefence ();
		}
		if(currentBodyArmour != null)
		{
			totalDefence += currentBodyArmour.GetMagicalDefence ();
		}
		
		return totalDefence;
	}

	public int GetArmourEvasion()
	{
		int totalEvasion = 0;
		if(currentHandgear != null)
		{
			totalEvasion += currentHandgear.GetEvasion ();
		}
		if(currentHeadgear != null)
		{
			totalEvasion += currentHeadgear.GetEvasion ();
		}
		if(currentBodyArmour != null)
		{
			totalEvasion += currentBodyArmour.GetEvasion ();
		}
		
		return totalEvasion;
	}

	/* Setters */
	public void SetId(int id)
	{
		characterId = id;
		serializable.characterId = characterId;
	}

	public void SetName(string newName)
	{
		gameObject.name = newName;
		characterName = newName;
		serializable.characterName = newName;
	}

	public void SetCurrentLevel(int level)
	{
		this.currentLevel = level;
		serializable.currentLevel = currentLevel;
	}

	public void SetMaxHP(int maxHP)
	{
		this.maxHP = maxHP;
		serializable.maxHP = maxHP;
	}

	public void SetCurrentHP(int currentHP)
	{
		this.currentHP = currentHP;
		serializable.currentHP = currentHP;

		if(this.currentHP > this.maxHP)
		{
			this.currentHP = maxHP;
			serializable.currentHP = maxHP;
		}
		if(this.currentHP < 0)
		{
			this.currentHP = 0;
			serializable.currentHP = 0;
		}
	}

	public void SetMaxMP(int maxMP)
	{
		this.maxMP = maxMP;
		serializable.maxMP = maxMP;
	}

	public void SetCurrentMP(int currentMP)
	{
		this.currentMP = currentMP;
		serializable.currentMP = currentMP;

		if(this.currentMP > this.maxMP)
		{
			this.currentMP = maxMP;
			serializable.currentMP = maxMP;
		}
		if(this.currentMP < 0)
		{
			this.currentMP = 0;
			serializable.currentMP = 0;
		}
	}

	public void SetAttack(int attack)
	{
		this.attack = attack;
		serializable.attack = attack;
	}

	public void SetDefence(int defence)
	{
		this.defence = defence;
		serializable.defence = defence;
	}

	public void SetMagicAttack(int magicAttack)
	{
		this.magicAttack = magicAttack;
		serializable.magicAttack = magicAttack;
	}

	public void SetMagicDefence(int magicDefence)
	{
		this.magicDefence = magicDefence;
		serializable.magicDefence = magicDefence;
	}

	public void SetSpeed(int speed)
	{
		this.speed = speed;
		serializable.speed = speed;
	}

	public void SetLuck(int luck)
	{
		this.luck = luck;
		serializable.luck = luck;
	}

	public void AddExperiencePoints(int experience)
	{
		totalExperiencePoints += experience;
		currentExperiencePoints += experience;
		serializable.totalExperiencePoints = totalExperiencePoints;
		serializable.currentExperiencePoints = currentExperiencePoints;

		//CheckForLevelUp ();
	}

	public void SetCurrentExperiencePoints(int experience)
	{
		currentExperiencePoints = experience;
		serializable.currentExperiencePoints = currentExperiencePoints;
	}

	public void SetTotalExperiencePoints(int experience)
	{
		totalExperiencePoints = experience;
		serializable.totalExperiencePoints = totalExperiencePoints;
	}

	public void SetExperienceForNextLevel(int experience)
	{
		experienceForNextLevel = experience;
		serializable.experienceForNextLevel = experienceForNextLevel;
	}

	public void ApplyStatus(Status status)
	{
		if (!currentStatus.Contains (status))
		{
			currentStatus.Add (status);

			if (currentStatus.Contains(Status.Okay))
			{
				currentStatus.Remove (Status.Okay);
			}

			serializable.currentStatus = currentStatus;
		}
	}

	public void RemoveStatus(Status status)
	{
		if (status != Status.Okay && currentStatus.Contains (status))
		{
			currentStatus.Remove (status);
			serializable.currentStatus = currentStatus;
		}

		if (currentStatus.Count == 0)
		{
			currentStatus.Add (Status.Okay);
		}

		serializable.currentStatus = currentStatus;
	}

	public void RemoveAllStatuses()
	{
		currentStatus.Clear ();
		currentStatus = new List<Status> ();
		currentStatus.Add (Status.Okay);
		serializable.currentStatus = currentStatus;
	}

	public void SetSkillType(SkillType skillType)
	{
		this.skillType = skillType;
		serializable.skillType = skillType;
		SetLevelUpConstant ();
	}

	private void SetLevelUpConstant()
	{
		switch(skillType)
		{
		case SkillType.Thievery:
			levelUpModifier = 0.04f;
			break;
		case SkillType.Gallantry:
			levelUpModifier = 0.035f;
			break;
		case SkillType.WhiteMagic:
			levelUpModifier = 0.03f;
			break;
		case SkillType.BlackMagic:
			levelUpModifier = 0.025f;
			break;
		default:
			break;
		}

		serializable.levelUpModifier = levelUpModifier;
		GenerateNextLevelRequirement ();
	}

	public void AddSkillToList(Skill newSkill)
	{
		skillList.Add (newSkill);
		serializable.skillList = skillList;
	}

	public void SetEquipment(Equipment equipment)
	{
		if (equipment.IsWeapon()) 
		{
			SetWeapon((Weapon)equipment);
		}
		else if(equipment.IsArmour())
		{
			if (((Armour)equipment).IsHeadgear ()) 
			{
				SetHeadgear((Armour)equipment);
			}
			else if (((Armour)equipment).IsHandgear ()) 
			{
				SetHandgear((Armour)equipment);
			}
			else if (((Armour)equipment).IsBodyArmour ()) 
			{
				SetBodyArmour((Armour)equipment);
			}
		}
	}

	public void SetWeapon(Weapon weapon)
	{
		if(weapon != null)
		{
			weapon.Equip (serializable);
		}
		if(serializable.currentWeapon != null)
		{
			serializable.currentWeapon.Equip(null);
		}

		currentWeapon = weapon;
		serializable.currentWeapon = weapon;
	}
	
	public void SetHeadgear(Armour headgear)
	{
		if(headgear != null)
		{
			headgear.Equip(serializable);
		}
		if(serializable.currentHeadgear != null)
		{
			serializable.currentHeadgear.Equip(null);
		}

		currentHeadgear = headgear;
		serializable.currentHeadgear = headgear;
	}
	
	public void SetHandgear(Armour handgear)
	{
		if(handgear != null)
		{
			handgear.Equip(serializable);
		}
		if(serializable.currentHandgear != null)
		{
			serializable.currentHandgear.Equip(null);
		}

		currentHandgear = handgear;
		serializable.currentHandgear = handgear;
	}
	
	public void SetBodyArmour(Armour bodyArmour)
	{
		if(bodyArmour != null)
		{
			bodyArmour.Equip(serializable);
		}
		if(serializable.currentBodyArmour != null)
		{
			serializable.currentBodyArmour.Equip(null);
		}

		currentBodyArmour = bodyArmour;
		serializable.currentBodyArmour = bodyArmour;
	}

	public void UnequipItem(Equipment equipmentToRemove)
	{
		if(typeof(Weapon).IsInstanceOfType (equipmentToRemove) &&
		   serializable.currentWeapon.Equals((Weapon)equipmentToRemove))
		{
			SetWeapon(null);
			equipmentToRemove.Equip(null);
		}
		else if(typeof(Armour).IsInstanceOfType (equipmentToRemove))
		{
			if(serializable.currentHeadgear.Equals((Armour)equipmentToRemove))
			{
				SetHeadgear(null);
				equipmentToRemove.Equip(null);
			}
			else if(serializable.currentHandgear.Equals((Armour)equipmentToRemove))
			{
				SetHandgear(null);
				equipmentToRemove.Equip(null);
			}
			else if(serializable.currentBodyArmour.Equals((Armour)equipmentToRemove))
			{
				SetBodyArmour(null);
				equipmentToRemove.Equip(null);
			}
		}
	}
}

[System.Serializable]
public class SerializableCharacter
{
	public int currentLevel;
	public int characterId;
	public string characterName;
	public float maxHP;
	public float currentHP;
	public float maxMP;
	public float currentMP;
	public int attack;
	public int defence;
	public int magicAttack;
	public int magicDefence;
	public int speed;
	public int luck;
	public float levelUpModifier;
	public int totalExperiencePoints;
	public int currentExperiencePoints;
	public int experienceForNextLevel;
	public List<Skill> skillList = new List<Skill>();
	public SkillType skillType;
	public List<Status> currentStatus;
	public Weapon currentWeapon;
	public Armour currentHeadgear;
	public Armour currentBodyArmour;
	public Armour currentHandgear;
}
