﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CharacterScript : BaseBattleCharacter 
{
	GameObject hand;
	int weaponIndex;
	
	float magicLerpTime = 0.0f;
	float movementLerpTime = 0.0f;
	Vector3 defaultPosition;
	Vector3 targetPosition;

	private bool usingItem;
	public bool selected;
	public bool inPosition = false;

	Image progressionBarImage;

	// Use this for initialization
	new void Start () 
	{
		base.Start ();

		defaultPosition = gameObject.transform.position;
		targetPosition = transform.position;
		targetPosition.z -= 2.0f;
		transform.position = targetPosition;
		targetPosition = defaultPosition;

		animator.enabled = false;
		casting = false;
		usingItem = false;
		selected = false;

		Transform[] modelJoints = gameObject.GetComponentsInChildren<Transform> ();
		foreach(Transform joint in modelJoints)
		{
			if(joint.gameObject.CompareTag("Hand"))
			{
				hand = joint.gameObject;
				hand.transform.localEulerAngles = new Vector3(0.0f, 270.0f, 270.0f);
			}
		}

		InstantiateWeapon ();
		Invoke ("MoveIntoStartingPosition", 3.0f);
	}

	new void Update()
	{
		base.Update ();
		MoveCharacter ();

		if(currentHP > 0 && currentHP != targetHP)
		{
			TakeDamage();
		}
		
		if(currentMP != targetMP)
		{
			LerpMP();
		}

		if(BattleControllerScript.reference.battleFlow)
		{
			if (currentTurnTime >= TURN_TIME_MAX) 
			{
				if(currentAction.GetActionType() == ActionType.None && !ready)
				{
					ready = true;
					if (currentStatus.Contains(Status.Berserk))
					{
						GenerateBerserkAction ();
					}
					else
					{
						BattleControllerScript.reference.SendMessage("AddCharacterToReadyList", id);
					}
				}
			}
		}
	}
	
	new public void UpdateTurn()
	{
		if(!dead)
		{
			if(BattleControllerScript.reference.battleFlow)
			{
				base.UpdateTurn();
			}
		}
		else if(currentTurnTime != 0)
		{
			gameObject.transform.position = defaultPosition;
		}
	}

	new private void Flee()
	{
		base.Flee();
		transform.Translate(-transform.forward * Time.smoothDeltaTime * FLEE_MOVEMENT_MULTIPLIER);
	}

	new public void TakeDamage()
	{
		base.TakeDamage ();

		if((int)targetHP <= 0 && !dead)
		{
			BattleControllerScript.reference.AddCharacterToDeadCount(this);
			dead = true;
			currentTurnTime = 0;
			Vector3 tempPosition = gameObject.transform.localPosition;
			tempPosition.y = 0;
			animator.rootPosition = tempPosition;
			animator.rootRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
			animator.SetTrigger("Dead");
			CancelInvoke();
		}
	
		if(damaged)
		{
			SetDamaged (false);
		}

		if((int)currentHP == (int)targetHP)
		{
			healthLerpTime = 0;
			//SetDamaged(false);
		}
	}

	private void MoveIntoStartingPosition()
	{
		animator.enabled = true;
		InvokeRepeating ("LerpCharacterMaterial", 0, 0.0167f); 
	}

	private void LerpCharacterMaterial()
	{
		fleeAmount += Time.smoothDeltaTime;
		
		for(int i = 0; i < materialRenderers.Count; i++)
		{
			materialRenderers[i].material.Lerp(fleeMaterial, materialRenderers[i].material, fleeAmount);
		}

		if(fleeAmount >= 1.0f)
		{
			fleeAmount = 0;
			CancelInvoke("LerpCharacterMaterial");
		}
	}

	public void LerpMP()
	{
		magicLerpTime += 0.001f;

		if(currentMP > 0 && (int)currentMP <= (int)maxMP)
		{
			currentMP = Mathf.SmoothStep (currentMP, targetMP, magicLerpTime);
			if(currentMP > maxMP)
			{
				currentMP = maxMP;
			}

			if(currentMP < 0)
			{
				currentMP = 0;
			}

			if((int)currentMP == (int)targetMP)
			{
				magicLerpTime = 0;
			}
		}
	}

	public void InstantiateWeapon()
	{
		GameObject temp;
		temp = (GameObject)Instantiate (ResourceLoader.LoadWeapon(currentWeapon.GetName()), new Vector3(0.0f, 0.0f, 0.0f), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f)) as GameObject;
		temp.transform.parent = hand.transform;
		temp.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
		temp.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
	}

	new public void ActionComplete()
	{
		base.ActionComplete ();
		SetAttacking (false);
		SetUsingItem (false);
		SetCasting (false);
	}

	private void MoveCharacter()
	{
		if (animator.enabled)
		{
			if(transform.position != targetPosition)
			{
				if (movementLerpTime < 1.0f)
				{
					movementLerpTime += Time.deltaTime * 0.25f;
					Vector3 newPosition = gameObject.transform.position;
					newPosition.z = Mathf.SmoothStep (newPosition.z, targetPosition.z, movementLerpTime);
					gameObject.transform.position = newPosition;
				}
				else
				{
					movementLerpTime = 0;
				}
			}
		}

		//else
		//{
		//	if(movementLerpTime > 0)
		//	{
		//		movementLerpTime -= 0.075f;
		//		Vector3 newPosition = gameObject.transform.position;
		//		newPosition.z = Mathf.SmoothStep(defaultPosition.z, defaultPosition.z + 1.0f, movementLerpTime);
		//		gameObject.transform.position = newPosition;
		//	}
		//}
	}

	private void GenerateBerserkAction()
	{
		BattleAction action = new BattleAction (this.gameObject);
		action.SetSoundEffect (ResourceLoader.LoadBattleSFX (currentWeapon.GetWeaponType () + "Attack"));
		action.SetParticleEffect (ResourceLoader.LoadAttackParticles (currentWeapon.GetWeaponType () + "Attack"));
		SetAction (action);
		SetActionType (ActionType.Attack);
		SetActionTarget(BattleControllerScript.reference.enemies[Random.Range(
			0, BattleControllerScript.reference.enemies.Count)].gameObject);

		BattleControllerScript.reference.SendMessage ("AddCharacterActionToList", id);
	}

	public bool CalculateFleeAttempt()
	{
		return luck + (int)(speed * 0.5f) > Random.Range (0, currentLevel + 15);
	}

	public void BeginEmptyAction()
	{
		inPosition = true;
		Invoke ("EndEmptyAction", 1.0f);
	}

	private void EndEmptyAction()
	{
		inPosition = false;
		ActionComplete ();
	}

	// Getters and Setters //

	public void SetVictorious()
	{
		animator.SetTrigger ("Victory");
	}

	public bool GetCasting()
	{
		return casting;
	}

	public bool GetUsingItem()
	{
		return usingItem;
	}

	new public void SetDamaged(bool damaged)
	{
		base.SetDamaged (damaged);
	}

	public void SetUsingItem(bool usingItem)
	{
		if(usingItem && !this.usingItem)
		{
			animator.SetTrigger("Using");
			this.usingItem = true;
		}
		else if(!usingItem && this.usingItem)
		{
			this.usingItem = false;
			animator.Play("Idle");
		}
	}

	public void SetActionType(ActionType type)
	{
		currentAction.SetActionType(type);
	}

	public void SetActionTarget(GameObject target)
	{
		currentAction.SetTarget (target);
	}

	public void SetActionTargetList(List<GameObject> targetList)
	{
		currentAction.SetTargetParty (targetList);
	}

	override protected void PlayMagicEffect(ParticleSystem particleEffect)
	{
		if(particleEffect != null)
		{
			Vector3 systemPosition = gameObject.transform.localPosition;
			if(particleEffect.name.Contains("Attack"))
			{
				systemPosition.y = gameObject.GetComponent<Collider>().bounds.center.y;
				systemPosition.z += 1.0f;
			}
			else if(particleEffect.name.Equals("PrepareMagic"))
			{
				systemPosition.y += 0.25f;
			}
			else
			{
				systemPosition.y = 1.0f; 
			}
			
			battleParticleSystem = (ParticleSystem)Instantiate (particleEffect, systemPosition, particleEffect.transform.localRotation);
			battleParticleSystem.name = particleEffect.name;
			battleParticleSystem.transform.SetParent (transform);
			
			CameraManagerScript.reference.SetDelay (battleParticleSystem.main.duration * 60.0f);
			
			//battleParticleSystem.GetComponentInChildren<Light> ().enabled = true;
			battleParticleSystem.Play ();
		}
	}

	public void SetSelected(bool selected)
	{
		this.selected = selected;
		inPosition = selected;

		if (selected)
		{
			targetPosition.z = defaultPosition.z + 1.0f;
		}
		else
		{
			targetPosition.z = defaultPosition.z;
		}
	}

	new public void SetId(int id)
	{
		this.id = id;

		targetHP = maxHP;
		targetMP = currentMP;
	}

	override public int GetAttack()
	{
		return attack + (GetPhysicalWeaponDamage() / 2);
	}

	override public int GetMagicAttack()
	{
		return magicAttack + (GetMagicalWeaponDamage() / 2);
	}

	override public int GetDefence()
	{
		return defence + (GetPhysicalArmourDefence() / 2);
	}
	
	override public int GetMagicDefence()
	{
		return magicDefence + (GetMagicalArmourDefence() / 2);
	}

	override public int GetAccuracy()
	{
		return currentWeapon.GetAccuracy ();
	}

	override public int GetEvasion()
	{
		return GetArmourEvasion();
	}
}
