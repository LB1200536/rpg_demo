﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum EnemyAttackType {Bite, Punch, Whip, Slash};

public class EnemyScript : BaseBattleCharacter 
{
	private EnemyXMLParserScript statParser;
	private EnemyAttackType attackType;
	public Material transparentMaterial;
	private Vector3 originalPosition;
	private float bossDeathShakeDuration = 4.156f;
	private float bossDeathShakeStrength = 0.5f;
	private int currentTarget;
	private Item itemDrop;
	private int itemDropRate = 0;
	private Item itemSteal;
	private int itemStealRate = 0;
	private int goldDrop;
	private int accuracy;
	private int evasion;
	private int courage; // Value used to determine if an enemy is going to flee from the battle.
	private bool isBoss;

	// Use this for initialization
	new void Start () 
	{
		base.Start ();
		DatabaseHandler.LoadEnemy (this);
		//statParser = new EnemyXMLParserScript ();
		//statParser.ParseEnemy (this);

		originalPosition = transform.position;
		currentTarget = -1;
	}

	// Update is called once per frame
	new void Update () 
	{
		base.Update ();
		if(currentHP != targetHP)
		{
			TakeDamage();
		}

		if(currentHP == 0 && defeatedLerpTime >= 1.0f)
		{
			CancelInvoke("PerformDefeatedAnimation");
			BattleControllerScript.reference.AddEnemyToDeadCount(this);
			Destroy(this.gameObject);
		}

		if(ready && currentTarget == -1)
		{
			ChooseNextAction();
		}
	}
	
	new public void UpdateTurn()
	{
		if(BattleControllerScript.reference.battleFlow && !dead && !ready)
		{
			base.UpdateTurn();
			if (currentTurnTime >= TURN_TIME_MAX) 
			{
				if(!ready)
				{
					ready = true;
				}
			}
		}
	}

	private void ChooseNextAction()
	{
		currentAction = new BattleAction(this.gameObject);

		if(!currentStatus.Contains(Status.Berserk))
		{
			// Check to see if the enemy is going to flee on its next turn
			if(!PerformFleeCheck())
			{
				switch(((ActionType)Random.Range(0,2)))
				{
				case ActionType.Attack:
					CreateAttackAction ();
					break;
				case ActionType.Skill:
					if(skillList.Count > 0)
					{
						CreateSkillAction ();
					}
					else
					{
						CreateAttackAction ();
					}
					break;
				}
			}
		}
		else
		{
			CreateAttackAction ();
		}

		
		BattleControllerScript.reference.AddEnemyActionToList(currentAction);
	}

	new private void Flee()
	{
		base.Flee ();
		transform.Translate(transform.forward * Time.smoothDeltaTime * FLEE_MOVEMENT_MULTIPLIER * 3);

		if(fleeAmount >= 1.0f)
		{
			BattleControllerScript.reference.EnemyFled(this);
			ActionComplete();
			CancelInvoke();
			Destroy(this.gameObject, 0.5f);
		}
	}

	private void CreateAttackAction()
	{
		currentAction.SetActionType(ActionType.Attack);
		currentAction.SetParticleEffect(ResourceLoader.LoadAttackParticles(attackType.ToString()+"Attack"));
		currentAction.SetSoundEffect(ResourceLoader.LoadBattleSFX(attackType.ToString()+"Attack"));

		ChooseRandomActiveTarget ();
	}

	private void CreateSkillAction()
	{
		currentAction.SetActionType (ActionType.Skill);
		int skillIndex = Random.Range (0, skillList.Count - 1);

		if(skillList[skillIndex].GetMPCost() <= targetMP)
		{
			currentAction.SetSkill (skillList [skillIndex]);
			currentAction.SetParticleEffect (ResourceLoader.LoadAttackParticles (skillList[skillIndex].GetName()));
			currentAction.SetSoundEffect(ResourceLoader.LoadBattleSFX(skillList[skillIndex].GetName()));

			if(currentAction.GetSkill().GetTargetType().ToString().Contains("Group"))
			{
				ChooseActiveTargetsInGroup();
			}
			else
			{
				ChooseRandomActiveTarget();
			}

		}
		else // Can't use skill, so choose another action instead
		{
			CreateAttackAction();
		}
	}

	private void ChooseRandomActiveTarget()
	{
		bool newTargetChosen = false;
		// Ensure there are still players to choose from
		while(!newTargetChosen)
		{
			int newTarget = Random.Range(0, 4);
			if(!BattleControllerScript.reference.playerCharacters[newTarget].GetDead())
			{
				currentAction.SetTarget(BattleControllerScript.reference.playerCharacters[newTarget].gameObject);
				newTargetChosen = true;
				currentTarget = newTarget;
			}
		}
	}

	private void ChooseActiveTargetsInGroup()
	{
		List<GameObject> groupToTarget = new List<GameObject>();

		for(int i = 0; i < BattleControllerScript.reference.playerCharacters.Count; i++)
		{
			if(!BattleControllerScript.reference.playerCharacters[i].GetDead())
			{
				groupToTarget.Add(BattleControllerScript.reference.playerCharacters[i].gameObject);
			}
		}

		currentTarget = 1;
		currentAction.SetTargetParty (groupToTarget);
	}

	private void PerformDefeatedAnimation()
	{
		if(isBoss)
		{
			defeatedLerpTime += 0.0035f;
			BossDeathShake();
		}
		else
		{
			defeatedLerpTime += 0.01f;
		}

		foreach(Renderer renderer in materialRenderers)
		{
			float alpha = Mathf.SmoothStep(1.0f, 0.0f, defeatedLerpTime);
			Color newColour = Color.red;
			
			newColour.a = alpha;
			renderer.material.color = newColour;
		}
	}

	private void BossDeathShake()
	{
		if (bossDeathShakeDuration > 0)
		{
			transform.localPosition = originalPosition + Random.insideUnitSphere * bossDeathShakeStrength;
			 
			bossDeathShakeDuration -= Time.deltaTime * 1.0f;
			bossDeathShakeStrength -= Time.deltaTime * 0.09f;
		}
		else
		{
			bossDeathShakeDuration = 0f;
			transform.localPosition = originalPosition;
		}
	}

	new public void TakeDamage()
	{
		base.TakeDamage ();

		if(damaged)
		{
			SetDamaged(false);
		}

		if(targetHP <= 0)
		{
			if(!dead)
			{
				if(isBoss)
				{
					animator.SetTrigger("Dead");
					FirstFlash();
				}
				else
				{
					AudioManagerScript.reference.PlayAttackEffect("EnemyDefeated");
					StartDeathAnimation();
				}
				BattleControllerScript.reference.RemoveDeadEnemyActionFromList(this);

			}
			dead = true;
		}
	}

	private void FirstFlash()
	{
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = transform.eulerAngles;
		cameraPosition.z -= ((collider.bounds.size.z) * 1.35f);
		cameraPosition.y = collider.bounds.max.y/2;
		cameraRotation.y = 0.0f;
		FadeoutScript.reference.Flash(Color.white, FadeoutScript.PAUSE_FADEOUT, FadeoutScript.PAUSE_FADEOUT, "BossDefeatedFlash");
		//AudioManagerScript.reference.PlayAttackEffect ("BossDefeatedFlash");
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		Invoke ("SecondFlash", 0.75f);
	}

	private void SecondFlash()
	{
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = transform.eulerAngles;
		cameraPosition.z -= ((collider.bounds.size.z) * 1.3f);
		cameraPosition.y = collider.bounds.max.y/2;
		cameraPosition.x -= 3.0f;
		cameraRotation.y = 30.0f;
		FadeoutScript.reference.Flash(Color.white, FadeoutScript.PAUSE_FADEOUT, FadeoutScript.PAUSE_FADEOUT, "BossDefeatedFlash");
		//AudioManagerScript.reference.PlayAttackEffect ("BossDefeatedFlash");
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		Invoke ("ThirdFlash", 1.0f);
	}

	private void ThirdFlash()
	{
		float secondsToWait = 1.25f;
		Vector3 cameraPosition = transform.position;
		Vector3 cameraRotation = transform.eulerAngles;
		cameraPosition.z -= ((collider.bounds.size.z) * 1.25f);
		cameraPosition.y = collider.bounds.max.y/2.2f;
		cameraPosition.x += 3.0f;
		cameraRotation.y = -30.0f;
		cameraRotation.x = 15.0f;
		FadeoutScript.reference.Flash(Color.white, FadeoutScript.PAUSE_FADEOUT, FadeoutScript.PAUSE_FADEOUT, "BossDefeatedFlash");
		//AudioManagerScript.reference.PlayAttackEffect ("BossDefeatedFlash");
		CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDelay(secondsToWait * 60.0f);
		CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.003f);
		Invoke("StartDeathAnimation", secondsToWait);
	}

	private void StartDeathAnimation()
	{
		if(isBoss)
		{
			AudioManagerScript.reference.PlayAttackEffect("BossDefeated");
		}
		SetMaterialToTransparent();
		InvokeRepeating("PerformDefeatedAnimation", 0.0f, 0.0167f);
	}

	private void SetMaterialToTransparent()
	{
		foreach(Renderer renderer in materialRenderers)
		{
			if(renderer.material != transparentMaterial)
			{
				renderer.material = transparentMaterial;
			}
		}
	}

	new public void ActionComplete()
	{
		base.ActionComplete ();
		SetCasting (false);
		currentTarget = -1;
	}
	
	override protected void PlayMagicEffect(ParticleSystem particleEffect)
	{
		if(particleEffect != null)
		{
			Vector3 systemPosition = gameObject.transform.localPosition;
			systemPosition.z = gameObject.GetComponent<Collider> ().bounds.min.z;
			systemPosition.y += gameObject.GetComponent<Collider> ().bounds.center.y; 
			
			battleParticleSystem = (ParticleSystem)Instantiate (particleEffect, systemPosition, particleEffect.transform.localRotation);
			battleParticleSystem.name = particleEffect.name;
			battleParticleSystem.transform.SetParent (transform);
			CameraManagerScript.reference.SetDelay (battleParticleSystem.main.duration * 60.0f);
			
			//battleParticleSystem.GetComponentInChildren<Light> ().enabled = true;
			battleParticleSystem.Play ();
		}
	}

	private bool PerformFleeCheck()
	{
		if(!isBoss)
		{
			int fleeValue;
			fleeValue = courage - (BattleControllerScript.reference.playerCharacters[0].GetCurrentLevel() + Random.Range(0, 51));

			if(fleeValue < 80)
			{
				currentAction.SetActionType(ActionType.Run);
				currentAction.SetSoundEffect(ResourceLoader.LoadBattleSFX("Run"));
				currentTarget = 0;
				return true;
			}
		}

		return false;
	}

	override public int GetEvasion()
	{
		return evasion;
	}

	override public int GetAccuracy()
	{
		return accuracy;
	}

	public int GetCurrentTarget()
	{
		return currentTarget;
	}

	public Element GetWeakElement()
	{
		return weakElement;
	}

	public int GetGoldDrop()
	{
		return goldDrop;
	}

	public Item GetItemDrop()
	{
		return itemDrop;
	}

	public int GetItemDropRate()
	{
		return itemDropRate;
	}

	public Item GetItemSteal()
	{
		return itemSteal;
	}

	public int GetItemStealRate()
	{
		return itemStealRate;
	}

	public bool IsBoss()
	{
		return isBoss;
	}

	public EnemyAttackType GetAttackType()
	{
		return attackType;
	}

	public void SetDisplayButton(Button displayButton)
	{
		displayButton.GetComponentInChildren<Text>().text = name;
		displayButton.GetComponentInChildren<Text>().enabled = true;
	}

	public void SetWeakElement(Element element)
	{
		weakElement = element;
	}

	public void SetGoldDrop(int goldDrop)
	{
		this.goldDrop = goldDrop;
	}

	public void SetAccuracy(int accuracy)
	{
		this.accuracy = accuracy;
	}

	public void SetEvasion(int evasion)
	{
		this.evasion = evasion;
	}

	public void SetCourage(int courage)
	{
		this.courage = courage;
	}

	public void SetItemDrop(int itemDrop, int itemDropRate)
	{
		this.itemDrop = new Item(itemDrop);
		this.itemDropRate = itemDropRate;
	}

	public void SetItemSteal(int itemSteal, int itemStealRate)
	{
		if(itemSteal != -1)
		{
			if(itemSteal < 6)
			{
				this.itemSteal = new Item(itemSteal);
			}
			else if(itemSteal < 11)
			{
				this.itemSteal = new Weapon(itemSteal);
			}
			else 
			{
				this.itemSteal = new Armour(itemSteal);
			}

			this.itemStealRate = itemStealRate;
		}
		else
		{
			this.itemSteal = null;
			this.itemStealRate = 0;
		}
	}

	public void SetIsBoss(bool isBoss)
	{
		this.isBoss = isBoss;
	}

	public void SetAttackType(EnemyAttackType attackType)
	{
		this.attackType = attackType;
	}
}
