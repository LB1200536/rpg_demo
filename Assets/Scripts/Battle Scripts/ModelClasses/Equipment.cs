﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class Equipment : Item 
{
	private SerializableCharacter equippedBy = null;

	public Equipment(int id):base(id)
	{
		equipable = true;
	}

	public override bool IsEquipped()
	{
		return equippedBy != null;
	}

	public bool IsWeapon()
	{
		if(typeof(Weapon).IsInstanceOfType(this))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public bool IsArmour()
	{
		if(typeof(Armour).IsInstanceOfType(this))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public SerializableCharacter GetEquippedBy()
	{
		return equippedBy;
	}

	public void Equip(SerializableCharacter character)
	{
		equippedBy = character;
	}

	public void Unequip()
	{
		if(IsArmour())
		{
			if (((Armour)this).IsHeadgear ()) 
			{
				PartyDataHolderScript.reference.characters[equippedBy.characterId].SetHeadgear(null);
			}
			else if (((Armour)this).IsHandgear ()) 
			{
				PartyDataHolderScript.reference.characters[equippedBy.characterId].SetHandgear(null);
			}
			else if (((Armour)this).IsBodyArmour ()) 
			{
				PartyDataHolderScript.reference.characters[equippedBy.characterId].SetBodyArmour(null);
			}
		}
		else if(IsWeapon())
		{
			PartyDataHolderScript.reference.characters[equippedBy.characterId].SetWeapon(null);
		}

		equippedBy = null;
	}
}
