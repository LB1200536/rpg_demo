﻿using UnityEngine;
using System.Collections;

public class FlameScript : MonoBehaviour 
{
	Light lightObject;
	ParticleSystem system;
	// Use this for initialization
	void Start () 
	{
		lightObject = gameObject.GetComponentInChildren<Light> ();
		system = gameObject.GetComponent<ParticleSystem> ();
		AudioManagerScript.reference.PlayAttackEffect ("Flame");
		system.Play ();
		Destroy(lightObject, 1.0f); // Stops the light from flickering when the system is ending
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(system.isPlaying && lightObject)
		{
			lightObject.intensity = Random.Range (1.0f, 5.0f);
			lightObject.range = Random.Range (2.0f, 8.0f);
		}
	}
}
