﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Inventory
{
	public const int MAX_INVENTORY_SIZE = 150;
	public const int MAX_ITEM_QUANTITY = 99;
	// TEMP CONSTANTS FOR DEMO PURPOSES //
	private const int POTION_COUNT = 3;
	private const int HI_POTION_COUNT = 2;
	private const int ETHER_COUNT = 1;

	private List<Item> itemList = new List<Item>();
	private bool sortAscending;

	public Inventory(ItemXMLParserScript itemParser)
	{
		sortAscending = true;
		Item potion = new Item (0);
		potion.SetQuantity (POTION_COUNT);
		Item hiPotion = new Item (1);
		hiPotion.SetQuantity (HI_POTION_COUNT);
		Item ether = new Item (3);
		ether.SetQuantity (ETHER_COUNT);

		AddItemToInventory(potion);
		AddItemToInventory (ether);
		AddItemToInventory(hiPotion);
	}

	public List<Item> GetAllItems()
	{
		return itemList;
	}

	public Item GetItem(int index)
	{
		return itemList [index];
	}

	public void CheckItemQuantities()
	{
		for (int i = 0; i < itemList.Count; i++) 
		{
			if(itemList[i].GetQuantity() <= 0)
			{
				itemList.RemoveAt(i);
			}
		}
	}
	
	public void AddItemToInventory(Item newItem)
	{
		bool itemAdded = false;
		for(int i = 0; i < itemList.Count; i++)
		{
			if(newItem.GetUsable() && itemList[i].GetName().Equals(newItem.GetName()))
			{
				itemList[i].SetQuantity(itemList[i].GetQuantity() + newItem.GetQuantity());
				itemAdded = true;
			}
		}

		if(!itemAdded)
		{
			itemList.Add(newItem);
		}
	}

	public void DeductItemFromTotal(Item item, int deductionCount)
	{
		for(int i = 0; i < itemList.Count; i++)
		{
			if(itemList[i].GetName().Equals(item.GetName()))
			{
				itemList[i].SetQuantity(itemList[i].GetQuantity() - deductionCount);

				// Remove the item from the item list if there are no more in stock
				if(itemList[i].GetQuantity() <= 0)
				{
					itemList.RemoveAt(i);
				}
				break;
			}
		}
	}

	public void SortById()
	{
		sortAscending = !sortAscending;

		if(sortAscending)
		{
			itemList.Sort(delegate(Item x, Item y)
			              {
				return x.GetItemId ().CompareTo(y.GetItemId ());
			});
		}
		else
		{
			itemList.Sort(delegate(Item x, Item y)
			              {
				return y.GetItemId ().CompareTo(x.GetItemId ());
			});
		}
	}

	public bool CheckInventoryForItem(Item item)
	{
		return itemList.Exists (x => x.GetQuantity() > 0 && x.GetItemId() == item.GetItemId());
	}

	public bool IsSortAscending()
	{
		return sortAscending;
	}
}
