﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum ItemType {None, HP_Healing, MP_Healing, Attacking, Status_Healing};

[System.Serializable]
public class Item
{
	protected int itemId;
	private ItemType itemType;
	protected string itemName;
	private int effectAmount; // Determines the effect of the item (i.e. how much it heals, or hurts)
	protected int quantity;
	protected bool equipable;
	protected int buyAmount; // How expensive it is to purchase this item from a merchant
	protected int sellAmount; // How much money will be given to the player for selling this item;
	protected bool keyItem; // Key items cannot be sold or dropped
	protected Status status;

	public Item(int id)
	{
		itemId = id;
		//ItemXMLParserScript itemParser = new ItemXMLParserScript ();
		//itemParser.ParseItem (this);
		DatabaseHandler.LoadItem(this);
		quantity = 1;
	}

	public int GetItemId()
	{
		return itemId;
	}

	public bool GetUsable()
	{
		return !equipable;
	}

	public string GetName()
	{
		return itemName;
	}

	public ItemType GetItemType()
	{
		return itemType;
	}

	public int GetEffectAmount()
	{
		return effectAmount;
	}

	public int GetQuantity()
	{
		return quantity;
	}

	public int GetBuyAmount()
	{
		return buyAmount;
	}

	public int GetSellAmount()
	{
		return sellAmount;
	}

	public Status GetStatus()
	{
		return status;
	}

	virtual public Sprite GetMenuImage()
	{
		if(!keyItem)
		{
			return ResourceLoader.LoadSprite (itemType.ToString ());
		}

		return ResourceLoader.LoadSprite ("KeyItem");
	}

	public bool IsKeyItem()
	{
		return keyItem;
	}

	public virtual bool IsEquipped()
	{
		return false;
	}

	public void SetItemId(int id)
	{
		itemId = id;
	}

	public void SetName(string name)
	{
		itemName = name;
	}

	public void SetUsable(bool usable)
	{
		equipable = !usable;
	}

	public void SetItemType(ItemType itemType)
	{
		this.itemType = itemType;
	}

	public void SetEffectAmount(int amount)
	{
		effectAmount = amount;
	}

	public void SetQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	public void SetBuyAmount(int amount)
	{
		buyAmount = amount;
	}

	public void SetSellAmount(int amount)
	{
		sellAmount = amount;
	}

	public void SetKeyState(bool isKey)
	{
		keyItem = isKey;
	}

	public void SetStatus(Status status)
	{
		this.status = status;
	}
}
