﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum Element {None, Flame, Spark, Frost, Holy, Dark, Heal};
[System.Serializable]
public enum SkillType {BlackMagic, WhiteMagic, Thievery, Gallantry, EnemySkills};
[System.Serializable]
public enum TargetType {All, SameGroup, OtherGroup, SameSingle, OtherSingle, ToggleGroup, ToggleSingle, ToggleBoth, Self};

[System.Serializable]
public class Skill
{
	private int skillId;
	private string name;
	private int baseDamage;
	private Element element;
	private int mpCost;
	private Status associatedStatus;
	private int statusPercentageChance;
	private SkillType skillType;
	private TargetType targetType;
	private bool usableInField;

	public Skill(int id, SkillType type)
	{
		skillId = id;
		skillType = type;
		associatedStatus = Status.None;
		DatabaseHandler.LoadSkill (this);
		//SkillXMLParserScript skillParser = new SkillXMLParserScript ();
		//skillParser.ParseSkill (this, skillType);
	}

	public Skill(string name, int baseDamage, int mpCost, Element element, SkillType skillType)	
	{
		this.name = name;
		this.baseDamage = baseDamage;
		this.mpCost = mpCost;
		this.element = element;
		this.skillType = skillType;
	}

	public int GetSkillId()
	{
		return skillId;
	}

	public string GetName()
	{
		return name;
	}

	public int GetBaseDamage()
	{
		return baseDamage;
	}

	public Element GetElement()
	{
		return element;
	}

	public int GetMPCost()
	{
		return mpCost;
	}

	public SkillType GetSkillType()
	{
		return skillType;
	}

	public Status GetAssociatedStatus()
	{
		return associatedStatus;
	}

	public int GetStatusPercentageChance()
	{
		if(associatedStatus != Status.None)
		{
			return statusPercentageChance;
		}
		else
		{
			return 0;
		}
	}

	public TargetType GetTargetType()
	{
		return targetType;
	}

	public bool GetUsableInField()
	{
		return usableInField;
	}

	public void SetName(string name)
	{
		this.name = name;
	}
	
	public void SetBaseDamage(int baseDamage)
	{
		this.baseDamage = baseDamage;
	}
	
	public void SetElement(Element element)
	{
		this.element = element;
	}
	
	public void SetMPCost(int mpCost)
	{
		this.mpCost = mpCost;
	}
	
	public void SetSkillType(SkillType skillType)
	{
		this.skillType = skillType;
	}

	public void SetAssociatedStatus(Status associatedStatus)
	{
		this.associatedStatus = associatedStatus;
	}

	public void SetStatusPercentageChance(int statusPercentageChance)
	{
		this.statusPercentageChance = statusPercentageChance;
	}

	public void SetTargetType(TargetType targetType)
	{
		this.targetType = targetType;
	}

	public void SetUsableInField(bool usableInField)
	{
		this.usableInField = usableInField;
	}
}
