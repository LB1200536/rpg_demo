﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum WeaponType {BareHands, ShortSword, Staff, Rod, Dagger, Axe, Polearm, LongSword, Mace};

[System.Serializable]
public class Weapon : Equipment
{
	private int physicalDamage;
	private int magicalDamage;
	private int accuracy;
	private WeaponType type;
	private Element attackElement;

	public Weapon (int id) : base(id)
	{
		
	}

	public int GetPhysicalDamage()
	{
		return physicalDamage;
	}

	public int GetMagicalDamage()
	{
		return magicalDamage;
	}

	public WeaponType GetWeaponType()
	{
		return type;
	}

	public int GetAccuracy()
	{
		return accuracy;
	}

	public Element GetElement()
	{
		return attackElement;
	}

	public void SetPhysicalDamage(int damage)
	{
		physicalDamage = damage;
	}

	public void SetMagicalDamage(int damage)
	{
		 magicalDamage = damage;
	}

	public void SetWeaponType(WeaponType type)
	{
		this.type = type;
	}

	public void SetAccuracy(int accuracy)
	{
		this.accuracy = accuracy;
	}

	public void SetElement(Element element)
	{
		attackElement = element;
	}

	override public Sprite GetMenuImage()
	{
		return ResourceLoader.LoadSprite (type.ToString ());
	}
}
