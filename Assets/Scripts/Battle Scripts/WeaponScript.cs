﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour 
{
	private Animator animator;
	bool attacking;
	bool casting;
	private WeaponType type;
	private Vector3 defaultPosition;
	private Quaternion defaultRotation;
	
	void Awake()
	{
		animator = GetComponent<Animator> ();
		attacking = false;
	}

	void Start()
	{
		defaultPosition = gameObject.transform.localPosition;
		defaultRotation = gameObject.transform.localRotation;
	}

	public void SetAttacking(bool attacking)
	{
		//animator.SetBool ("Attacking", attacking);

		//Debug.Log ("Attacking set to " + attacking);

		if(attacking && !this.attacking)
		{
			animator.rootRotation = new Quaternion(1.0f, 0.0f, 0.0f, 1.0f);
			animator.Play("SwordAttack");
			this.attacking = attacking;
		}
		else if(!attacking && this.attacking)
		{
			this.attacking = attacking;
		}
	}

	public void SetCasting(bool casting)
	{
		if(casting && !this.casting)
		{
			animator.rootPosition = new Vector3(defaultPosition.x + 0.6f, defaultPosition.y, defaultPosition.z + 1.8f);
			animator.rootRotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
			animator.SetBool("Attacking", false);
			animator.Play("Casting");
			this.casting = casting;
		}
		else if(!casting && this.casting)
		{
			Vector3 tempPosition = defaultPosition;
			tempPosition.z += 1.0f;
			animator.rootPosition = tempPosition;
			animator.rootRotation = defaultRotation;
			this.casting = casting;
		}
	}

	public void SetWeaponType(WeaponType type)
	{
		this.type = type;
	}

	public void SetDefaultLocation(Vector3 position, Quaternion rotation)
	{
		defaultPosition = position;
		defaultRotation = rotation;
	}

	public WeaponType GetWeaponType()
	{
		return type;
	}

	public Vector3 GetDefaultPosition()
	{
		return defaultPosition;
	}

	public Quaternion GetDefaultRotation()
	{
		return defaultRotation;
	}

	public bool GetAttacking()
	{
		return attacking;
	}

	//public bool GetCasting()
	//{
	//	return casting;
	//}

	public void Test()
	{
		Debug.Log ("Animation Starting");
	}
}
