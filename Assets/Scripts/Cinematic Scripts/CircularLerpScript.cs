﻿using UnityEngine;
using System.Collections;

public static class CircularLerpScript
{
	const float MIN_ANGLE = 0;
	const float MAX_ANGLE = 2* Mathf.PI;
	const float DEFAULT_RATE = 0.02f;

	public static Vector2 CircularLerp(float centerX, float centerY, float radius, float lerpValue)
	{
		Vector2 returnCoordinates = new Vector2();
		float newAngle = LerpAngle (lerpValue);

		returnCoordinates.x = centerX + radius * Mathf.Cos (newAngle);
		returnCoordinates.y = centerY + radius * Mathf.Sin (newAngle);

		return returnCoordinates;
	}

	private static float LerpAngle(float value)
	{
		return MIN_ANGLE + (MAX_ANGLE - MIN_ANGLE) * value;
	}
}
