﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum ScreenTransitionType {Fadeout, Circle, Vertical, Horizontal};

public class FadeoutScript : MonoBehaviour
{
	public const float SCENE_TRANSITION_FADEOUT = 0.025f;
	public const float DEATH_FADEOUT = 0.05f;
	public const float PAUSE_FADEOUT = 0.15f;

	private Image blackout;
	private Image transitionPanel;
	private float lerpPercentage = 1.0f;
	public bool active;
	private float lerpRate;
	private float flashRate;
	private Color fadeColour; // Colour set by the user (the end colour)
	private Color lerpColour; // Interm colour used for lerping between nothing and the fadeColour
	private string finishMessage;
	private GameObject callingObject = null;
	private ScreenTransitionType currentTransitionType;

	public static FadeoutScript reference;

	void Awake()
	{
		if(!reference)
		{
			reference = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}

		currentTransitionType = ScreenTransitionType.Fadeout;
		blackout = GetComponentInChildren<Image> ();
		transitionPanel = GetComponentsInChildren<Image> ()[1];
		fadeColour = blackout.color;
		lerpRate = 0;
		finishMessage = "";
		active = true;
		blackout.enabled = false;
		transitionPanel.enabled = false;

		InvokeRepeating ("UpdateTransition", 0, 0.025f);
	}

	void UpdateTransition()
	{
		if(lerpPercentage != 1.0f)
		{
			UpdateLerpPercentage (lerpRate);
			lerpColour = fadeColour;

			switch(currentTransitionType)
			{
			case ScreenTransitionType.Fadeout:
				if (active)
				{
					FadeOut ();
				}
				else
				{
					FadeIn ();
				}
				break;
			case ScreenTransitionType.Circle:
				if (active)
				{
					CircleTransitionOut ();
				}
				else
				{
					CircleTransitionIn ();
				}
				break;
			case ScreenTransitionType.Horizontal:
				if (active)
				{
					HorizontalTransitionOut ();
				}
				else
				{
					HorizontalTransitionIn ();
				}
				break;
			case ScreenTransitionType.Vertical:
				if (active)
				{
					VerticalTransitionOut ();
				}
				else
				{
					VerticalTransitionIn ();
				}
				break;
			}
		}

		blackout.color = lerpColour;
	}

	public void Flash(Color colour, float flashInRate, float flashOutRate, string audioFileName = "")
	{
		flashRate = flashOutRate;
		StartTransition (ScreenTransitionType.Fadeout, flashInRate, colour, "EndFlash", this.gameObject);
		if (!audioFileName.Equals (""))
		{
			AudioManagerScript.reference.PlayMenuEffect (audioFileName);
		}
	}

	private void EndFlash()
	{
		EndTransition (flashRate, "", this.gameObject);
	}

	// Lerps the blackout Image into 0 alpha
	public void FadeIn()
	{
		lerpColour.a = Mathf.SmoothStep (fadeColour.a, 0.0f, lerpPercentage);
	}

	// Lerps the blackout Image into full alpha
	public void FadeOut()
	{
		lerpColour.a = Mathf.SmoothStep (0.0f, fadeColour.a, lerpPercentage);
	}

	public void CircleTransitionIn()
	{
		transitionPanel.fillMethod = Image.FillMethod.Radial360;
		transitionPanel.fillAmount = Mathf.SmoothStep (1.0f, 0.0f, lerpPercentage);
	}

	public void CircleTransitionOut()
	{
		transitionPanel.fillMethod = Image.FillMethod.Radial360;
		transitionPanel.fillAmount = Mathf.SmoothStep (0.0f, 1.0f, lerpPercentage);
	}

	public void VerticalTransitionIn()
	{
		transitionPanel.fillMethod = Image.FillMethod.Vertical;
		transitionPanel.fillAmount = Mathf.SmoothStep (1.0f, 0.0f, lerpPercentage);
	}

	public void VerticalTransitionOut()
	{
		transitionPanel.fillMethod = Image.FillMethod.Vertical;
		transitionPanel.fillAmount = Mathf.SmoothStep (0.0f, 1.0f, lerpPercentage);
	}

	public void HorizontalTransitionIn()
	{
		transitionPanel.fillMethod = Image.FillMethod.Horizontal;
		transitionPanel.fillAmount = Mathf.SmoothStep (1.0f, 0.0f, lerpPercentage);
	}
	
	public void HorizontalTransitionOut()
	{
		transitionPanel.fillMethod = Image.FillMethod.Horizontal;
		transitionPanel.fillAmount = Mathf.SmoothStep (0.0f, 1.0f, lerpPercentage);
	}

	private void UpdateLerpPercentage(float lerpRate)
	{
		lerpPercentage += lerpRate;
		if(lerpPercentage >= 1.0f)
		{
			lerpPercentage = 1.0f;
			if(callingObject != null)
			{
				//Debug.Log ("Message String: " + finishMessage);
				//Debug.Log ("Calling Object: " + callingObject.name);
				GameObject tempObject = callingObject;
				string tempMessage = finishMessage;

				callingObject.SendMessage(finishMessage, SendMessageOptions.DontRequireReceiver);

				// If the calling object and finishing messages are the same as when the last message was sent,
				// null them. The only time this won't be the case is if the finishing message wants to set up a new
				// transition
				if(tempObject.Equals(callingObject) && tempMessage.Equals(finishMessage))
				{
					callingObject = null;
					tempObject = null;
					finishMessage = null;
					tempMessage = null;
				}

				if(currentTransitionType == ScreenTransitionType.Fadeout && lerpColour.a == 0.0f)
				{
					blackout.enabled = false;
				}
				else if(currentTransitionType != ScreenTransitionType.Fadeout && transitionPanel.fillAmount == 0.0f)
				{
					transitionPanel.enabled = false;
				}
			}
		}
	}

	public void StartTransition(ScreenTransitionType transitionType, float lerpRate, Color fadeColour, string finishMessage, GameObject callingObject)
	{
		if(lerpPercentage >= 1.0f)
		{
			if(transitionType == ScreenTransitionType.Fadeout)
			{
				blackout.enabled = true;
				transitionPanel.enabled = false;
			}
			else
			{
				blackout.enabled = false;
				transitionPanel.enabled = true;
			}

			currentTransitionType = transitionType;
			active = true;
			lerpPercentage = 0.0f;
			this.lerpRate = lerpRate;
			this.fadeColour = fadeColour;
			this.finishMessage = finishMessage;
			this.callingObject = callingObject;
		}
	}

	public void EndTransition(float lerpRate, string finishMessage, GameObject callingObject)
	{
		if(lerpPercentage >= 1.0f)
		{
			if(currentTransitionType == ScreenTransitionType.Fadeout)
			{
				blackout.enabled = true;
				transitionPanel.enabled = false;
			}
			else
			{
				blackout.enabled = false;
				transitionPanel.enabled = true;
			}

			active = false;
			lerpPercentage = 0.0f;
			this.lerpRate = lerpRate;
			this.finishMessage = finishMessage;
			this.callingObject = callingObject;
		}
	}

	public bool IsBlackedOut()
	{
		if(lerpPercentage == 1.0f)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
