﻿using UnityEngine;
using System.Collections;

public class OrbitScript : MonoBehaviour 
{
	public float lerpRate = 0.0f;
	public GameObject orbitAround = null;
	public float radius = 0.0f;
	private float damage;

	private float lerpValue;
	private Vector3 startPosition;

	// Use this for initialization
	void Start () 
	{
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(lerpRate != 0)
		{
			LerpPosition ();
		}
	}

	public void SetUp(float lerpRate, GameObject orbitAround, float radius)
	{
		this.lerpRate = lerpRate;
		this.orbitAround = orbitAround;
		startPosition = orbitAround.transform.position;
		this.radius = radius;
	}

	public void SetRotationSpeed(float speed)
	{
		lerpRate = speed;
	}

	public float GetRotationSpeed()
	{
		return lerpRate;
	}

	public void SetDamage(float newDamage)
	{
		this.damage = newDamage;
	}

	public float GetDamage()
	{
		return damage;
	}

	void LerpPosition()
	{
		lerpValue += lerpRate;
		if(lerpValue > 1.0f)
		{
			lerpValue = 0;
		}

		Vector3 newPosition = CircularLerpScript.CircularLerp (orbitAround.transform.position.x, orbitAround.transform.position.z, radius, lerpValue);
		newPosition.z = newPosition.y;
		newPosition.y = startPosition.y;
		transform.position = newPosition;

		transform.LookAt (orbitAround.transform);
	}
}
