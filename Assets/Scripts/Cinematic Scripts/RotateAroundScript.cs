﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundScript : MonoBehaviour 
{
	public Vector3 vectorToRotateAround;
	private float angle = 0;
	public float speed = 0.001f;
	private Vector3 eulerAngles;

	void Start()
	{
		eulerAngles = transform.eulerAngles;
	}

	// Update is called once per frame
	void Update () 
	{
		angle += (Time.deltaTime * speed);
		eulerAngles.y = angle;
		transform.eulerAngles = eulerAngles;

		if (angle > 360)
		{
			angle = 0;
		}
	}
}
