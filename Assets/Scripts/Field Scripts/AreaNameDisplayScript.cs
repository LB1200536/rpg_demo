﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaNameDisplayScript : MonoBehaviour 
{
	const float NAME_DISPLAY_DURATION = 2.5f;
	public Text nameText;
	public Animator animator;

	public void Display(string nameString)
	{
		nameText.text = nameString;
		animator.SetBool ("Display", true);
		Invoke ("Hide", NAME_DISPLAY_DURATION);
	}

	public void Hide()
	{
		animator.SetBool ("Display", false);
	}
}
