﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine;
using UnityEngine.UI;

enum BattleTransitionEffect {None, MotionTilt, MotionZoom, MotionBoth, ScreenTilt, ScreenZoom, ScreenBoth} ;

public class CameraEffectsScript : MonoBehaviour 
{
	public RawImage battleTransitionImage;
	public Canvas transitionCanvas;
	public GameObject minimapObject;

	new private Camera camera;
	BattleTransitionEffect transitionEffect = BattleTransitionEffect.None;
	private BlurOptimized screenBlurController;
	private MotionBlur motionBlurController;
	private Twirl twirlController;
	private float blurLerpValue = 0;
	private const float BLUR_LERP_RATE = 0.005f;
	private Vector3 startingPosition;
	private Vector3 endingPosition;

	// Use this for initialization
	void Start () 
	{
		screenBlurController = GetComponent<BlurOptimized>();
		screenBlurController.enabled = false;
		motionBlurController = GetComponent<MotionBlur>();
		motionBlurController.enabled = false;
		twirlController = GetComponent<Twirl>();
		twirlController.enabled = false;
		camera = GetComponentsInChildren<Camera> ()[1];
		camera.enabled = false;
		transitionCanvas.enabled = false;

		battleTransitionImage.texture.width = Screen.width;
		battleTransitionImage.texture.height = Screen.height;
	}
	
	// Update is called once per frame
	void LateUpdate()
	{
		if(transitionEffect != BattleTransitionEffect.None)
		{
			PerformBattleEntranceEffect();
		}
	}
	
	public void BeginBattleEntrance()
	{
		VirtualControllerScript.reference.Enable (false);
		minimapObject.SetActive (false);
		Camera.main.farClipPlane = 5.0f;
		camera.Render ();
		transitionCanvas.enabled = true;

		transitionEffect = (BattleTransitionEffect)Random.Range (1, 7);
		//Debug.Log (transitionEffect.ToString () + " applied.");
		if((int)transitionEffect < 4)
		{
			motionBlurController.enabled = true;
		}
		else
		{
			screenBlurController.enabled = true;
		}
		if(transitionEffect.ToString().Contains("Tilt") ||  transitionEffect.ToString().Contains("Both"))
		{
			twirlController.enabled = true;
		}
		startingPosition = transform.position;
		endingPosition = transform.position + (transform.forward * 3.0f);
	}
	
	void PerformBattleEntranceEffect()
	{
		if(blurLerpValue < 1.0f)
		{
			blurLerpValue += BLUR_LERP_RATE;
			
			switch(transitionEffect)
			{
			case BattleTransitionEffect.MotionZoom:
				motionBlurController.blurAmount = 1.0f;
				ZoomEffect(1.2f);
				break;
			case BattleTransitionEffect.MotionTilt:
				motionBlurController.blurAmount = 1.0f;
				TiltEffect(140.0f);
				break;
			case BattleTransitionEffect.MotionBoth:
				motionBlurController.blurAmount = 1.0f;
				ZoomEffect(1.2f);
				TiltEffect(140.0f);
				break;
			case BattleTransitionEffect.ScreenZoom:
				screenBlurController.blurSize = Mathf.SmoothStep(0, 10, blurLerpValue);
				ZoomEffect(1.2f);
				break;
			case BattleTransitionEffect.ScreenTilt:
				screenBlurController.blurSize = Mathf.SmoothStep(0, 10, blurLerpValue);
				TiltEffect(140.0f);
				break;
			case BattleTransitionEffect.ScreenBoth:
				screenBlurController.blurSize = Mathf.SmoothStep(0, 10, blurLerpValue);
				ZoomEffect(1.2f);
				TiltEffect(140.0f);
				break;
			}
		}
	}
	
	void TiltEffect(float maxTiltAngle)
	{
		// Camera Tilt
		//Vector3 cameraTilt = transform.eulerAngles;
		//cameraTilt.z = Mathf.SmoothStep(0, maxTiltAngle, blurLerpValue);
		//transform.eulerAngles = cameraTilt;
		twirlController.angle = Mathf.SmoothStep (0, 120.0f, blurLerpValue * 1.25f);
	}
	
	void ZoomEffect(float maxZoomVelocity)
	{
		// Camera Zoom
		//float cameraZoomSpeed = 0.0f;
		//cameraZoomSpeed = Mathf.SmoothStep(0, maxZoomVelocity, blurLerpValue);
		//transform.position += transform.forward * Time.deltaTime * cameraZoomSpeed;

		battleTransitionImage.rectTransform.localScale = Vector2.Lerp (new Vector2(1.0f, 1.0f), new Vector2(1.5f, 1.5f), blurLerpValue);

		//transform.position = Vector3.Lerp (startingPosition, endingPosition, blurLerpValue);
	}
}
