﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;

// This script should be attached to the main camera of the scene, with the target to follow being supplied in
// the Inspector; otherwise, the script will attempt to find a 'Player' object tag.

public class CameraFollowScript : MonoBehaviour 
{
	public GameObject target;
	public Vector3 offset;

	// Use this for initialization
	void Start () 
	{
		if (target != null)
		{
			transform.LookAt (target.transform);
			transform.position = target.transform.position + offset;
		}
	}

	// Update is called once per frame
	void LateUpdate () 
	{
		transform.LookAt(target.transform);
		transform.position = target.transform.position + offset;
	}

	protected void Toggle()
	{
		this.enabled = !this.enabled;
	}
}
