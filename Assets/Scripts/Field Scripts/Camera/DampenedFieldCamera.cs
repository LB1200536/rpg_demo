﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Based on: https://code.tutsplus.com/tutorials/unity3d-third-person-cameras--mobile-11230

public class DampenedFieldCamera : MonoBehaviour 
{
	public GameObject target;
	public float damping = 1;
	public Vector3 offset;
	private CameraFollowScript undampenedTransform;

	// Use this for initialization
	void Start () 
	{
		undampenedTransform = FindObjectOfType<CameraFollowScript> ();
		offset = transform.position - target.transform.position;
		undampenedTransform.target = target;
		undampenedTransform.offset = offset;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		Vector3 desiredPosition = target.transform.position + offset;
		transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);

		//Vector3 position = new Vector3 ();
		//position.x = Mathf.SmoothStep (transform.position.x, desiredPosition.x, Time.deltaTime * damping);
		//position.y = Mathf.SmoothStep (transform.position.y, desiredPosition.y, Time.deltaTime * damping);
		//position.z = Mathf.SmoothStep (transform.position.z, desiredPosition.z, Time.deltaTime * damping);

		Quaternion lookAtRotation = Quaternion.LookRotation(target.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookAtRotation, Time.deltaTime * damping * 1.5f);
	}

	void SetTransformWithoutDampening()
	{
		transform.position = target.transform.position + offset;
		transform.rotation = Quaternion.LookRotation (target.transform.position - transform.position);
	}

	void SetOffset(Vector3 newOffset)
	{
		offset = newOffset;
		undampenedTransform.offset = offset;
	}

	protected void Toggle()
	{
		this.enabled = !this.enabled;
	}
}
