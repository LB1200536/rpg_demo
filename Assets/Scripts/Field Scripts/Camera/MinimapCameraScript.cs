﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCameraScript : MonoBehaviour 
{
	protected Vector3 OUTSIDE_CAMERA_OFFSET_POSITION;
	protected Vector3 OUTSIDE_CAMERA_OFFSET_ROTATION;
	protected Vector3 INSIDE_CAMERA_OFFSET_POSITION;
	protected Vector3 INSIDE_CAMERA_OFFSET_ROTATION;

	public Transform followTarget;
	public Vector3 cameraPositionOffset;
	protected Vector3 cameraPosition;

	// Use this for initialization
	protected void Start () 
	{
		OUTSIDE_CAMERA_OFFSET_POSITION = new Vector3 (0.0f, 20.0f, 0.0f);
		OUTSIDE_CAMERA_OFFSET_ROTATION = new Vector3 (90.0f, 0.0f, 0.0f);
		INSIDE_CAMERA_OFFSET_POSITION = new Vector3 (0.0f, 10.0f, 0.0f);
		INSIDE_CAMERA_OFFSET_ROTATION = new Vector3 (90.0f, 0.0f, 0.0f);

		// If the target isn't set by the developer in the Inspector, automatically find the 'Player' object
		if(!followTarget)
		{
			followTarget = GameObject.FindGameObjectWithTag("Player").transform;

			if(followTarget) // If a target was found
			{
				Debug.Log("Camera automatically found an object tagged 'Player'.");
			}
			else // Otherwise, a warning is issued
			{
				Debug.LogWarning("Camera could not find a target to follow. Please set a target in the Inspector, or tag a Gameobject as 'Player'.");
				followTarget = this.transform;
			}
		}

		cameraPosition = cameraPositionOffset;
		//cameraEulerAngle = transform.eulerAngles;
	}

	// Update is called once per frame
	protected void LateUpdate () 
	{
		cameraPosition = cameraPositionOffset + followTarget.localPosition;
		transform.position = cameraPosition;
	}

	protected void Toggle()
	{
		this.enabled = !this.enabled;
	}

	protected void SetCameraOffset(bool insideArea)
	{
		if(insideArea)
		{
			cameraPositionOffset = INSIDE_CAMERA_OFFSET_POSITION;
			transform.eulerAngles = INSIDE_CAMERA_OFFSET_ROTATION;
		}
		else
		{
			cameraPositionOffset = OUTSIDE_CAMERA_OFFSET_POSITION;
			transform.eulerAngles = OUTSIDE_CAMERA_OFFSET_ROTATION;
		}
	}
}
