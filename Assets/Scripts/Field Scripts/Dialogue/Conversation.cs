﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Conversation
{
	private int id;
	private bool isUnique;
	private List<Dialogue> dialogueList;
	private int itemRequirementId;
	private int itemGiftId;
	private int questGiftId;

	public Conversation (int id, bool isUnique, int itemRequirementId, int itemGiftId, int questGiftId)
	{
		this.id = id;
		this.isUnique = isUnique;
		this.itemRequirementId = itemRequirementId;
		this.itemGiftId = itemGiftId;
		this.questGiftId = questGiftId;
	}

	public int GetId()
	{
		return id;
	}
		
	public bool IsUnique()
	{
		return isUnique;
	}

	public Dialogue GetDialogue(int index)
	{
		return dialogueList [index];
	}

	public List<Dialogue> GetDialogueList()
	{
		return dialogueList;
	}

	public void SetDialogue(List<Dialogue> dialogueList)
	{
		this.dialogueList = new List<Dialogue> ();
		for (int i = 0; i < dialogueList.Count; i++)
		{
			this.dialogueList.Add (dialogueList [i]);
		}
	}
}


