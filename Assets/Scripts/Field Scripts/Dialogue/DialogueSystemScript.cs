﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class DialogueSystemScript : MonoBehaviour 
{
	private const float LOCK_TIME = 1.0f;
	private int dialogueListIndex;
	private Text dialogueText;
	private Animator canvasAnimator;
	private Canvas canvas;
	private Conversation currentConversation;
	private List<Dialogue> dialogueList;
	private string currentString;
	private float currentlockMessageTime;
	private Image buttonPrompt;
	private int charIndex = 0;
	private bool stringFinished = false;
	private float typewriterSpeed = 0.2f;
	private Action postDialogueAction;

	// Use this for initialization
	void Start () 
	{
		dialogueListIndex = -1;
		dialogueText = GetComponentInChildren<Text> ();
		canvasAnimator = GetComponentInChildren<Animator> ();
		buttonPrompt = GetComponentsInChildren<Image> () [3];
		canvas = GetComponent<Canvas> ();
	}

	void LateUpdate()
	{
		if (currentlockMessageTime > 0)
		{
			if (stringFinished)
			{
				UpdateLock ();
			}
		}
	}

	private void TypewriterText()
	{
		if (currentConversation != null)
		{
			if (charIndex < currentConversation.GetDialogue(dialogueListIndex).GetSentence().ToCharArray ().Length)
			{
				currentString += currentConversation.GetDialogue(dialogueListIndex).GetSentence().ToCharArray () [charIndex];
				dialogueText.text = currentString;
				charIndex++;
			}
			else
			{
				stringFinished = true;
				CancelInvoke ("TypewriterText");
			}
		}
		else
		{
			if (charIndex < dialogueList [dialogueListIndex].GetSentence().ToCharArray ().Length)
			{
				currentString += dialogueList [dialogueListIndex].GetSentence().ToCharArray () [charIndex];
				dialogueText.text = currentString;
				charIndex++;
			}
			else
			{
				stringFinished = true;
				CancelInvoke ("TypewriterText");
			}
		}

	}

	private void UpdateLock()
	{
		currentlockMessageTime -= Time.fixedDeltaTime;

		// The lower the lock time, the more filled the prompt becomes.
		if (dialogueListIndex == 0)
		{
			buttonPrompt.fillAmount = 1.0f - (currentlockMessageTime / LOCK_TIME);
		}
		else
		{
			buttonPrompt.fillAmount = 1.0f - (currentlockMessageTime / (LOCK_TIME * 0.5f));
		}

		if (currentlockMessageTime <= 0) // Ensure the message stays on the screen for a short time
		{
			GetComponentInChildren<Button> ().enabled = true;
			buttonPrompt.fillAmount = 1.0f; // Ensures the fill amount tops off at 1, due to the nature of using deltaTime.
		}
	}

	// Method called when the dialogue box is pressed
	public void UpdateDialogue () 
	{
		if (currentlockMessageTime <= 0)
		{
			if (dialogueListIndex == dialogueList.Count - 1)
			{
				EndDialogue ();
			}
			else if (stringFinished)
			{
				currentlockMessageTime = LOCK_TIME * 0.5f; // Lock the screen for a shorter time between consecutive dialogue strings.
				dialogueListIndex++;
				charIndex = 0;
				currentString = "";
				stringFinished = false;
				InvokeRepeating ("TypewriterText", 0, typewriterSpeed);
			}
		}
		else
		{
			if (!stringFinished)
			{
				stringFinished = true;
				dialogueText.text = dialogueList [dialogueListIndex].GetSentence();
				CancelInvoke ("TypewriterText");
			}
		}
	}

	public void StartDialogue(int conversationId, Action action = null)
	{
		dialogueList = new List<Dialogue> ();
		DatabaseHandler.LoadConversation (conversationId, ref currentConversation);
		dialogueList = currentConversation.GetDialogueList ();
		postDialogueAction = action;
		
		SetupDialogue ();
	}

	public void StartDialogue(string dialogueString, Action action = null)
	{
		dialogueList = new List<Dialogue> ();
		dialogueList.Add (new Dialogue(0, dialogueString));
		postDialogueAction = action;

		SetupDialogue ();
	}

	public void StartDialogue(List<Dialogue> dialogueList, Action action = null)
	{
		this.dialogueList = new List<Dialogue> ();
		this.dialogueList = dialogueList;
		postDialogueAction = action;

		SetupDialogue ();
	}

	private void SetupDialogue()
	{
		currentlockMessageTime = LOCK_TIME;
		//GetComponentInChildren<Button> ().enabled = false;
		VirtualControllerScript.reference.Enable (false);
		dialogueListIndex = 0;
		charIndex = 0;
		currentString = "";
		stringFinished = false;
		buttonPrompt.fillAmount = 0.0f;
		typewriterSpeed = 1 - PlayerPrefs.GetFloat ("textSpeed", 0.2f);

		canvas.enabled = true;
		canvasAnimator.SetBool ("Open", true);
		//dialogueText.text = dialogueStrings [dialogueListIndex];
		InvokeRepeating("TypewriterText", 0, typewriterSpeed);
	}

	private void EndDialogue()
	{
		dialogueListIndex = -1;
		canvasAnimator.SetBool ("Open", false);
		currentConversation = null;
		dialogueList = new List<Dialogue> ();
		dialogueText.text = "";
		VirtualControllerScript.reference.Enable (true);

		if (postDialogueAction != null)
		{
			postDialogueAction ();
			postDialogueAction = null;
		}

		canvas.enabled = false;
	}
}
