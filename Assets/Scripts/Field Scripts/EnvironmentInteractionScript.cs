﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentInteractionScript : MonoBehaviour 
{
	public AudioSource audioSource;
	public Transform leftFoot;
	public Transform rightFoot;

	private AudioClip footstepClip;
	private ParticleSystem footstepParticles;
	private Vector3 characterRotation;

	void Start()
	{
		characterRotation = transform.parent.eulerAngles;
		footstepClip = ResourceLoader.LoadFieldSFX ("Footstep");
		footstepParticles = (ParticleSystem)Instantiate (ResourceLoader.LoadParticleSystem ("Footsteps"));
	}

	void Update()
	{
		characterRotation = transform.parent.eulerAngles;
	}

	public void WalkStep(int foot)
	{
		audioSource.volume = 0.3f;

		SetParticleFoot (foot);
		Footstep ();
	}

	public void RunStep(int foot)
	{
		audioSource.volume = 0.6f;

		SetParticleFoot (foot);
		Footstep ();
	}

	public void Footstep()
	{
		PlayFootstepAudio ();
		//PlayFootstepParticleSystem ();
	}

	private void PlayFootstepAudio()
	{
		if(audioSource != null && footstepClip != null)
		{
			audioSource.clip = footstepClip;
			audioSource.Play ();
		}

		audioSource.pitch = Random.Range (0.7f, 1.3f);
	}

	private void PlayFootstepParticleSystem()
	{
		if(footstepParticles != null)
		{
			footstepParticles.Play ();
		}
	}

	private void SetParticleFoot(int foot)
	{
		footstepParticles.transform.eulerAngles = characterRotation;

		if(foot == 0) // Left foot
		{
			footstepParticles.transform.localPosition = new Vector3 (leftFoot.position.x, leftFoot.position.y + 0.25f, leftFoot.position.z);
		}
		else if(foot == 1) // Right foot
		{
			footstepParticles.transform.localPosition = new Vector3 (rightFoot.position.x, rightFoot.position.y + 0.25f, rightFoot.position.z);
		}
	}
}
