﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/*
 * Author: Liam Burns ( based on code from 'taoa' : 
 * http://answers.unity3d.com/questions/46745/how-do-i-find-the-frames-per-second-of-my-game.html )
 * Date of Creation: 18/8/16
 * Description: Object used to calculate and display the last known framerate of the application. The object will remain 
 * in existance until the application is closed, meaning only one of these objects needs to exist.
 * How To Use: Simply place this object into the first scene of your application, and the counter will do the rest.
*/

public class FPSCounterScript : MonoBehaviour 
{
	int frameCounter = 0;
	float timeCounter = 0.0f;
	float lastFramerate = 0.0f;
	public float refreshTime = 0.5f; // By default, refreshes every half-second
	private Text display;

	void Awake()
	{
		if(GameObject.Find("FPS Counter") != this.gameObject)
		{
			Destroy(this.gameObject);
		}
		else
		{
			DontDestroyOnLoad (this);
		}
	}

	void Start()
	{
		display = GetComponentInChildren<Text> (); // Automatically finds the Text component of the object
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( timeCounter < refreshTime )
		{
			timeCounter += Time.deltaTime;
			frameCounter++;
		}
		else
		{
			//This code will break if you set your refreshTime to 0, which makes no sense.
			lastFramerate = (float)frameCounter/timeCounter;
			frameCounter = 0;
			timeCounter = 0.0f;
		}

		display.text = "FPS: "+ lastFramerate.ToString("0");
	}

	public void ToggleDisplay()
	{
		display.enabled = !display.enabled;
	}
}
