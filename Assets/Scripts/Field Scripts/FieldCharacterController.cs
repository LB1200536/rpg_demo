﻿using UnityEngine;
using System.Collections;

public class FieldCharacterController : MonoBehaviour 
{
	private const float WALK_SPEED = 2.5f;
	private const float MAXIMUM_WALK_VELOCITY = 20.0f;

	private const float MAXIMUM_MOVEMENT_TIME_BETWEEN_BATTLES = 10.0f;
	private const float MINIMUM_MOVEMENT_TIME_BETWEEN_BATTLES = 6.0f;

	Rigidbody playerPhysicsBody;
	Vector3 playerEulerAngles;
	Vector3 targetDirection;
	Vector3 transformedCameraForward;
	Vector3 transformedCameraRight;
	Vector3 forceToApplyPerFrame;
	Animator animator;

	protected Canvas prompt;

	private GameObject activeSavePoint;
	private FieldControllerScript fieldController;
	private CameraFollowScript offsetObject;
	private bool jumping = false;
	private Vector2 lastPosition; // x-value = xPosition, y-value = zPosition
	private LayerMask interactableMask;

	public GameObject characterModel;
	public float movementTimeBetweenBattles;
	public DecisionPromptScript decisionPrompt;

	// Use this for initialization
	void Start () 
	{
		ResetTimeBetweenEncounters ();
		characterModel = this.transform.GetChild (0).gameObject;
		playerPhysicsBody = GetComponent<Rigidbody> ();
		animator = GetComponentInChildren<Animator> ();
		transform.position = PartyDataHolderScript.reference.lastPosition;
		transform.eulerAngles = PartyDataHolderScript.reference.lastRotation;
		playerEulerAngles = transform.eulerAngles;
		prompt = GameObject.Find("Prompt").GetComponent<Canvas> ();
		prompt.enabled = false;

		interactableMask = 1 << LayerMask.NameToLayer ("Interactable");

		fieldController = GameObject.Find ("FieldController").GetComponent<FieldControllerScript>();
		offsetObject = GameObject.FindObjectOfType<CameraFollowScript> ();

		InstantiateWeapon ();
		SetCameraDirection ();
	}
		
	// Update is called once per frame
	void Update () 
	{
		if (VirtualControllerScript.reference.enabled) 
		{
			if(VirtualControllerScript.reference.useAnalogue)
			{
				MoveWithAnalogueStick();
			}
			else
			{
				MoveWithDPad();
			}
			animator.SetFloat ("VerticalVelocity", playerPhysicsBody.velocity.y);

			SmoothRotateCharacter();

			if (VirtualControllerScript.reference.AButtonPressed ()) 
			{
				if(activeSavePoint)
				{
					if (activeSavePoint.GetComponent<SavePointScript> ()) 
					{
						prompt.enabled = false;
						AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
						activeSavePoint.GetComponent<SavePointScript> ().DisplayPrompt ();
					}
				}
				else
				{
					UpdateInteraction ();
				}
			}
		
			if (VirtualControllerScript.reference.BButtonPressed ())
			{
				if (!jumping) 
				{
					jumping = true;
					Invoke("Jump", 0.24f);
					animator.Play("Jump");
				}
			}
		}

		CheckForWaterBoundary ();

		if (movementTimeBetweenBattles <= 0)
		{
			if (fieldController.GetCurrentArea ().HasEncounters ())
			{
				playerPhysicsBody.Sleep ();
				animator.enabled = false;
				// FIXME: Broadcast to all animated objects in the scene to stop playing
				fieldController.StartRandomBattleTransition ();
			}
		
			ResetTimeBetweenEncounters ();
		}
	}

	void UpdateInteraction()
	{
		GameObject interactableObject = CheckForInteractableObject ();
		if (interactableObject) 
		{
			//Debug.Log ("Interacted with " + interactableObject.name);
			prompt.enabled = false;
			if(interactableObject.GetComponentInChildren<Door> ())
			{
				if (interactableObject.GetComponentInChildren<Door> ().associatedAreaId != -1) 
				{
					if(!interactableObject.GetComponentInChildren<Door>().IsLocked())
					{
						fieldController.SendMessage ("EnableMiniMap", false);
						FadeoutScript.reference.StartTransition (ScreenTransitionType.Circle, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black, "DestroyCurrentArea", fieldController.gameObject);
						VirtualControllerScript.reference.Enable (false);
					}

					interactableObject.GetComponentInChildren<Interactable> ().Interact (this.gameObject);
				} 
				else 
				{
					GameObject.FindGameObjectWithTag("GameController").SendMessage("StartRandomBattleTransition");
				}
			}
			else
			{
				interactableObject.GetComponentInChildren<Interactable> ().Interact (this.gameObject);
			}
		} 
	}


	void FixedUpdate()
	{
		if((playerPhysicsBody.velocity.x < WALK_SPEED && playerPhysicsBody.velocity.x > -WALK_SPEED) && 
			(playerPhysicsBody.velocity.z < WALK_SPEED && playerPhysicsBody.velocity.z > -WALK_SPEED))
		{
			forceToApplyPerFrame.x = Mathf.Clamp(forceToApplyPerFrame.x, -MAXIMUM_WALK_VELOCITY, MAXIMUM_WALK_VELOCITY);
			forceToApplyPerFrame.z = Mathf.Clamp(forceToApplyPerFrame.z, -MAXIMUM_WALK_VELOCITY, MAXIMUM_WALK_VELOCITY);

			playerPhysicsBody.AddForce (forceToApplyPerFrame, ForceMode.Force);
		}
	}

	private void SetCameraDirection()
	{
		transformedCameraForward = offsetObject.transform.TransformDirection(Vector3.forward);
		transformedCameraForward.y = 0.0f;
		transformedCameraForward.Normalize ();

		transformedCameraRight = offsetObject.transform.TransformDirection(Vector3.right);
		transformedCameraRight.y = 0.0f;
		transformedCameraRight.Normalize ();
	}
	
	private void Jump()
	{
		AudioManagerScript.reference.PlayFieldEffect ("Jump");
		playerPhysicsBody.AddForce (new Vector3 (0, 15.0f, 0), ForceMode.VelocityChange);
	}

	private void MoveWithDPad()
	{
		if (VirtualControllerScript.reference.UpButtonPressed ())
		{
			targetDirection = transformedCameraForward;
		}
		else if (VirtualControllerScript.reference.DownButtonPressed ())
		{
			targetDirection = -transformedCameraForward;
		}
		else if (VirtualControllerScript.reference.LeftButtonPressed ())
		{
			targetDirection = -transformedCameraRight;
		}
		else if (VirtualControllerScript.reference.RightButtonPressed ())
		{
			targetDirection = transformedCameraRight;
		}

		if (!VirtualControllerScript.reference.AnyDirectionButtonDown ()) 
		{
			characterModel.transform.localPosition = Vector3.zero;
			animator.SetBool ("Walking", false);
			SetCameraDirection ();
			forceToApplyPerFrame = Vector3.zero;
		} 
		else 
		{
			
			//Quaternion newRotation = Quaternion.LookRotation(targetDirection);
			//transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5.5f);
			forceToApplyPerFrame = targetDirection * 10.0f;

			animator.SetBool ("Walking", true);
			animator.SetFloat("AnalogueWalkStrength", 1.0f);
			movementTimeBetweenBattles -= Time.deltaTime;
		}
	}

	private void MoveWithAnalogueStick()
	{
		if(VirtualControllerScript.reference.GetAnalogueTiltStrength() > 25)
		{
			//playerPhysicsBody.gameObject.transform.Translate(Vector3.forward *
				//(0.06f * (VirtualControllerScript.reference.GetAnalogueTiltStrength () * 0.01f)));

			Quaternion newRotation = Quaternion.AngleAxis(VirtualControllerScript.reference.GetAnalogueStickRotation() - 90, Vector3.up);
			//transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5.0f);

			forceToApplyPerFrame = (transform.forward * (VirtualControllerScript.reference.GetAnalogueTiltStrength () * 0.01f)) * 30.0f;

			animator.SetBool ("Walking", true);
			animator.SetFloat("AnalogueWalkStrength", VirtualControllerScript.reference.GetAnalogueTiltStrength() * 0.01f);

			movementTimeBetweenBattles -= Time.deltaTime;
		}
		else 
		{
			SetCameraDirection ();
			characterModel.transform.localPosition = Vector3.zero;
			forceToApplyPerFrame = Vector3.zero;
			animator.SetBool ("Walking", false);
			animator.SetFloat("AnalogueWalkStrength", 0);
		}
	}

	void SmoothRotateCharacter()
	{
		if (VirtualControllerScript.reference.IsEnabled())
		{
			if(VirtualControllerScript.reference.useAnalogue && VirtualControllerScript.reference.GetAnalogueTiltStrength() > 20.0f)
			{
				Quaternion newRotation = Quaternion.AngleAxis(VirtualControllerScript.reference.GetAnalogueStickRotation() - 90, Vector3.up);
				transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5.0f);
			}
			else
			{
				Quaternion newRotation = Quaternion.LookRotation(targetDirection);
				transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 5.5f);
			}
		}
	}

	GameObject CheckForInteractableObject()
	{
		RaycastHit hit;
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Vector3 pos = transform.position;
		pos.y += 1.0f;
		
		Debug.DrawRay (pos, fwd, Color.magenta, 0.75f);
		if (Physics.Raycast (pos, fwd, out hit, 1.5f, interactableMask.value))
		{
			if(hit.transform.gameObject.tag.Equals("Interactable"))
			{
				return hit.transform.gameObject;
			}
		}
		
		return null;
	}

	void CheckForWaterBoundary()
	{
		RaycastHit hit;
		Vector3 fwd = transform.TransformDirection(new Vector3(0, -1, 1));
		Vector3 pos = transform.position;
		
		//Debug.DrawRay (pos, fwd, Color.magenta, 0.1f);
		if (Physics.Raycast (pos, fwd, out hit, 10.0f))
		{
			if(hit.transform.gameObject.tag.Equals("Water"))
			{
				Vector3 newPosition = transform.position;
				newPosition.x = lastPosition.x;
				newPosition.z = lastPosition.y;
				transform.position = newPosition;
				return;
			}
		}

		lastPosition.x = transform.position.x;
		lastPosition.y = transform.position.z;
	}

	public void InstantiateWeapon()
	{
		if(PartyDataHolderScript.reference.characters[0].GetCurrentWeapon() != null)
		{
			Transform[] modelJoints = GetComponentsInChildren<Transform> ();
			
			foreach(Transform joint in modelJoints)
			{
				if(joint.gameObject.name.Equals("WeaponHolster"))
				{
					Transform holster = joint.gameObject.transform;
					
					if(holster.transform.childCount > 0) // If the holster already has a weapon
					{
						Destroy(holster.transform.GetChild(0).gameObject);
					}

					GameObject temp;
					temp = (GameObject)Instantiate (ResourceLoader.LoadWeapon(PartyDataHolderScript.reference.characters[0].GetCurrentWeapon().GetName()), new Vector3(0.0f, 0.0f, 0.0f), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f)) as GameObject;
					temp.transform.parent = holster;
					temp.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
					temp.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
				}
			}
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if(collision.collider.gameObject.layer == LayerMask.NameToLayer("World") || 
		   collision.collider.gameObject.layer == LayerMask.NameToLayer("Terrain"))
		{
			if(transform.position.y > collision.collider.gameObject.transform.position.y)
			{
				if(jumping)
				{
					AudioManagerScript.reference.PlayFieldEffect("Land");
					animator.Play("Land");
					jumping = false;
					//this.transform.GetChild(0).gameObject.transform.position = new Vector3(0,0,0);
				}
			}
		}
	}
	void OnCollisionExit(Collision collision)
	{
		if(collision.collider.gameObject.layer == LayerMask.NameToLayer("World") || 
		   collision.collider.gameObject.layer == LayerMask.NameToLayer("Terrain"))
		{
			if(transform.position.y > collision.collider.bounds.max.y)
			{
				if(!jumping && playerPhysicsBody.velocity.y < -0.1f)
				{
					//AudioManagerScript.reference.PlayFieldEffect("Land");
					animator.Play("Fall");
					jumping = true;
					//this.gameObject.GetComponentsInChildren<Transform>()[0].position = new Vector3(0,0,0);
				}
			}
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name.Equals("SavePoint"))
		{
			activeSavePoint = other.gameObject;
			EnablePrompt();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.name.Equals("SavePoint"))
		{
			activeSavePoint = null;
			DisablePrompt();
		}
	}

	public void EnablePrompt()
	{
		if(!FadeoutScript.reference.active)
		{
			prompt.enabled = true;
		}
	}

	public void DisablePrompt()
	{
		prompt.enabled = false;
	}

	void ResetTimeBetweenEncounters()
	{
		movementTimeBetweenBattles = Random.Range (MINIMUM_MOVEMENT_TIME_BETWEEN_BATTLES, MAXIMUM_MOVEMENT_TIME_BETWEEN_BATTLES);
	}
}
