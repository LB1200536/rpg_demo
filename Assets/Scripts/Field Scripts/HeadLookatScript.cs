﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadLookatScript : MonoBehaviour 
{
	public Transform headTransform;
	private Quaternion initialHeadRotation;
	private const float MAX_LOOKAT_ANGLE = 60.0f;
	Vector3 lookatPosition;

	// Use this for initialization
	void Start () 
	{
		initialHeadRotation = headTransform.rotation;
		Vector3 eulerRotation = initialHeadRotation.eulerAngles;
		eulerRotation.y = 0;
		initialHeadRotation = Quaternion.Euler (eulerRotation);
		lookatPosition = new Vector3 ();
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		if (lookatPosition == Vector3.zero)
		{
			Quaternion newQuaternion = new Quaternion ();
			newQuaternion.SetLookRotation (transform.forward, -Vector3.up);
			headTransform.rotation = Quaternion.Slerp (headTransform.rotation, newQuaternion, Time.deltaTime * 4.0f);
		}
		else
		{
			Quaternion lookRotation = Quaternion.LookRotation (lookatPosition - headTransform.position, -transform.up);
			//Quaternion newQuaternion = new Quaternion ();
			//newQuaternion.SetLookRotation (transform.forward, transform.up);
			//Quaternion currentRotation = lookRotation * newQuaternion;
			//headTransform.rotation = lookRotation * initialHeadRotation;
			//ClampLookatRotation(MAX_LOOKAT_ANGLE)
			headTransform.rotation = Quaternion.Slerp(headTransform.rotation, lookRotation, Time.deltaTime * 5.0f);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<Interactable>() != null)
		{
			CheckForAxisRotation (other.gameObject.GetComponent<Interactable>().GetLookatTransform().position);
		}

		if(other.gameObject.CompareTag ("Player"))
		{
			CheckForAxisRotation (other.gameObject.GetComponent<HeadLookatScript>().headTransform.position);
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.GetComponent<Interactable>() != null)
		{
			CheckForAxisRotation (other.gameObject.GetComponent<Interactable>().GetLookatTransform().position);
		}

		if(other.gameObject.CompareTag ("Player"))
		{
			CheckForAxisRotation (other.gameObject.GetComponent<HeadLookatScript>().headTransform.position);
		}
	}

	Quaternion ClampLookatRotation(float maximumAngle)
	{
		Vector3 eulerRotation = headTransform.localRotation.eulerAngles;


		if((eulerRotation.x > maximumAngle && eulerRotation.x < 360-maximumAngle) 
		   || (eulerRotation.y > maximumAngle && eulerRotation.y < 360-maximumAngle))
		{
			eulerRotation.x = 0;
			eulerRotation.y = 0;
			eulerRotation.z = 0;
		}

		//eulerRotation.z = 0;

		return Quaternion.Euler (eulerRotation);
	}

	void CheckForAxisRotation(Vector3 targetPosition)
	{
		Vector3 targetDir = targetPosition - headTransform.position;
		Debug.DrawRay (headTransform.position, targetDir, Color.red);

		//lookatPosition = targetPosition;	

		Vector3 forward = transform.TransformDirection(Vector3.forward);
		Vector3 toOther = targetPosition - headTransform.position;

		// Dot product result= -1 -> 1 ('behind' -> 'in front')
		if (Vector3.Dot (forward, toOther) > 0.2f)
		{
			lookatPosition = targetPosition;
		}
		else
		{
			lookatPosition = Vector3.zero;
		}
	}

	void OnTriggerExit(Collider other)
	{
		ResetLookatPosition ();
	}

	public void ResetLookatPosition()
	{
		lookatPosition = Vector3.zero;
	}
}
