﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;


public class PointerListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
	[HideInInspector]
	public bool pressed = false;
	public bool down = false;
	private float timePressed = 0;
	private int currentTouchId = -1;

	void Update()
	{
		if(pressed)
		{
			if(timePressed < Time.time)
			{
				down = true;
				pressed = false;
			}
		}
	}
	
	public void OnPointerDown(PointerEventData eventData)
	{
		pressed = true;
		timePressed = Time.time;
		currentTouchId = eventData.pointerId;

		Touch[] touches = Input.touches;
		if(touches.Length > 0)
		{
			currentTouchId = touches [touches.Length - 1].fingerId;
		}
		
		//Debug.Log ("Touch id = " + currentTouchId.ToString ());
	}
	
	public void OnPointerEnter(PointerEventData eventData)
	{
		pressed = true;
		timePressed = Time.time;
	}
	
	public void OnPointerExit(PointerEventData eventData)
	{
		down = false;
		pressed = false;
		//currentTouchId = -1;
	}
	
	public void OnPointerUp(PointerEventData eventData)
	{
		down = false;
		pressed = false;
		currentTouchId = -1;
	}

	public int GetTouchId()
	{
		return currentTouchId;
	}
}
