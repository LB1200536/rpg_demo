﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class VirtualControllerScript : MonoBehaviour 
{
	public bool useAnalogue = false;
	List<Button> buttons; // For applying changes to all buttons easily
	Button up, down, left, right, a, b, start, select, analogueStick; // For checking individual buttons
	PointerListener analogueListener;
	Image analogueStickCenter;
	Canvas canvas;
	float analogueStickDistanceFromCenter = 0.0f;

	const float ANALOGUE_RADIUS = 100.0f;

	public static VirtualControllerScript reference;

	void Awake()
	{
		if(!reference)
		{
			reference = this;
			//DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}

		canvas = GetComponent<Canvas> ();
		AssignButtons ();
	}

	// Use this for initialization
	void Start () 
	{
		SetTransparency (0.5f);

		canvas.worldCamera = Camera.main;
		Input.simulateMouseWithTouches = true;

		ActivateChosenControls ();
	}

	void Update()
	{
		if(useAnalogue)
		{
			if(analogueListener.GetTouchId() != -1)
			{
				foreach(Touch touch in Input.touches)
				{
					if(touch.fingerId == analogueListener.GetTouchId())
					{
						CalculateDistanceBetweenTouchAndAnalogueStickCenter(touch.position);
						if(analogueStickDistanceFromCenter < ANALOGUE_RADIUS)
						{
							analogueStick.transform.position = touch.position;
						}
						else
						{
							KeepAnalogueStickWithinARadius(touch.position, analogueStickCenter.transform.position);
						}

						CalculateDistanceBetweenTouchAndAnalogueStickCenter(analogueStick.transform.position);
						break;
					}
				}
			}
			else
			{
				analogueStick.transform.position = analogueStickCenter.transform.position;
				CalculateDistanceBetweenTouchAndAnalogueStickCenter(analogueStick.transform.position);
			}
		}
	}

	private void CalculateDistanceBetweenTouchAndAnalogueStickCenter(Vector2 touchPosition)
	{
		// Using the distance of a line formula, calculates the distance between a given touch and the center of the
		// analogue stick default position.
		analogueStickDistanceFromCenter = Mathf.Sqrt(((touchPosition.x - analogueStickCenter.transform.transform.position.x) * 
		                   (touchPosition.x - analogueStickCenter.transform.transform.position.x)) + 
		                  ((touchPosition.y - analogueStickCenter.transform.transform.position.y) * 
		 					(touchPosition.y - analogueStickCenter.transform.transform.position.y)));
	}

	private void KeepAnalogueStickWithinARadius(Vector2 touchPosition, Vector2 centerPoint)
	{
		// Help found here: http://stackoverflow.com/questions/1549909/intersection-on-circle-of-vector-originating-inside-circle 
		Vector2 vectorBetweenPoints = new Vector2 ();
		vectorBetweenPoints.x = touchPosition.x - centerPoint.x;
		vectorBetweenPoints.y = touchPosition.y - centerPoint.y;

		float a = vectorBetweenPoints.x * vectorBetweenPoints.x + vectorBetweenPoints.y * vectorBetweenPoints.y;
		float b = 2 * (vectorBetweenPoints.x * (touchPosition.x - centerPoint.x) 
		               + vectorBetweenPoints.y * (touchPosition.y - centerPoint.y));

		// C is the direction and distance of the line vector
		float c = (vectorBetweenPoints.x * vectorBetweenPoints.x) + 
			(vectorBetweenPoints.y * vectorBetweenPoints.y) - (ANALOGUE_RADIUS * ANALOGUE_RADIUS);

		// Calculate the discriminant using the quadratic formula
		float disc = (b * b) - (4 * a * c);

		// Only use a positive resultant from the quadratic equation
		if (disc >= 0) 
		{
			Vector2 newPosition = new Vector2 ();
			float t = (-b + (float)Mathf.Sqrt(disc)) / (2 * a);
			newPosition.x = touchPosition.x + vectorBetweenPoints.x * t;
			newPosition.y = touchPosition.y + vectorBetweenPoints.y * t;
			analogueStick.transform.position = newPosition;
		}
	}

	public float GetAnalogueStickRotation()
	{
		float angle = ((Mathf.Atan2(-(analogueStick.transform.position.x - analogueStickCenter.transform.position.x), 
		                            -(analogueStick.transform.position.y - analogueStickCenter.transform.position.y))) 
		               * (180/Mathf.PI) - 90);

		return angle < 0 ? 360 + angle : angle;  // Ensure positive angle

		//return degrees;
	}

	private void AssignButtons()
	{
		buttons = GetComponentsInChildren<Button> ().ToList ();
		foreach(Button button in buttons)
		{
			switch(button.name)
			{
			case "Up Button":
				up = button;
				break;
			case "Down Button":
				down = button;
				break;
			case "Left Button":
				left = button;
				break;
			case "Right Button":
				right = button;
				break;
			case "A Button":
				a = button;
				break;
			case "B Button":
				b = button;
				break;
			case "Start Button":
				start = button;
				break;
			case "Select Button":
				select = button;
				break;
			case "Analogue Stick":
				analogueStick = button;
				analogueListener = analogueStick.GetComponent<PointerListener>();
				analogueStickCenter = analogueStick.transform.parent.GetComponentInChildren<Image>();
				break;
			}
		}
	}

	private void ActivateChosenControls()
	{
		int analogueSetting = PlayerPrefs.GetInt ("useAnalogue", 1);
		if(analogueSetting == 0)
		{
			useAnalogue = true;
		}
		else
		{
			useAnalogue = false;
		}

		analogueStick.transform.parent.gameObject.SetActive(useAnalogue);
		up.gameObject.SetActive(!useAnalogue);
		down.gameObject.SetActive(!useAnalogue);
		left.gameObject.SetActive(!useAnalogue);
		right.gameObject.SetActive(!useAnalogue);
	}

	public void Enable(bool enable)
	{
		canvas.enabled = enable;
		if (enable)
		{
			ActivateChosenControls ();
		}
	}

	public void DisableControl(string name)
	{
		GameObject control = GameObject.FindWithTag (name);

		if(control)
		{
			control.SetActive(false);
		}
	}

	public void EnableControl(string name)
	{
		GameObject control = GameObject.FindWithTag (name);
		
		if(control)
		{
			control.SetActive(true);
		}
	}

	public void SetTransparency(float transparency)
	{
		foreach(Button button in buttons)
		{
			Color normalColour = button.colors.normalColor;
			Color pressedColour = button.colors.pressedColor;
			Color highlightedColour = button.colors.highlightedColor;

			ColorBlock colourBlock = button.colors;

			normalColour.a = transparency;
			pressedColour.a = transparency;
			highlightedColour.a = transparency;

			colourBlock.normalColor = normalColour;
			colourBlock.pressedColor = pressedColour;
			colourBlock.highlightedColor = highlightedColour;

			button.colors = colourBlock;
		}
	}

	// Check for analogue stick tilt strength
	public float GetAnalogueTiltStrength()
	{
		return Mathf.Clamp(analogueStickDistanceFromCenter, 0, 100.0f);
	}

	public bool IsEnabled()
	{
		return canvas.enabled;
	}

	// Check for 'press' //
	public bool UpButtonPressed()
	{
		return up.GetComponent<PointerListener> ().pressed;
	}

	public bool DownButtonPressed()
	{
		return down.GetComponent<PointerListener> ().pressed;
	}

	public bool LeftButtonPressed()
	{
		return left.GetComponent<PointerListener> ().pressed;
	}

	public bool RightButtonPressed()
	{
		return right.GetComponent<PointerListener> ().pressed;
	}

	public bool AButtonPressed()
	{
		return a.GetComponent<PointerListener> ().pressed;
	}

	public bool BButtonPressed()
	{
		return b.GetComponent<PointerListener> ().pressed ;
	}

	public bool StartButtonPressed()
	{
		return start.GetComponent<PointerListener> ().pressed;
	}
	
	public bool SelectButtonPressed()
	{
		return select.GetComponent<PointerListener> ().pressed;
	}

	public bool AnyDirectionButtonPressed()
	{
		return up.GetComponent<PointerListener> ().pressed | down.GetComponent<PointerListener> ().pressed |
			left.GetComponent<PointerListener> ().pressed | right.GetComponent<PointerListener> ().pressed;

	}

	// Check for 'down' //
	public bool UpButtonDown()
	{
		return up.GetComponent<PointerListener> ().down;
	}
	
	public bool DownButtonDown()
	{
		return down.GetComponent<PointerListener> ().down;
	}
	
	public bool LeftButtonDown()
	{
		return left.GetComponent<PointerListener> ().down;
	}
	
	public bool RightButtonDown()
	{
		return right.GetComponent<PointerListener> ().down;
	}
	
	public bool AButtonDown()
	{
		return a.GetComponent<PointerListener> ().down;
	}
	
	public bool BButtonDown()
	{
		return b.GetComponent<PointerListener> ().down;
	}
	
	public bool StartButtonDown()
	{
		return start.GetComponent<PointerListener> ().down;
	}
	
	public bool SelectButtonDown()
	{
		return select.GetComponent<PointerListener> ().down;
	}
	
	public bool AnyDirectionButtonDown()
	{
		return up.GetComponent<PointerListener> ().down | down.GetComponent<PointerListener> ().down |
			left.GetComponent<PointerListener> ().down | right.GetComponent<PointerListener> ().down;
		
	}
}
