﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuPopUpScript : MonoBehaviour 
{
	protected Text message;
	protected Canvas canvas;
	protected Animator canvasAnimator;

	// Use this for initialization
	protected void Start () 
	{
		message = GetComponentInChildren<Text> ();
		canvas = GetComponent<Canvas> ();
		canvasAnimator = GetComponentInChildren<Animator> ();
		canvas.enabled = false;
	}

	void OpenWithMessage(string text)
	{
		canvas.enabled = true;
		message.enabled = true;
		canvasAnimator.SetBool("Open", true);
		message.text = text;
	}

	protected void OpenWithoutMessage()
	{
		canvas.enabled = true;
		message.enabled = true;
		canvasAnimator.SetBool("Open", true);
	}

	void DelayedOpenWithMessage(string text)
	{
		message.text = text;
		message.enabled = false;
		Invoke ("OpenWithoutMessage", 0.02f);
	}

	void Close()
	{
		canvasAnimator.SetBool("Open", false);
		message.enabled = false;
		Invoke ("DisablePopUp", 0.3f);
	}

	protected void DisablePopUp()
	{
		canvas.enabled = false;
		canvasAnimator.SetBool("Open", false);
		message.text = "";
	}
}
