﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class AreaSegment : MonoBehaviour 
{
	public Vector3 cameraOffset;
	public Vector3[] encounterList;
	private List<int[]> encountersForArea;

	protected void OnTriggerEnter(Collider other)
	{
		if (other.tag.Equals ("Player"))
		{
			Area area = GetComponentInParent<Area> ();
			if (area)
			{
				area.SetEncountersList (BuildEncounterListForArea());
				area.SetCameraOffset(cameraOffset);
			}
		}
	}

	private List<int[]> BuildEncounterListForArea()
	{
		List<int[]> encountersForArea = new List<int[]>();
		for (int i = 0; i < encounterList.Length; i++)
		{
			encountersForArea.Add(new int[]{(int)encounterList[i].x, (int)encounterList[i].y, (int)encounterList[i].z});
		}

		return encountersForArea;
	}
}

