﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleEncounter : Interactable 
{
	public int encounterId = -1;
	public int[] enemyIds = new int[3];
	SerializableBattleEncounter serializable;

	private void Awake()
	{
		serializable = new SerializableBattleEncounter();
		serializable.enemyIds = enemyIds;
		serializable.encounterId = encounterId;
		GetComponent<Collider> ().enabled = false;
	}

	// Use this for initialization
	new void Start () 
	{
		base.Start ();
		if(enemyIds.Length > 0)
		{
			GameObject enemyObject = (GameObject)Instantiate(ResourceLoader.LoadFieldEnemy(enemyIds[0].ToString()));
			enemyObject.transform.parent = this.gameObject.transform;
			enemyObject.transform.localPosition = Vector3.zero;
			if (!HasBeenDefeated())
			{
				GetComponent<Collider> ().enabled = true;
			}
		}
	}

	new void OnTriggerEnter(Collider other)
	{
		if(serializable.hasBeenDefeated)
		{
			this.gameObject.SetActive(false);
		}

		if(!FadeoutScript.reference.IsBlackedOut())
		{
			if(other.tag.Equals("Player"))
			{
				if(serializable.hasBeenEncountered)
				{
					// Start battle straight away
					GameObject.FindGameObjectWithTag("GameController").SendMessage("StartEncounterBattleTransition", enemyIds);
				}
				else
				{
					// FIXME: Start cinematic before battle
					GetComponentInParent<Area>().BattleEncounterEncountered(encounterId);
					GameObject.FindGameObjectWithTag("GameController").SendMessage("StartEncounterBattleTransition", enemyIds);
				}
			}
		}
	}

	public void SetBeenEncountered(bool encountered)
	{
		serializable.hasBeenEncountered = encountered;
	}
	
	public bool HasBeenEncountered()
	{
		return serializable.hasBeenEncountered;
	}

	public void SetBeenDefeated(bool defeated)
	{
		serializable.hasBeenDefeated = defeated;
		
		if (serializable.hasBeenDefeated)
		{
			this.gameObject.SetActive (false);
		}
		else
		{
			GetComponent<Collider> ().enabled = true;
		}
	}
	
	public bool HasBeenDefeated()
	{
		return serializable.hasBeenDefeated;
	}
	
	public SerializableBattleEncounter GetSerializable()
	{
		return serializable;
	}
	
	public void SetSerializable(SerializableBattleEncounter serializable)
	{
		SetBeenEncountered (serializable.hasBeenEncountered);
		SetBeenDefeated (serializable.hasBeenDefeated);
	}
}

[System.Serializable]
public class SerializableBattleEncounter
{
	public int encounterId;
	public int[] enemyIds;
	public bool hasBeenEncountered = false;
	public bool hasBeenDefeated = false;
}
