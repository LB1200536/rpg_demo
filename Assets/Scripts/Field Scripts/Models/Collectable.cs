﻿using UnityEngine;
using System.Collections;

public enum CollectableType {UsableItem, Weapon, Armour, KeyItem, Gold};

public class Collectable : Interactable 
{
	private float LIGHT_LERP_RATE = 0.01f;

	public CollectableType collectableType = CollectableType.UsableItem;
	public int associatedItemId = -1; // Needs to be set in order to instantiate the correct Item.
	public int goldValue = 0;
	public float minSpriteSize = 0.5f;
	public float maxSpriteSize = 1.5f;

	private SerializableCollectable serializableCollectable;

	GameObject sprite;
	float lightLerpValue;
	Item associatedItem;

	bool isCollectable;
	bool increaseLightRange; // Used to determine whether to lerp the intensity up or down.

	private void Awake()
	{
		serializableCollectable = new SerializableCollectable();
		serializableCollectable.itemId = associatedItemId;
	}

	// Use this for initialization
	new void Start () 
	{
		base.Start ();

		sprite = GameObject.Find ("Sprite");
		lightLerpValue = 0.0f;

		if(associatedItemId != -1)
		{
			switch(collectableType)
			{
			case CollectableType.UsableItem:
			case CollectableType.KeyItem:
				associatedItem = new Item(associatedItemId);
				break;
			case CollectableType.Weapon:
				associatedItem = new Weapon(associatedItemId);
				break;
			case CollectableType.Armour:
				associatedItem = new Armour(associatedItemId);
				break;
			}
		}

		name = "Collectable (" + associatedItem.GetName () + ")";
		isCollectable = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		sprite.transform.LookAt(Camera.main.transform.position, -Vector3.up);
		LerpSpriteSize ();
	}

	private void LerpSpriteSize()
	{
		Vector2 scale;
		if(increaseLightRange)
		{
			lightLerpValue += LIGHT_LERP_RATE;
		}
		else
		{
			lightLerpValue -= LIGHT_LERP_RATE;
		}

		scale.x = Mathf.Lerp(minSpriteSize, maxSpriteSize, lightLerpValue);
		scale.y = Mathf.Lerp(minSpriteSize, maxSpriteSize, lightLerpValue);
		
		if(scale.x >= maxSpriteSize || scale.x <= minSpriteSize)
		{
			increaseLightRange = !increaseLightRange;
			//lightLerpValue = 0;
		}

		sprite.transform.localScale = scale;
	}

	new void OnTriggerEnter(Collider other)
	{
		isCollectable = true;
		base.OnTriggerEnter(other);
	}

	new void OnTriggerStay(Collider other)
	{
		base.OnTriggerStay(other);
	}

	new void OnTriggerExit(Collider other)
	{
		isCollectable = false;
		base.OnTriggerExit(other);
	}

	override public void Interact(GameObject player = null)
	{
		if(isCollectable)
		{
			SetCollected(true);
			AudioManagerScript.reference.PlayFieldEffect ("ItemPickup");
			GetComponentInParent<Area>().CollectableCollected(associatedItemId);

			if (collectableType == CollectableType.Gold)
			{
				PartyDataHolderScript.reference.goldCount += goldValue;
				GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (PartyDataHolderScript.reference.GetPartyLeaderName ()
					+ " picked up " + goldValue.ToString () + " Gold.");
			}
			else
			{
				PartyDataHolderScript.reference.inventory.AddItemToInventory (associatedItem);
				if (associatedItem.GetQuantity () == 1)
				{
					if (associatedItem.GetName ().IndexOfAny("AEIOU".ToCharArray()) == 0) // Item name begins with a vowel
					{
						GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (PartyDataHolderScript.reference.GetPartyLeaderName ()
							+ " picked up an " + associatedItem.GetName () + ".");
					}
					else
					{
						GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (PartyDataHolderScript.reference.GetPartyLeaderName ()
							+ " picked up a " + associatedItem.GetName () + ".");
					}
				}
				else
				{
					GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (PartyDataHolderScript.reference.GetPartyLeaderName ()
						+ " picked up " + associatedItem.GetQuantity().ToString() + " " + associatedItem.GetName () + "s.");
				}
			}
		}

		player.GetComponent<HeadLookatScript> ().ResetLookatPosition ();
	}

	public void SetCollected(bool collected)
	{
		serializableCollectable.hasBeenCollected = collected;

		if(serializableCollectable.hasBeenCollected)
		{
			this.gameObject.SetActive(false);
		}
	}

	public bool HasBeenCollected()
	{
		return serializableCollectable.hasBeenCollected;
	}

	public SerializableCollectable GetSerializable()
	{
		return serializableCollectable;
	}

	public void SetSerializable(SerializableCollectable serializable)
	{
		SetCollected (serializable.hasBeenCollected);
	}

}

[System.Serializable]
public class SerializableCollectable
{
	public int itemId;
	public bool hasBeenCollected;
}
