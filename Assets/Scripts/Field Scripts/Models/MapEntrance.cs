﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEntrance : MonoBehaviour 
{
	public int areaId;
	public Door door;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag.Equals ("Player"))
		{
			if(!door.IsLocked())
			{
				CancelInvoke ();
				PlayerPrefs.SetInt ("lastDoorUsed", door.doorId);
				door.PlayDoorEntranceSFX();

				GameObject fieldController = GameObject.FindObjectOfType<FieldControllerScript> ().gameObject;
				fieldController.SendMessage ("EnableMiniMap", false);
				FadeoutScript.reference.StartTransition (ScreenTransitionType.Vertical, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black, "DestroyCurrentArea", fieldController);
				VirtualControllerScript.reference.Enable (false);
			}
			else
			{
				door.Interact (other.gameObject);
			}
		}
	}
}
