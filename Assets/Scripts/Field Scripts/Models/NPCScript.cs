﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : Interactable 
{
	protected BoxCollider trigger;
	protected Animator animator;
	protected GameObject playerObject;
	protected SerializableNPC serializable;
	protected List<int> conversationsHad;

	// Set these variables in the Inspector
	public Area currentArea;
	public int npc_id;

	public void Awake()
	{
		serializable = new SerializableNPC ();
		serializable.npc_id = npc_id;
	}

	// Use this for initialization
	new void Start () 
	{
		if (conversationsHad == null)
		{
			conversationsHad = new List<int>();
		}

		trigger = GetComponentInChildren<BoxCollider>();
		animator = GetComponentInChildren<Animator>();
	}

	override public void Interact(GameObject player)
	{
		playerObject = player;
		ToggleUIElements (false);

		GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (LoadConversationId(), this.EndInteraction);
		EstablishEyeContact (player.transform);

		MoveCameraForConversation (player.transform.position);
	}

	protected void EndInteraction()
	{
		// FIXME: End talking and listening animations
		animator.SetBool ("Talking", false);
		DampenedFieldCamera camera = Camera.main.GetComponent<DampenedFieldCamera> ();
		//camera.offset = new Vector3 (camera.offset.x, camera.offset.y * 4.0f, camera.offset.z);

		camera.offset = currentArea.cameraOffset;
		camera.target = playerObject;

		ToggleUIElements (true);
	}

	protected virtual int LoadConversationId()
	{
		bool conversationFound = false;
		int conversationId = -1;
		while (!conversationFound)
		{
			if (conversationsHad.Count == 0)
			{
				conversationId = DatabaseHandler.FindNextConversationId (npc_id, -1);
				conversationFound = true;
			}
			else
			{
				conversationId = DatabaseHandler.FindNextConversationId (npc_id, conversationsHad[conversationsHad.Count - 1]);
				conversationFound = true;
			}
		}
		//FIXME: Use the game's state and the npc_id to decide on the conversation to load
		if (!conversationsHad.Contains (conversationId))
		{
			conversationsHad.Add (conversationId);
			serializable.conversationsHad.Add (conversationId);
			GetComponentInParent<Area>().NPCConversed(npc_id, conversationId);
		}

		return conversationId;
	}

	protected void MoveCameraForConversation(Vector3 playerPosition)
	{
		// FIXME: Place the camera in relation to the characters
		DampenedFieldCamera camera = Camera.main.GetComponent<DampenedFieldCamera> ();
		camera.target = lookatTransform.gameObject;

		Vector3 newOffset = camera.target.transform.position;
		newOffset.x -= playerPosition.x * 2.5f;
		newOffset.y = 0;
		newOffset.z -= playerPosition.z * 2.5f;
		camera.offset = newOffset;
	}

	protected void ToggleUIElements(bool toggle)
	{
		VirtualControllerScript.reference.Enable (toggle);
		trigger.enabled = toggle;
		FindObjectOfType<FieldControllerScript>().SendMessage ("EnableMiniMap", toggle);
	}

	protected void EstablishEyeContact(Transform playerTransform)
	{
		// Rotate NPC to look at player
		Vector3 npcLookPosition = playerTransform.position - this.gameObject.transform.position;
		npcLookPosition.y = 0;
		playerTransform.rotation = Quaternion.LookRotation (npcLookPosition);
		//transform.LookAt (player.transform, Vector3.up);

		// Rotate player to look at NPC
		Vector3 playerLookPosition = this.gameObject.transform.position - playerTransform.position;
		playerLookPosition.y = 0;
		playerTransform.rotation = Quaternion.LookRotation (playerLookPosition);

		// FIXME: Check conversation to decide who is 'talking' and who is 'listening'
		animator.SetBool ("Talking", true);
	}

	public SerializableNPC GetSerializable()
	{
		return serializable;
	}

	public void SetSerializable(SerializableNPC serializable)
	{
		conversationsHad = new List<int>();
		npc_id = serializable.npc_id;

		for (int i = 0; i < serializable.conversationsHad.Count; i++)
		{
			conversationsHad.Add (serializable.conversationsHad [i]);
		}
	}
}

[System.Serializable]
public class SerializableNPC
{
	public int npc_id;
	public List<int> conversationsHad;

	public SerializableNPC()
	{
		conversationsHad = new List<int> ();
	}
}
