﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopkeeperNPC : NPCScript 
{
	public ShopManagerScript shopManager;
	private List<Item> itemsForSale;

	// Use this for initialization
	new void Start () 
	{
		conversationsHad = new List<int> ();
		// FIXME: Add items to list based on npc_id
		itemsForSale = new List<Item> ();
		itemsForSale.Add (new Item (0)); // potion
		itemsForSale.Add (new Item (5)); // antidote
		itemsForSale.Add (new Weapon (0)); // long sword
		itemsForSale.Add (new Armour (8)); // worn clothes
		trigger = GetComponentInChildren<BoxCollider>();
		animator = GetComponentInChildren<Animator>();
	}

	override public void Interact(GameObject player)
	{
		AudioManagerScript.reference.PlayBGMLoopWithIntro ("MogShop-Intro", "MogShop-Loop");
		playerObject = player;

		ToggleUIElements (false);

		GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (1, this.StartShopInteraction);
		EstablishEyeContact (player.transform);

		MoveCameraForConversation (player.transform.position);
	}

	private void StartBGMLoop()
	{
		AudioManagerScript.reference.PlayBGM ("MogShop-Loop", false);
		AudioManagerScript.reference.SetBGMSourceLoopingState (true);
	}

	private void StartShopInteraction()
	{
		VirtualControllerScript.reference.Enable (false);
		FindObjectOfType<DecisionPromptScript> ().StartDecision ("Would you like to trade?", shopManager.StartBuyMode, this.SayGoodbye, null,
			"Shop", "Leave");
	}

	public void SayGoodbye()
	{
		GameObject.FindObjectOfType<DialogueSystemScript> ().StartDialogue (2, this.EndInteraction);
		AudioManagerScript.reference.PlayBGM (currentArea.GetBGM(), true);
	}

	/*
	private void EndInteraction()
	{
		animator.SetBool ("Talking", false);
		DampenedFieldCamera camera = Camera.main.GetComponent<DampenedFieldCamera> ();
		camera.offset = new Vector3 (camera.offset.x * 2.0f, camera.offset.y * 2.0f, camera.offset.z * 2.0f);

		FindObjectOfType<FieldControllerScript>().SendMessage ("EnableMiniMap", true);
		trigger.enabled = true;
	}
	*/

	public List<Item> GetInventory()
	{
		return itemsForSale;
	}
}
