﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PartyDataHolderScript : MonoBehaviour 
{
	public static PartyDataHolderScript reference;

	public ItemXMLParserScript itemParser;
	public SkillXMLParserScript skillParser;
	public LevelUpXMLParserScript levelUpParser;

	private GameStateData currentGameStateData;
	
	public List<BaseCharacter> characters;
	public int goldCount;
	public Inventory inventory;

	public int areaId;
	public Vector3 lastPosition;
	public Vector3 lastRotation;

	public float timeElapsed;

	private int currentSaveSlot = -1;

	private Vector3 startPosition = new Vector3 (-9.0f, 0.0f, -10.0f);
	private Vector3 startRotation = new Vector3 (0.0f, 0.0f, 0.0f);
	private const int STARTING_PARTY_SIZE = 1;

	void Awake()
	{
		if(reference == null)
		{
			reference = this;
			DontDestroyOnLoad (this.gameObject);

			currentGameStateData = new GameStateData();

			itemParser = new ItemXMLParserScript ();
			skillParser = new SkillXMLParserScript();
			levelUpParser = new LevelUpXMLParserScript();

			characters = new List<BaseCharacter> ();
			ResetLastTransform ();
			
			currentSaveSlot = PlayerPrefs.GetInt ("saveSlot", 0);
			// Check for saved data to load character stats from
			if(SavedDataManager.SaveFileExists(currentSaveSlot))
			{
				SavedDataManager.LoadPlayerData(currentSaveSlot);
			}
			else
			{
				inventory = new Inventory (new ItemXMLParserScript());
				areaId = 1;
				// If none, generate fresh data
				//QuickGenerateNewData();
			}
		}
		else if(reference != this)
		{
			Destroy(gameObject);
		}
	}

	void Update()
	{
		timeElapsed += Time.deltaTime;
	}

	public void UpdateParty(List<CharacterScript> battleCharacters)
	{
		for(int i = 0; i < battleCharacters.Count; i++)
		{
			characters[i].SetCurrentHP(battleCharacters[i].GetCurrentHP());
			characters[i].SetCurrentMP(battleCharacters[i].GetCurrentMP());
			characters[i].SetCurrentExperiencePoints(battleCharacters[i].GetCurrentExperiencePoints());
			characters[i].SetTotalExperiencePoints(battleCharacters[i].GetTotalExperiencePoints());

			for(int j = 0; j < battleCharacters[i].GetStatuses().Count; j++)
			{
				characters[i].ApplyStatus(battleCharacters[i].GetStatuses()[j]);
			}

		}
	}

	private void QuickGenerateNewData()
	{
		// Set the area id to the starting area
		areaId = 0;

		GenerateNewCharacterData ();

		GenerateNewCharacterEquipment();	
	}
	
	public BaseCharacter CreateNewBaseCharacter()
	{
		GameObject newCharacterObject = new GameObject();
		newCharacterObject.AddComponent<BaseCharacter>();
		newCharacterObject.transform.parent = this.transform;

		return newCharacterObject.GetComponent<BaseCharacter>();
	}

	void GenerateNewCharacterData()
	{
		characters = new List<BaseCharacter> ();
		inventory = new Inventory (new ItemXMLParserScript());

		for(int i = 0; i < STARTING_PARTY_SIZE; i++)
		{
			BaseCharacter newCharacter = CreateNewBaseCharacter();

			switch(i)
			{
			case 0:
				newCharacter.SetName("Thief");
				newCharacter.SetMaxHP(28);
				newCharacter.SetMaxMP(10);
				newCharacter.SetSpeed(10);
				newCharacter.SetAttack(6); // Base 6
				newCharacter.SetDefence(3);
				newCharacter.SetMagicAttack(3);
				newCharacter.SetMagicDefence(3);
				newCharacter.SetSkillType(SkillType.Thievery);
				break;
			case 1:
				newCharacter.SetName("Knight");
				newCharacter.SetMaxHP(35);
				newCharacter.SetMaxMP(5);
				newCharacter.SetSpeed(6);
				newCharacter.SetAttack(9);
				newCharacter.SetDefence(5);
				newCharacter.SetMagicAttack(2);
				newCharacter.SetMagicDefence(4);
				newCharacter.SetSkillType(SkillType.Gallantry);
				break;
			case 2:
				newCharacter.SetName("WhiteMage");
				newCharacter.SetMaxHP(24);
				newCharacter.SetMaxMP(20);
				newCharacter.SetSpeed(4);
				newCharacter.SetAttack(2);
				newCharacter.SetDefence(2);
				newCharacter.SetMagicAttack(8);
				newCharacter.SetMagicDefence(9);
				newCharacter.SetSkillType(SkillType.WhiteMagic);
				break;
			case 3:
				newCharacter.SetName("BlackMage");
				newCharacter.SetMaxHP(20);
				newCharacter.SetMaxMP(25);
				newCharacter.SetSpeed(3);
				newCharacter.SetAttack(2);
				newCharacter.SetDefence(2);
				newCharacter.SetMagicAttack(9);
				newCharacter.SetMagicDefence(8);
				newCharacter.SetSkillType(SkillType.BlackMagic);
				break;
			}

			newCharacter.SetCurrentHP(newCharacter.GetMaxHP());
			newCharacter.SetCurrentMP(newCharacter.GetMaxMP());
			newCharacter.GenerateNextLevelRequirement();
			characters.Add (newCharacter);
			newCharacter.gameObject.name = newCharacter.GetName();
		}
	}

	public void GenerateNewCharacterEquipment()
	{
		//itemParser.ParseItems(itemsToFind);
		for(int i = 0; i < characters.Count; i++)
		{
			Weapon longSword = new Weapon (0);
			Weapon shortSword = new Weapon (1);
			Weapon woodenStick = new Weapon (3);
			Armour hat = new Armour (2);
			Armour gloves = new Armour (5);
			Armour clothes = new Armour (8);

			if(i < 2)
			{
				inventory.AddItemToInventory(shortSword);
				characters[i].SetWeapon(longSword);
			}
			else
			{
				characters[i].SetWeapon(woodenStick);
			}
			characters[i].SetHeadgear(hat);
			characters[i].SetBodyArmour(clothes);
			characters[i].SetHandgear(gloves);

			inventory.AddItemToInventory(characters[i].GetCurrentWeapon());
			inventory.AddItemToInventory(characters[i].GetCurrentHandgear());
			inventory.AddItemToInventory(characters[i].GetCurrentHeadgear());
			inventory.AddItemToInventory(characters[i].GetCurrentBodyArmour());
		}
	}

	public void AddAreaToCurrentStateData(Area alteredArea)
	{
		int alteredAreaIndex = currentGameStateData.alteredAreaList.FindIndex (area => area.areaId == alteredArea.areaId);
		if(alteredAreaIndex != -1)
		{
			currentGameStateData.alteredAreaList.RemoveAt(alteredAreaIndex);
			SerializableArea tempArea = alteredArea.GetSerializableArea();
			currentGameStateData.alteredAreaList.Add(tempArea);
		}
		else
		{
			SerializableArea tempArea = alteredArea.GetSerializableArea();
			currentGameStateData.alteredAreaList.Add(tempArea);
		}
	}

	public int GetCurrentSaveSlot()
	{
		return currentSaveSlot;
	}

	public void SetCurrentGameStateData(GameStateData newData)
	{
		currentGameStateData = newData;
	}

	public string GetPartyLeaderName()
	{
		return characters [0].GetName ();
	}

	public GameStateData GetCurrentGameStateData()
	{
		return currentGameStateData;
	}

	public void ResetLastTransform()
	{
		lastPosition = startPosition;
		lastRotation = startRotation;
	}
}
