﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class PartyMenuScript : MonoBehaviour 
{
	// Objects to be set in the Inspector
	public Canvas mainMenu;
	public Canvas itemMenu;
	public Canvas skillMenu;
	public Canvas equipmentMenu;
	public Canvas characterMenu;
	public Canvas popUpMenu;
	public Canvas optionsMenu;
	public Image equipmentConfirmDialog;

	// Other UI components taken from Canvases
	Text gameTimer;
	Text goldDisplay;

	// Collections of windows displaying data 	
	List<EquipmentDisplayScript> equipmentDisplayWindows;

	// Misc.
	bool selectingCharacter = false;
	string menuToOpen = "";
	Camera mainCamera;

	// Use this for initialization
	void Start () 
	{
		mainCamera = Camera.main;
		mainMenu.enabled = false;

		goldDisplay = mainMenu.GetComponentInChildren<Text> ();
		goldDisplay.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString();
		
		gameTimer = mainMenu.GetComponentsInChildren<Text> () [1];

		characterMenu.enabled = false;

		if(!itemMenu)
		{
			Debug.LogError("ItemList object not found.");
		}
		itemMenu.enabled = false;

		if(!skillMenu)
		{
			Debug.LogError("SkillList object not found.");
		}
		skillMenu.enabled = false;

		PopulateCharacterWindows ();

		if(!optionsMenu)
		{
			Debug.LogError("optionsMenu object not found.");
		}

		if(!equipmentMenu)
		{
			Debug.LogError("equipmentScreen object not found.");
		}
		equipmentMenu.enabled = false;
		equipmentDisplayWindows = characterMenu.GetComponentsInChildren<EquipmentDisplayScript> ().ToList();
	}

	void OpenMenu()
	{
		mainMenu.enabled = true;
		goldDisplay.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString();
		PopulateCharacterWindows ();
		InvokeRepeating ("UpdateTimer", 0.0f, 1.0f);
	}

	void UpdateTimer()
	{
		System.TimeSpan timeSpan = System.TimeSpan.FromSeconds (PartyDataHolderScript.reference.timeElapsed);
		string time = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
		gameTimer.text = "Time: " + time;
	}

	public void CloseMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");

		if(menuToOpen.Equals("EquipItem"))
		{
			CancelCharacterSelect();
			popUpMenu.SendMessage("Close");

			if(equipmentConfirmDialog.transform.localScale.x > 0)
			{
				equipmentConfirmDialog.SendMessage ("Reset");
				equipmentConfirmDialog.GetComponentInChildren<Animator>().SetBool("Open", false);
				equipmentConfirmDialog.enabled = false;
			}

			itemMenu.enabled = true;
		}
		else if(selectingCharacter)
		{
			popUpMenu.SendMessage("Close");
			CancelCharacterSelect();
		}
		else
		{
			mainCamera.GetComponent<DampenedFieldCamera>().SendMessage("Toggle");
			Camera.main.farClipPlane = 100.0f;
			Camera.main.SendMessage ("SetTransformWithoutDampening");
			Invoke ("RestartController", 0.3f);
			if(GetComponentInChildren<Animator>())
			{
				GetComponentInChildren<Animator>().SetBool("Open", false);
				CancelInvoke("UpdateTimer");
			}
		}
	}

	public void OpenItemMenu()
	{
		if(selectingCharacter)
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
			popUpMenu.SendMessage("Close");
			CancelCharacterSelect();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
			itemMenu.GetComponent<ItemMenuScript>().SendMessage("UpdateButtons");
			itemMenu.enabled = true;
			if(itemMenu.GetComponentInChildren<Animator>())
			{
				itemMenu.GetComponentInChildren<Animator>().SetBool("Open", true);
			}
		}
	}

	public void OpenOptionsMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");

		// Ensure the player hasn't been trying to open another menu
		selectingCharacter = false;
		popUpMenu.enabled = false;
		menuToOpen = "";

		optionsMenu.GetComponent<OptionsMenuScript>().OpenMenu ();
		
		Camera.main.gameObject.BroadcastMessage ("SetRotationSpeed", 0.0f, SendMessageOptions.DontRequireReceiver);
	}

	public void CloseItemMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
		if(itemMenu.GetComponentInChildren<Animator>())
		{
			itemMenu.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		itemMenu.enabled = false;
	}

	public void OpenCharacterMenu(BaseCharacter character)
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		characterMenu.enabled = true;
		if(characterMenu.GetComponentInChildren<Animator>())
		{
			characterMenu.GetComponentInChildren<Animator>().SetBool("Open", true);
		}
		characterMenu.SendMessage ("UpdateDetails", character);
		popUpMenu.SendMessage ("DisablePopUp");
		//popUpMenu.SendMessage ("OpenWithMessage", character.GetName ());

		equipmentDisplayWindows [0].AssignItem (character.GetCurrentWeapon ());
		equipmentDisplayWindows [1].AssignItem (character.GetCurrentHandgear ());
		equipmentDisplayWindows [2].AssignItem (character.GetCurrentHeadgear ());
		equipmentDisplayWindows [3].AssignItem (character.GetCurrentBodyArmour ());
	}

	public void OpenSkillMenu(BaseCharacter character)
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		skillMenu.GetComponent<SkillMenuScript> ().SetSelectedCharacter (character);
		skillMenu.enabled = true;
		popUpMenu.SendMessage ("DisablePopUp");
		//popUpMenu.SendMessage ("OpenWithMessage", character.GetName () + "\n(" + character.GetSkillType ().ToString () + ")");
		if(skillMenu.GetComponentInChildren<Animator>())
		{
			skillMenu.GetComponentInChildren<Animator>().SetBool("Open", true);
		}		
	}

	public void CloseCharacterMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
		popUpMenu.SendMessage ("Close");
		if(characterMenu.GetComponentInChildren<Animator>())
		{
			characterMenu.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		characterMenu.enabled = false;
	}
	
	public void CloseSkillMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
		popUpMenu.SendMessage ("Close");
		if(skillMenu.GetComponentInChildren<Animator>())
		{
			skillMenu.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		skillMenu.enabled = false;
	}

	public void OpenEquipmentScreen(BaseCharacter character)
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		equipmentMenu.enabled = true;
		popUpMenu.SendMessage ("DisablePopUp");
		popUpMenu.SendMessage ("OpenWithMessage", character.GetName ());
		if(equipmentMenu.GetComponentInChildren<Animator>())
		{
			equipmentMenu.GetComponentInChildren<Animator>().SetBool("Open", true);
		}

		equipmentDisplayWindows [0].AssignItem (character.GetCurrentWeapon ());
		equipmentDisplayWindows [1].AssignItem (character.GetCurrentHeadgear ());
		equipmentDisplayWindows [2].AssignItem (character.GetCurrentBodyArmour ());
		equipmentDisplayWindows [3].AssignItem (character.GetCurrentHandgear ());
	}
	
	public void CloseEquipmentScreen()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
		popUpMenu.SendMessage ("Close");
		if(equipmentMenu.GetComponentInChildren<Animator>())
		{
			equipmentMenu.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		equipmentMenu.enabled = false;
	}

	public void StartCharacterSelect(string menuName)
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		// Activate window displaying text to select character 
		popUpMenu.SendMessage ("OpenWithMessage", "Select Character");
		selectingCharacter = true;
		menuToOpen = menuName;
	}

	public void CancelCharacterSelect()
	{
		selectingCharacter = false;
		if(menuToOpen.Equals("UseSkill"))
		{
			skillMenu.enabled = true;
		}
		else if(menuToOpen.Equals("ItemUse"))
		{
			itemMenu.enabled = true;
		}
		else
		{
			menuToOpen = "";
		}
	}

	public void CharacterSelected(int index)
	{
		switch(menuToOpen)
		{
		case "Skill":
			CancelCharacterSelect ();
			OpenSkillMenu(PartyDataHolderScript.reference.characters[index]);
			break;
		case "Equipment":
			CancelCharacterSelect ();
			OpenEquipmentScreen(PartyDataHolderScript.reference.characters[index]);
			break;
		case "UseItem":
			//CancelCharacterSelect ();
			UseItemOnCharacter(PartyDataHolderScript.reference.characters[index], itemMenu.GetComponent<ItemMenuScript>().GetItemToUse());
			break;
		case "EquipItem":
			//CancelCharacterSelect ();
			popUpMenu.enabled = false;
			equipmentConfirmDialog.GetComponent<EquipmentConfirmationDialogScript>().Setup(PartyDataHolderScript.reference.characters[index],
			                                                                               (Equipment)itemMenu.GetComponent<ItemMenuScript>().GetItemToUse());
			break;
		case "UseSkill":
			//CancelCharacterSelect ();
			UseSkillOnCharacter(PartyDataHolderScript.reference.characters[index], skillMenu.GetComponent<SkillMenuScript>().GetSkillToUse());
			break;
		default:
			CancelCharacterSelect ();
			OpenCharacterMenu(PartyDataHolderScript.reference.characters[index]);
			break;
		}
	}

	private void UseItemOnCharacter(BaseCharacter character, Item item)
	{
		bool itemUsed = false;

		switch(item.GetItemType())
		{
		case ItemType.HP_Healing:
			if(character.GetCurrentHP() < character.GetMaxHP())
			{
				character.SetCurrentHP(character.GetCurrentHP() + (item.GetEffectAmount()));
				itemUsed = true;
			}
			break;
		case ItemType.MP_Healing:
			if(character.GetCurrentMP() < character.GetMaxMP())
			{
				character.SetCurrentMP(character.GetCurrentMP() + (item.GetEffectAmount()));
				itemUsed = true;
			}
			break;
		case ItemType.Status_Healing:
			if(character.GetStatuses().Contains(item.GetStatus()))
			{
				character.RemoveStatus(item.GetStatus());
				itemUsed = true;
			}
			break;
		}

		if(itemUsed)
		{
			AudioManagerScript.reference.PlayFieldEffect("HealFromMenu");
			itemMenu.GetComponent<ItemMenuScript>().ItemUsed();
			PopulateCharacterWindows();

			if(item.GetQuantity() == 0)
			{
				CancelCharacterSelect();
				popUpMenu.enabled = false;
				itemMenu.enabled = true;
			}
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public void EquipItemOnCharacter()
	{
		AudioManagerScript.reference.PlayMenuEffect("ItemEquipped");
		equipmentConfirmDialog.GetComponent<EquipmentConfirmationDialogScript> ().GetCharacter ().SetEquipment (
			equipmentConfirmDialog.GetComponent<EquipmentConfirmationDialogScript> ().GetEquipment ());

		CancelCharacterSelect();
		itemMenu.GetComponent<ItemMenuScript>().ItemUsed();

		equipmentConfirmDialog.GetComponent<Animator>().SetBool ("Open", false);
		equipmentConfirmDialog.enabled = false;
		popUpMenu.enabled = false;
		itemMenu.enabled = true;
	}

	public void CancelEquipmentChange()
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
		equipmentConfirmDialog.GetComponent<EquipmentConfirmationDialogScript> ().Reset ();
		CancelCharacterSelect();
		
		equipmentConfirmDialog.GetComponent<Animator>().SetBool ("Open", false);
		equipmentConfirmDialog.enabled = false;
		popUpMenu.enabled = false;
		itemMenu.enabled = true;
	}

	private void UseSkillOnCharacter(BaseCharacter character, Skill skill)
	{
		bool skillUsed = false;

		if(skill.GetMPCost() < skillMenu.GetComponent<SkillMenuScript>().GetSelectedCharacter().GetCurrentMP())
		{
			switch(skill.GetElement())
			{
			case Element.Heal:
				// If the skill is able to heal a status ailment
				if(skill.GetAssociatedStatus() != Status.None)
				{
					// If the character is afflicted by the status ailment
					if(character.GetStatuses().Contains(skill.GetAssociatedStatus()))
					{
						character.RemoveStatus(skill.GetAssociatedStatus());
						skillUsed = true;
					}
				}
				
				// If the skill is able to damage or heal
				if(skill.GetBaseDamage() != 0)
				{
					// If the character actually needs healing
					if(character.GetCurrentHP() < character.GetMaxHP())
					{
						// The skill damage gets a multiplier for not being used in battle
						character.SetCurrentHP(character.GetCurrentHP() + (skill.GetBaseDamage() * 2));
						skillUsed = true;
					}
				}
				break;
			}
		}
		
		if(skillUsed)
		{
			AudioManagerScript.reference.PlayAttackEffect(skill.GetName());
			skillMenu.GetComponent<SkillMenuScript>().SkillUsed();
			PopulateCharacterWindows();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	private void RestartController()
	{
		Resources.UnloadUnusedAssets ();
		transform.parent.gameObject.SendMessage ("ReturnFromMenu");
		mainMenu.enabled = false;
		VirtualControllerScript.reference.Enable (true);
	}

	private void PopulateCharacterWindows()
	{
		for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			GameObject window = GameObject.Find("CharacterWindow"+i);

			if(window)
			{
				Text[] textFields = window.GetComponentsInChildren<Text>();
				Image characterPortrait = window.GetComponentsInChildren<Image> ()[1];
				BaseCharacter character = PartyDataHolderScript.reference.characters[i];

				// First Element = Name
				textFields[0].text = character.GetName();
				// Second Element = Level
				textFields[1].text = "LVL: " + character.GetCurrentLevel().ToString();
				// Third Element = HP
				textFields[2].text = "HP: " + character.GetCurrentHP().ToString() + "/" + character.GetMaxHP().ToString();
				// Fourth Element = MP
				textFields[3].text = "MP: " + character.GetCurrentMP().ToString() + "/" + character.GetMaxMP().ToString();
				// Fifth Element (waaay-yooo) = Experience
				textFields[4].text = "EXP: " + character.GetCurrentExperiencePoints().ToString() + "/" + character.GetExperiencePointsForNextLevel().ToString();
				// Sixth Element = Class
				textFields[5].text = character.GetClassName();

				characterPortrait.sprite = ResourceLoader.LoadSprite (character.GetClassName () + "Headshot");
			}
			else
			{
				Debug.LogError("Character Window not found: " + i.ToString());
			}
		}
	}
}
