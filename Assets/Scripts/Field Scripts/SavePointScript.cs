﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class SavePointScript : MonoBehaviour 
{
	public bool active;
	public ParticleSystem triggerParticles;
	private DecisionPromptScript decisionPrompt;
	private GameObject playerObject;
	private Color particleBaseColour;

	// Use this for initialization
	void Start () 
	{
		active = false;
		decisionPrompt = FindObjectOfType<DecisionPromptScript> ();
		decisionPrompt.Enable (false);
		particleBaseColour = triggerParticles.main.startColor.color;
	}

	public void PromptYes()
	{
		AudioManagerScript.reference.PlayFieldEffect("SaveComplete");

		PartyDataHolderScript.reference.lastPosition = playerObject.transform.position;
		PartyDataHolderScript.reference.lastRotation = playerObject.transform.eulerAngles;
		SavedDataManager.Save(PartyDataHolderScript.reference.GetCurrentSaveSlot());
		if(decisionPrompt.GetComponentInChildren<Animator>())
		{
			decisionPrompt.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		decisionPrompt.Enable (false);
		VirtualControllerScript.reference.Enable(true);

	}

	public void PromptNo()
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
		if(decisionPrompt.GetComponentInChildren<Animator>())
		{
			decisionPrompt.GetComponentInChildren<Animator>().SetBool("Open", false);
		}
		decisionPrompt.Enable (false);
		VirtualControllerScript.reference.Enable(true);
	}

	public void DisplayPrompt()
	{
		if(decisionPrompt.GetComponentInChildren<Animator>())
		{
			decisionPrompt.GetComponentInChildren<Animator>().SetBool("Open", true);
		}
		decisionPrompt.Enable (true);
		decisionPrompt.StartDecision ("Would you like to save your game?", this.PromptYes, this.PromptNo);
		VirtualControllerScript.reference.gameObject.GetComponent<Canvas> ().enabled = false;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			ParticleSystem.ColorOverLifetimeModule colourModule = triggerParticles.colorOverLifetime;
			playerObject = other.gameObject;
			playerObject.SendMessage("EnablePrompt");
			active = true;

			bool healUsed = false;
			foreach(BaseCharacter character in PartyDataHolderScript.reference.characters)
			{
				// Fully heal each character
				if(character.GetCurrentHP() < character.GetMaxHP())
				{
					healUsed = true;
					character.SetCurrentHP(character.GetMaxHP());
				}
				if(character.GetCurrentMP() < character.GetMaxMP())
				{
					healUsed = true;
					character.SetCurrentMP(character.GetMaxMP());
				}
				if(!character.GetStatuses().Contains(Status.Okay))
				{
					healUsed = true;
					character.RemoveAllStatuses ();
				}
			}

			if (healUsed)
			{
				AudioManagerScript.reference.PlayFieldEffect ("HealFromMenu");
				colourModule.color = Color.green;
			}
			else
			{
				colourModule.color = particleBaseColour;
			}

			AudioManagerScript.reference.PlayFieldEffect("SavePoint");
			triggerParticles.Play ();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			active = false;
			playerObject.SendMessage("DisablePrompt");
			playerObject = null;
		}
	}
}
