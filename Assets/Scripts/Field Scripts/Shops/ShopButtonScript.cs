﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButtonScript : MonoBehaviour 
{
	private ShopManagerScript shopOwner;
	private int buttonId;
	private Item item;
	public Text itemName;
	public Text itemCost;
	public Text itemOwnedQuantity;
	public Image itemIcon;

	public void Setup(Item item, ShopManagerScript owner, int buttonId)
	{
		this.item = item;
		this.buttonId = buttonId;
		this.shopOwner = owner;

		this.item.SetQuantity (1);
		itemName.text = this.item.GetName ();

		if (!shopOwner.buyMode)
		{
			itemCost.text = "Sell for: " + this.item.GetSellAmount().ToString();
		}
		else
		{
			itemCost.text = "Buy for: " + this.item.GetBuyAmount().ToString();
		}

		UpdateQuantity ();
		itemIcon.sprite = this.item.GetMenuImage ();
	}

	public void UpdateQuantity()
	{
		if (PartyDataHolderScript.reference != null)
		{
			Item itemFound = PartyDataHolderScript.reference.inventory.GetAllItems ().Find (x => x.GetItemId () == item.GetItemId ());
			if (!shopOwner.buyMode && item.IsEquipped ())
			{
				itemOwnedQuantity.text = "Equipped";
			}
			else if (itemFound != null)
			{
				itemOwnedQuantity.text = "Owned = " + itemFound.GetQuantity ().ToString ();
			}
			else
			{
				itemOwnedQuantity.text = "Owned = 0";
				if (!shopOwner.buyMode)
				{
					item.SetQuantity (0);
				}
			}
		}
	}

	public void ButtonPress()
	{
		shopOwner.ButtonPress (buttonId);
	}

	public Item GetItem()
	{
		return item;
	}
}
