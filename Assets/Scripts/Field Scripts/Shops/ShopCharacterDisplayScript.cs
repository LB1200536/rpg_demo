﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopCharacterDisplayScript : MonoBehaviour 
{
	public Text name;
	public Text currentEquipment;
	public Text stat1;
	public Text stat2;
	public Text stat3;

	private BaseCharacter character;

	// Use this for initialization
	public void Setup (BaseCharacter baseCharacter) 
	{
		character = baseCharacter;

		name.text = character.GetName ();
		currentEquipment.text = "";
		stat1.text = "";
		stat2.text = "";
		stat3.text = "";
	}

	public void DisplayWeaponStats(Weapon newWeapon)
	{
		Weapon weapon = character.GetCurrentWeapon ();

		currentEquipment.text = weapon.GetName ();
		stat1.text = "P.Dam = " + weapon.GetPhysicalDamage ().ToString() + 
			" (" + (newWeapon.GetPhysicalDamage() - weapon.GetPhysicalDamage()).ToString() + ")";
		stat2.text = "M.Dam = " + weapon.GetMagicalDamage ().ToString() + 
			" (" + (newWeapon.GetMagicalDamage() - weapon.GetMagicalDamage()).ToString() + ")";
		stat3.text = "Acc = " + weapon.GetAccuracy ().ToString() + 
			" (" + (newWeapon.GetAccuracy() - weapon.GetAccuracy()).ToString() + ")";
	}

	public void DisplayHeadgearStats(Armour newHeadgear)
	{
		Armour headgear = character.GetCurrentHeadgear ();

		currentEquipment.text = headgear.GetName ();
		stat1.text = "P.Def = " + headgear.GetPhysicalDefence ().ToString() + 
			" (" + (newHeadgear.GetPhysicalDefence() - headgear.GetPhysicalDefence()).ToString() + ")";
		stat2.text = "M.Def = " + headgear.GetMagicalDefence ().ToString() + 
			" (" + (newHeadgear.GetMagicalDefence() - headgear.GetMagicalDefence()).ToString() + ")";
		stat3.text = "Eva = " + headgear.GetEvasion ().ToString() + 
			" (" + (newHeadgear.GetEvasion() - headgear.GetEvasion()).ToString() + ")";
	}

	public void DisplayBodyArmourStats(Armour newBodyArmour)
	{
		Armour bodyArmour = character.GetCurrentBodyArmour ();

		currentEquipment.text = bodyArmour.GetName ();
		stat1.text = "P.Def = " + bodyArmour.GetPhysicalDefence ().ToString() + 
			" (" + (newBodyArmour.GetPhysicalDefence() - bodyArmour.GetPhysicalDefence()).ToString() + ")";
		stat2.text = "M.Def = " + bodyArmour.GetMagicalDefence ().ToString() + 
			" (" + (newBodyArmour.GetMagicalDefence() - bodyArmour.GetMagicalDefence()).ToString() + ")";
		stat3.text = "Eva = " + bodyArmour.GetEvasion ().ToString() + 
			" (" + (newBodyArmour.GetEvasion() - bodyArmour.GetEvasion()).ToString() + ")";
	}

	public void DisplayHandgearStats(Armour newHandgear)
	{
		Armour handgear = character.GetCurrentHandgear ();

		currentEquipment.text = handgear.GetName ();
		stat1.text = "P.Def = " + handgear.GetPhysicalDefence ().ToString() + 
			" (" + (newHandgear.GetPhysicalDefence() - handgear.GetPhysicalDefence()).ToString() + ")";
		stat2.text = "M.Def = " + handgear.GetMagicalDefence ().ToString() + 
			" (" + (newHandgear.GetMagicalDefence() - handgear.GetMagicalDefence()).ToString() + ")";
		stat3.text = "Eva = " + handgear.GetEvasion ().ToString() + 
			" (" + (newHandgear.GetEvasion() - handgear.GetEvasion()).ToString() + ")";
	}
}
