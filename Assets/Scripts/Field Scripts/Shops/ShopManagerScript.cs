﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManagerScript : MonoBehaviour 
{
	private int lastButtonIdPressed;

	private List<Item> itemsForSale;

	public ShopkeeperNPC shopkeeper;
	public bool buyMode = true;
	public List<ShopButtonScript> itemButtons;
	public List<ShopCharacterDisplayScript> characterDisplays;
	public Canvas characterStatDisplay;
	public Canvas shopDisplay;
	public CanvasGroup shopContent;
	public Transform contentPanel;
	public Text goldDisplay;
	public Text modeButtonText;
	public Scrollbar scrollbar;
	public DecisionPromptScript decisionPrompt;
	public SimpleObjectPool buttonObjectPool;

	// Use this for initialization
	void Start () 
	{
		decisionPrompt = FindObjectOfType<DecisionPromptScript> ();
		itemsForSale = new List<Item> ();
		buyMode = true;

		shopDisplay.enabled = false;
		characterStatDisplay.enabled = false;
		for (int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			characterDisplays [i].gameObject.SetActive (true);
			characterDisplays [i].Setup (PartyDataHolderScript.reference.characters [i]);
		}
	}
		
	public void StartBuyMode()
	{
		itemsForSale = shopkeeper.GetInventory ();
		RefreshDisplay ();
		shopDisplay.enabled = true;
	}

	public void StartSellMode()
	{
		itemsForSale = PartyDataHolderScript.reference.inventory.GetAllItems ();
		RefreshDisplay ();
		shopDisplay.enabled = true;
	}

	public void SwapShopMode()
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuConfirm");
		itemsForSale = new List<Item> ();
		buyMode = !buyMode;
		characterStatDisplay.enabled = false;
		scrollbar.value = 1.0f;

		if (buyMode)
		{
			StartBuyMode ();
			modeButtonText.text = "Mode: Buy";
		}
		else
		{
			StartSellMode ();
			modeButtonText.text = "Mode: Sell";
		}
	}

	void RefreshDisplay()
	{
		lastButtonIdPressed = -1;
		characterStatDisplay.enabled = false;
		goldDisplay.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString ();
		RemoveButtons ();
		AddButtons ();
	}

	private void RemoveButtons()
	{
		while (contentPanel.childCount > 0) 
		{
			GameObject toRemove = contentPanel.GetChild(0).gameObject;
			buttonObjectPool.ReturnObject(toRemove);
		}

		itemButtons.Clear ();
	}

	private void AddButtons()
	{
		for (int i = 0; i < itemsForSale.Count; i++) 
		{
			Item item = itemsForSale[i];

			if(!item.IsEquipped())
			{
				GameObject newButton = buttonObjectPool.GetObject();
				newButton.transform.SetParent(contentPanel);
				newButton.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				
				ShopButtonScript shopButton = newButton.GetComponent<ShopButtonScript>();
				shopButton.Setup(item, this, itemButtons.Count);
				itemButtons.Add (shopButton);
			}
		}

		scrollbar.numberOfSteps = itemButtons.Count - 2;
	}

	public void ButtonPress(int buttonPressedId)
	{
		Item currentItem = itemButtons [buttonPressedId].GetItem ();
		if (lastButtonIdPressed == buttonPressedId)
		{
			if (buyMode && currentItem.GetBuyAmount () <= PartyDataHolderScript.reference.goldCount)
			{
				decisionPrompt.StartDecision ("Would you like to buy " + currentItem.GetName () + "?",
					this.PurchaseItem, null,
					shopContent);
				decisionPrompt.SetConfirmSFX("Purchase");
			}
			else if (!buyMode && (!currentItem.IsEquipped() && !currentItem.IsKeyItem()))
			{
				decisionPrompt.StartDecision ("Would you like to sell " + currentItem.GetName () + "?",
					this.SellItem, null,
					shopContent);
				decisionPrompt.SetConfirmSFX("Purchase");
			}
			else
			{
				AudioManagerScript.reference.PlayMenuEffect("MenuError");
			}
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			lastButtonIdPressed = buttonPressedId;
			characterStatDisplay.enabled = !currentItem.GetUsable ();
			if (characterStatDisplay.enabled)
			{
				UpdateCharacterDisplays ((Equipment)currentItem);
			}
		}
	}

	private void UpdateCharacterDisplays(Equipment currentEquipment)
	{
		if (currentEquipment.IsWeapon())
		{
			for (int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
			{
				characterDisplays [i].DisplayWeaponStats ((Weapon)currentEquipment);
			}
		}
		else if(currentEquipment.IsArmour())
		{
			for (int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
			{
				if (((Armour)currentEquipment).IsHeadgear ())
				{
					characterDisplays [i].DisplayHeadgearStats ((Armour)currentEquipment);
				}
				else if (((Armour)currentEquipment).IsHandgear ())
				{
					characterDisplays [i].DisplayHandgearStats ((Armour)currentEquipment);
				}
				else if (((Armour)currentEquipment).IsBodyArmour ())
				{
					characterDisplays [i].DisplayBodyArmourStats ((Armour)currentEquipment);
				}
			}
		}
	}

	private void PurchaseItem()
	{
		PartyDataHolderScript.reference.goldCount -= itemButtons [lastButtonIdPressed].GetItem ().GetBuyAmount ();
		PartyDataHolderScript.reference.inventory.AddItemToInventory (itemButtons [lastButtonIdPressed].GetItem ());
		itemButtons [lastButtonIdPressed].UpdateQuantity ();
		goldDisplay.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString ();
	}

	private void SellItem()
	{
		PartyDataHolderScript.reference.goldCount += itemButtons [lastButtonIdPressed].GetItem ().GetSellAmount ();
		PartyDataHolderScript.reference.inventory.DeductItemFromTotal (itemButtons [lastButtonIdPressed].GetItem (), 1);
		itemButtons [lastButtonIdPressed].UpdateQuantity ();

		if (itemButtons [lastButtonIdPressed].GetItem ().GetQuantity () == 0)
		{
			scrollbar.value = 1.0f;
		}

		RefreshDisplay ();
	}

	public void BackButton()
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuCancel");
		if (lastButtonIdPressed > -1)
		{
			lastButtonIdPressed = -1;
			characterStatDisplay.enabled = false;
		}
		else
		{
			CloseShopMenu ();
		}
	}

	private void CloseShopMenu()
	{
		itemsForSale = new List<Item> ();
		shopDisplay.enabled = false;
		characterStatDisplay.enabled = false;
		buyMode = true;
		modeButtonText.text = "Mode: Buy";
		scrollbar.value = 1.0f;
		RemoveButtons ();
		itemButtons.Clear ();
		decisionPrompt.StartDecision ("Would you like to trade?", this.StartBuyMode, shopkeeper.SayGoodbye, null,
			"Shop", "Leave");
	}
}
