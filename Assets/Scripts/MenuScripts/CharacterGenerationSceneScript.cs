﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class CharacterGenerationSceneScript : MonoBehaviour 
{
	private const int MAX_PARTY_SIZE = 4;
	private int currentId = 0;
	private BaseCharacter currentCharacter;
	public InputField nameField;
	public Text className;
	public GameObject[] characterModels;

	public Text skillField;
	public Text hpField;
	public Text mpField;
	public Text physicalAttackField;
	public Text magicalAttackField;
	public Text physicalResistanceField;
	public Text magicalResistanceField;
	public Text speedField;
	public Text luckField;

	void Awake()
	{
		DatabaseHandler.SetupItemDatabase("ItemDatabase.db");
		DatabaseHandler.OpenItemDatabase ();

		DatabaseHandler.SetupSkillDatabase("SkillsDatabase.db");
		DatabaseHandler.OpenSkillDatabase ();
	}

	// Use this for initialization
	void Start () 
	{
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "", this.gameObject);
		currentCharacter = PartyDataHolderScript.reference.CreateNewBaseCharacter ();
		UpdateData ();
	}

	public void NextButtonClick()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		if(PartyDataHolderScript.reference.characters.Count < MAX_PARTY_SIZE)
		{
			if(nameField.text.Equals(""))
			{
				currentCharacter.SetName(nameField.GetComponentInChildren<Text>().text);
			}
			else
			{
				currentCharacter.SetName (nameField.text);
			}

			PartyDataHolderScript.reference.characters.Add(currentCharacter);

			if(PartyDataHolderScript.reference.characters.Count < MAX_PARTY_SIZE)
			{
				currentId++;
				currentCharacter = PartyDataHolderScript.reference.CreateNewBaseCharacter ();
				UpdateData();
			}
			else
			{
				BeginSceneTransition();
				//FIXME: display pop-up to allow the player to continue to the main game.
			}
		}

	}

	private void UpdateData()
	{
		if (currentId > 0)
		{
			characterModels [currentId - 1].transform.position = new Vector3 (100.0f, 100.0f, 100.0f);
		}

		nameField.text = "";
		currentCharacter.SetCurrentLevel (1);
		characterModels [currentId].transform.position = Vector3.zero;

		switch(currentId)
		{
		case 0:
			className.text = "Thief";
			nameField.GetComponentInChildren<Text>().text = "Garwen";
			currentCharacter.SetMaxHP(28);
			currentCharacter.SetMaxMP(10);
			currentCharacter.SetSpeed(10);
			currentCharacter.SetAttack(6);
			currentCharacter.SetDefence(3);
			currentCharacter.SetMagicAttack(3);
			currentCharacter.SetMagicDefence(3);
			currentCharacter.SetLuck(7);
			currentCharacter.SetSkillType(SkillType.Thievery);
			currentCharacter.AddSkillToList(new Skill(1, SkillType.Thievery));
			break;
		case 1:
			className.text = "Knight";
			nameField.GetComponentInChildren<Text>().text = "Eryhn";
			currentCharacter.SetMaxHP(35);
			currentCharacter.SetMaxMP(5);
			currentCharacter.SetSpeed(6);
			currentCharacter.SetAttack(9);
			currentCharacter.SetDefence(5);
			currentCharacter.SetMagicAttack(2);
			currentCharacter.SetMagicDefence(4);
			currentCharacter.SetLuck(2);
			currentCharacter.SetSkillType(SkillType.Gallantry);
			break;
		case 2:
			className.text = "White Mage";
			nameField.GetComponentInChildren<Text>().text = "Krista";
			currentCharacter.SetMaxHP(24);
			currentCharacter.SetMaxMP(20);
			currentCharacter.SetSpeed(4);
			currentCharacter.SetAttack(2);
			currentCharacter.SetDefence(2);
			currentCharacter.SetMagicAttack(8);
			currentCharacter.SetMagicDefence(9);
			currentCharacter.SetLuck(3);
			currentCharacter.SetSkillType(SkillType.WhiteMagic);
			currentCharacter.AddSkillToList(new Skill(0, SkillType.WhiteMagic));
			break;
		case 3:
			className.text = "Black Mage";
			nameField.GetComponentInChildren<Text>().text = "Fritz";
			currentCharacter.SetMaxHP(20);
			currentCharacter.SetMaxMP(25);
			currentCharacter.SetSpeed(3);
			currentCharacter.SetAttack(2);
			currentCharacter.SetDefence(2);
			currentCharacter.SetMagicAttack(9);
			currentCharacter.SetMagicDefence(8);
			currentCharacter.SetLuck(4);
			currentCharacter.SetSkillType(SkillType.BlackMagic);
			currentCharacter.AddSkillToList(new Skill(0, SkillType.BlackMagic));
			break;
		}
		currentCharacter.SetId (currentId);
		currentCharacter.SetCurrentHP(currentCharacter.GetMaxHP());
		currentCharacter.SetCurrentMP(currentCharacter.GetMaxMP());
		currentCharacter.RemoveAllStatuses();

		UpdateStatDisplay ();
	}

	private void UpdateStatDisplay()
	{
		skillField.text = currentCharacter.GetSkillType ().ToString();
		hpField.text = currentCharacter.GetMaxHP ().ToString();
		mpField.text = currentCharacter.GetMaxMP ().ToString ();
		physicalAttackField.text = currentCharacter.GetAttack ().ToString();
		magicalAttackField.text = currentCharacter.GetMagicAttack ().ToString();
		physicalResistanceField.text = currentCharacter.GetDefence ().ToString();
		magicalResistanceField.text = currentCharacter.GetMagicDefence ().ToString();
		speedField.text = currentCharacter.GetSpeed ().ToString();
		luckField.text = currentCharacter.GetLuck ().ToString ();
	}

	public void ApplyNameToCharacter(string name)
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
	}

	public void BeginSceneTransition()
	{
		if(!FadeoutScript.reference.active)
		{
			PartyDataHolderScript.reference.GenerateNewCharacterEquipment ();
			FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black, "MoveToNextScene", this.gameObject);
		}

		DatabaseHandler.CloseAllDatabases ();
	}

	void MoveToNextScene()
	{
		SceneManager.LoadScene ("field_scene");
	}
}
