﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterMenuScript : MonoBehaviour 
{
	BaseCharacter character;

	Image characterPortrait;
	Text levelText;
	Text experienceText;
	Text experienceToNextLevelText;
	Text classNameText;
	Text healthText;
	Text magicText;
	Text statusText;
	Text physicalAttackText;
	Text magicalAttackText;
	Text physicalResistanceText;
	Text magicalResistanceText;
	Text speedText;
	Text luckText;
	Text physicalDamageText;
	Text magicalDamageText;
	Text physicalDefenceText;
	Text magicalDefenceText;
	Text accuracyText;
	Text evasionText;

	int currentStatusListIndex;

	// Use this for initialization
	void Start () 
	{
		characterPortrait = GetComponentsInChildren<Image> () [4];
		currentStatusListIndex = 0;

		Text[] textElements = GetComponentsInChildren<Text> ();
		levelText = textElements [1];
		experienceText = textElements [2];
		experienceToNextLevelText = textElements [3];
		classNameText = textElements [4];
		healthText = textElements [5];
		magicText = textElements [6];
		statusText = textElements [7];
		physicalAttackText = textElements [8];
		magicalAttackText = textElements [9];
		physicalResistanceText = textElements [10];
		magicalResistanceText = textElements [11];
		speedText = textElements [12];
		luckText = textElements [13];
		physicalDamageText = textElements [14];
		magicalDamageText = textElements [15];
		physicalDefenceText = textElements [16];
		magicalDefenceText = textElements [17];
		accuracyText = textElements [18];
		evasionText = textElements [19];
	}
	
	void UpdateDetails(BaseCharacter character)
	{
		this.character = character;
		characterPortrait.sprite = ResourceLoader.LoadSprite (character.GetClassName () + "HeadShot");
		levelText.text = "Lvl: " + character.GetCurrentLevel ().ToString ();
		experienceText.text = "Exp: " + character.GetTotalExperiencePoints ().ToString();
		experienceToNextLevelText.text = "Nxt: " + (character.GetExperiencePointsForNextLevel () - character.GetCurrentExperiencePoints()).ToString();
		classNameText.text = character.GetClassName ();
		healthText.text = "Hp: " + character.GetCurrentHP ().ToString() + "/" + character.GetMaxHP ().ToString();
		magicText.text = "Mp: " + character.GetCurrentMP ().ToString() + "/" + character.GetMaxMP ().ToString();
		statusText.text = "Status: " + character.GetStatuses ()[0].ToString ();
		physicalAttackText.text = "P.Atk: " + character.GetAttack ().ToString();
		magicalAttackText.text = "M.Atk: " + character.GetMagicAttack ().ToString();
		physicalResistanceText.text = "P.Res: " + character.GetDefence ().ToString();
		magicalResistanceText.text = "M.Res: " + character.GetMagicDefence ().ToString();
		speedText.text = "Spd: " + character.GetSpeed ().ToString ();
		luckText.text = "Luck: " + character.GetLuck ().ToString ();
		physicalDamageText.text = "P.Dam: " + character.GetPhysicalWeaponDamage ().ToString ();
		magicalDamageText.text = "M.Dam: " + character.GetMagicalWeaponDamage ().ToString ();
		physicalDefenceText.text = "P.Def: " + character.GetPhysicalArmourDefence ().ToString ();
		magicalDefenceText.text = "M.Def: " + character.GetMagicalArmourDefence ().ToString ();
		accuracyText.text = "Acc: " + character.GetWeaponAccuracy().ToString ();
		evasionText.text = "Eva: " + character.GetArmourEvasion ().ToString ();

		if (character.GetStatuses ().Count > 1)
		{
			InvokeRepeating ("UpdateStatus", 1.0f, 1.0f);
		}
	}

	// [InvokeRepeating] Iterate through all the character's current status', displaying them to the player
	void UpdateStatusText()
	{
		currentStatusListIndex++;
		if (currentStatusListIndex > character.GetStatuses ().Count - 1)
		{
			currentStatusListIndex = 0;
		}

		statusText.text = "Status" + character.GetStatuses ()[currentStatusListIndex].ToString ();
	}
}
