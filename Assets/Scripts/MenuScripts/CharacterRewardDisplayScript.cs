﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterRewardDisplayScript : MonoBehaviour 
{
	public Slider experienceSlider;
	public Text levelDisplay;
	public Text experienceDisplay;
	public Text nameDisplay;
	public Text statIncreaseDisplay;

	private float statIncreaseLerp = 0;
	private Vector3 statDisplayStartPosition;

	BaseCharacter character;

	private int experienceEarned;
	List<string> statIncreases;

	// Use this for initialization
	void Start () 
	{
		experienceSlider = GetComponentInChildren<Slider> ();
		levelDisplay = GetComponentsInChildren<Text> ()[2];
		experienceDisplay = GetComponentsInChildren<Text> ()[1];
		nameDisplay = GetComponentsInChildren<Text> () [0];
		statIncreaseDisplay = GetComponentsInChildren<Text> () [3];
		statIncreaseDisplay.enabled = false;
		statDisplayStartPosition = statIncreaseDisplay.rectTransform.anchoredPosition3D;
		statIncreases = new List<string> ();
	}

	public void InitialiseUpdate(BaseCharacter character, int experienceEarned)
	{
		this.character = character;
		this.experienceEarned = experienceEarned;
		nameDisplay.text = character.GetName ();
		levelDisplay.text = "LVL: " + character.GetCurrentLevel ().ToString();
		experienceSlider.maxValue = character.GetExperiencePointsForNextLevel();
		experienceSlider.value = character.GetCurrentExperiencePoints();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (character != null)
		{
			DisplayStatIncreases ();
		}
	}

	public void UpdateExperience(ref bool characterLeveledUp)
	{
		if (experienceEarned != 0)
		{
			if (!characterLeveledUp)
			{
				experienceEarned--;
				character.AddExperiencePoints (1);

				experienceSlider.maxValue = character.GetExperiencePointsForNextLevel();
				experienceSlider.value = character.GetCurrentExperiencePoints();
				experienceDisplay.text = "EXP: " + character.GetTotalExperiencePoints().ToString() + " (" + experienceEarned.ToString() + ")";

				if (character.GetCurrentExperiencePoints () >= character.GetExperiencePointsForNextLevel ())
				{
					characterLeveledUp = true; 
					statIncreaseDisplay.enabled = true;

					List<int> currentStats = new List<int>(); 
					currentStats.Add (character.GetMaxHP ()); // 0
					currentStats.Add (character.GetMaxMP ()); // 1
					currentStats.Add (character.GetAttack ()); // 2
					currentStats.Add (character.GetDefence ()); // 3
					currentStats.Add (character.GetMagicAttack ()); // 4
					currentStats.Add (character.GetMagicDefence ()); // 5
					currentStats.Add (character.GetSpeed ()); // 6
					currentStats.Add (character.GetLuck ()); // 7

					character.SetCurrentLevel (character.GetCurrentLevel () + 1);
					character.SetCurrentExperiencePoints (0);
					character.ApplyLevelRewards ();

					List<int> newStats = new List<int>(); 
					newStats.Add (character.GetMaxHP ()); // 0
					newStats.Add (character.GetMaxMP ()); // 1
					newStats.Add (character.GetAttack ()); // 2
					newStats.Add (character.GetDefence ()); // 3
					newStats.Add (character.GetMagicAttack ()); // 4
					newStats.Add (character.GetMagicDefence ()); // 5
					newStats.Add (character.GetSpeed ()); // 6
					newStats.Add (character.GetLuck ()); // 7

					for (int i = 0; i < currentStats.Count; i++)
					{
						if (currentStats [i] != newStats [i])
						{
							switch (i)
							{
							case 0:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " HP");
								break;
							case 1:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " MP");
								break;
							case 2:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " P.ATK");
								break;
							case 3:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " P.RES");
								break;
							case 4:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " M.ATK");
								break;
							case 5:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " M.RES");
								break;
							case 6:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " SPEED");
								break;
							case 7:
								statIncreases.Add ("+" + (newStats [i] - currentStats [i]) + " LUCK");
								break;
							}

							Invoke ("DisplayStatIncreases", 1.0f);
						}
					}
				}
			}

			if (characterLeveledUp && character.GetCurrentExperiencePoints () == 0)
			{
				characterLeveledUp = statIncreaseDisplay.enabled;
			}

		}
	}

	void DisplayStatIncreases()
	{
		if (statIncreases.Count > 0)
		{
			if (statIncreaseDisplay.text != statIncreases [0])
			{
				statIncreaseDisplay.enabled = true;
				if (statIncreaseLerp >= 1.0f)
				{
					statIncreaseLerp = 0;
				}
				statIncreaseDisplay.text = statIncreases [0];
				statIncreaseDisplay.rectTransform.position = statDisplayStartPosition;
				statIncreaseDisplay.color = Color.clear;
			}
				
			if (statIncreaseLerp >= 1.0f)
			{
				Invoke ("DisplayStatIncreases", 1.5f);
				statIncreases.RemoveAt (0);
			}
			else
			{
				statIncreaseLerp += Time.smoothDeltaTime;
				statIncreaseDisplay.rectTransform.anchoredPosition3D = Vector3.Lerp (statDisplayStartPosition, statDisplayStartPosition + new Vector3 (0, 10, 0), statIncreaseLerp);
				statIncreaseDisplay.color = Color.Lerp (Color.clear, Color.white, statIncreaseLerp);
			}
		}
		else
		{
			levelDisplay.text = "LVL: " + character.GetCurrentLevel ().ToString();
			experienceSlider.maxValue = character.GetExperiencePointsForNextLevel();
			experienceSlider.value = character.GetCurrentExperiencePoints();
			statIncreaseDisplay.enabled = false;
		}
	}
}
