﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecisionPromptScript : MonoBehaviour 
{
	Action yesMethod;
	Action noMethod;

	Button yesButton;
	Button noButton;
	Text yesText;
	Text noText;
	Text prompt;
	Animator animator;

	CanvasGroup callingGroup;

	string confirmSFXName = "MenuConfirm";
	string cancelSFXName = "MenuCancel";

	void Start()
	{
		yesButton = GetComponentInChildren<Button> ();
		noButton = GetComponentsInChildren<Button> () [1];
		yesText = yesButton.GetComponentInChildren<Text>();
		noText = noButton.GetComponentInChildren<Text>();
		prompt = GetComponentInChildren<Text> ();
		animator = GetComponentInChildren<Animator> ();
	}

	public void StartDecision (string prompt, Action YesMethod, Action NoMethod = null, CanvasGroup canvasGroup = null,
								string yesString = "Yes", string noString = "No") 
	{
		yesMethod = YesMethod;
		noMethod = NoMethod;
		yesButton.enabled = true;
		noButton.enabled = true;
		yesText.text = yesString;
		noText.text = noString;
		this.prompt.text = prompt;
		callingGroup = canvasGroup;
		Enable (true);
		confirmSFXName = "MenuConfirm";
		cancelSFXName = "MenuCancel";

		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
	}
	
	public void Confirm()
	{
		AudioManagerScript.reference.PlayMenuEffect (confirmSFXName);
		if (yesMethod != null)
		{
			// ? operator only makes the method call if the left hand variable is not null
			yesMethod ();
		}

		yesMethod = null;
		noMethod = null;
		Enable (false);
	}

	public void Deny()
	{
		AudioManagerScript.reference.PlayMenuEffect (cancelSFXName);
		if (noMethod != null)
		{
			noMethod ();
		}

		yesMethod = null;
		noMethod = null;
		Enable (false);
	}

	public void Enable(bool enable)
	{
		if (animator != null)
		{
			animator.SetBool("Open", enable);
			GetComponent<Canvas> ().enabled = enable;

			if (callingGroup != null)
			{
				callingGroup.interactable = !enable;
			}
		}
	}

	public void SetConfirmSFX(string audioName)
	{
		confirmSFXName = audioName;
	}

	public void SetCancelSFX(string audioName)
	{
		cancelSFXName = audioName;
	}
}
