﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EquipmentConfirmationDialogScript : MonoBehaviour 
{
	// Data members to store the relevant character and equipment data //
	private BaseCharacter currentCharacter;
	private Equipment newEquipment;

	// Data members representing old equipment details // 
	private Text oldName;
	private Text oldPhysicalStat;
	private Text oldMagicalStat;
	private Text oldMiscStat;

	// Data members representing new equipment details // 
	private Text newName;
	private Text newPhysicalStat;
	private Text newMagicalStat;
	private Text newMiscStat;

	// Misc data members //
	private Animator menuAnimator;

	// Use this for initialization
	void Start () 
	{
		Text[] textElements = GetComponentsInChildren<Text> ();

		oldName = textElements [2];
		oldPhysicalStat = textElements [3];
		oldMagicalStat = textElements [4];
		oldMiscStat = textElements [5];

		newName = textElements [7];
		newPhysicalStat = textElements [8];
		newMagicalStat = textElements [9];
		newMiscStat = textElements [10];

		menuAnimator = GetComponent<Animator> ();
		this.enabled = false;
	}
	
	public void Setup(BaseCharacter character, Equipment newEquipment)
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuButton");
		currentCharacter = character;
		this.newEquipment = newEquipment;

		if(newEquipment.IsWeapon())
		{
			UpdateText (character.GetCurrentWeapon(), (Weapon)newEquipment);
		}
		else if(newEquipment.IsArmour())
		{
			if(((Armour)newEquipment).IsHeadgear())
			{
				UpdateText(character.GetCurrentHeadgear(), (Armour)newEquipment);
			}
			else if(((Armour)newEquipment).IsHandgear())
			{
				UpdateText(character.GetCurrentHandgear(), (Armour)newEquipment);
			}
			else if(((Armour)newEquipment).IsBodyArmour())
			{
				UpdateText(character.GetCurrentBodyArmour(), (Armour)newEquipment);
			}
		}

		this.enabled = true;
		menuAnimator.SetBool ("Open", true);
	}

	private void UpdateText(Weapon oldWeapon, Weapon newWeapon)
	{
		if(oldWeapon != null)
		{
			oldName.text = oldWeapon.GetName ();
			oldPhysicalStat.text = "P.Dam: " + oldWeapon.GetPhysicalDamage ().ToString();
			oldMagicalStat.text = "M.Dam: " + oldWeapon.GetMagicalDamage ().ToString();
			oldMiscStat.text = "Acc: " + oldWeapon.GetAccuracy ().ToString();
		}
		else
		{
			oldName.text = "Nothing";
			oldPhysicalStat.text = "P.Dam: 0";
			oldMagicalStat.text = "M.Dam: 0";
			oldMiscStat.text = "Acc: 100";
		}

		newName.text = newWeapon.GetName ();
		newPhysicalStat.text = "P.Dam: " + newWeapon.GetPhysicalDamage ().ToString();
		newMagicalStat.text = "M.Dam: " + newWeapon.GetMagicalDamage ().ToString();
		newMiscStat.text = "Acc: " + newWeapon.GetAccuracy ().ToString();

		if(oldWeapon != null)
		{
			UpdateTextColour (oldWeapon.GetPhysicalDamage (), newWeapon.GetPhysicalDamage (),
			                  oldPhysicalStat, newPhysicalStat);
			
			UpdateTextColour (oldWeapon.GetMagicalDamage (), newWeapon.GetMagicalDamage (),
			                  oldMagicalStat, newMagicalStat);
			
			UpdateTextColour (oldWeapon.GetAccuracy (), newWeapon.GetAccuracy (),
			                  oldMiscStat, newMiscStat);
		}
		else
		{
			UpdateTextColour (0, newWeapon.GetPhysicalDamage (),
			                  oldPhysicalStat, newPhysicalStat);
			
			UpdateTextColour (0, newWeapon.GetMagicalDamage (),
			                  oldMagicalStat, newMagicalStat);
			
			UpdateTextColour (100, newWeapon.GetAccuracy (),
			                  oldMiscStat, newMiscStat);
		}

	}

	private void UpdateText(Armour oldArmour, Armour newArmour)
	{
		if(oldArmour != null)
		{
			oldName.text = oldArmour.GetName ();
			oldPhysicalStat.text = "P.Def: " + oldArmour.GetPhysicalDefence ().ToString();
			oldMagicalStat.text = "M.Def: " + oldArmour.GetMagicalDefence ().ToString();
			oldMiscStat.text = "Eva: " + oldArmour.GetEvasion ().ToString();
		}
		else
		{
			oldName.text = "Nothing";
			oldPhysicalStat.text = "P.Def: 0";
			oldMagicalStat.text = "M.Def: 0";
			oldMiscStat.text = "Eva: 0";
		}

		newName.text = newArmour.GetName ();
		newPhysicalStat.text = "P.Def: " + newArmour.GetPhysicalDefence ().ToString();
		newMagicalStat.text = "M.Def: " + newArmour.GetMagicalDefence ().ToString();
		newMiscStat.text = "Eva: " + newArmour.GetEvasion ().ToString();

		if(oldArmour != null)
		{
			UpdateTextColour (oldArmour.GetPhysicalDefence (), newArmour.GetPhysicalDefence (),
			                  oldPhysicalStat, newPhysicalStat);
			
			UpdateTextColour (oldArmour.GetMagicalDefence (), newArmour.GetMagicalDefence (),
			                  oldMagicalStat, newMagicalStat);
			
			UpdateTextColour (oldArmour.GetEvasion (), newArmour.GetEvasion (),
			                  oldMiscStat, newMiscStat);
		}
		else
		{
			UpdateTextColour (0, newArmour.GetPhysicalDefence (),
			                  oldPhysicalStat, newPhysicalStat);
			
			UpdateTextColour (0, newArmour.GetMagicalDefence (),
			                  oldMagicalStat, newMagicalStat);
			
			UpdateTextColour (0, newArmour.GetEvasion (),
			                  oldMiscStat, newMiscStat);
		}

	}

	private void UpdateTextColour(int oldStatValue, int newStatValue, Text oldStatText, Text newStatText)
	{
		if(oldStatValue != newStatValue)
		{
			if(oldStatValue > newStatValue)
			{
				oldStatText.color = Color.green;
				newStatText.color = Color.red;
			}
			else
			{
				oldStatText.color = Color.red;
				newStatText.color = Color.green;
			}
		}
		else
		{
			oldStatText.color = Color.white;
			newStatText.color = Color.white;
		}
	}

	public void Reset()
	{
		newEquipment = null;
		currentCharacter = null;
		menuAnimator.SetBool ("Open", false);
		this.enabled = false;
	}

	public BaseCharacter GetCharacter()
	{
		return currentCharacter;
	}

	public Equipment GetEquipment()
	{
		return newEquipment;
	}
}
