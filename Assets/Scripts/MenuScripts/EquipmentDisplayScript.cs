﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EquipmentDisplayScript : MonoBehaviour 
{
	Weapon associatedWeapon;
	Armour associatedArmour;

	Text itemName;
	Text itemElement;
	Text itemType;
	Text itemPhysicalValue;
	Text itemMagicalValue;
	Text itemAccuracyValue;

	// Use this for initialization
	void Start () 
	{
		Text[] textElements = GetComponentsInChildren<Text> ();
		itemName = textElements [1];
		itemType = textElements [2];
		itemPhysicalValue = textElements [3];
		itemMagicalValue = textElements [4];
		itemElement = textElements [5];
		itemAccuracyValue = textElements [6];
	}
	
	public void AssignItem(Equipment equipment)
	{
		if(equipment != null)
		{
			if(equipment.IsArmour())
			{  
				associatedArmour = (Armour)equipment;
				GenerateWithArmour();
			} 
			else if(equipment.IsWeapon())
			{
				associatedWeapon = (Weapon)equipment;
				GenerateWithWeapon();
			}
		}
		else
		{
			GenerateWithNoEquipment();
		}
	}

	void GenerateWithWeapon()
	{
		itemName.text = associatedWeapon.GetName ();
		itemType.text = associatedWeapon.GetWeaponType ().ToString ();
		itemPhysicalValue.text = "P.Atk: " + associatedWeapon.GetPhysicalDamage ().ToString();
		itemMagicalValue.text = "M.Atk: " + associatedWeapon.GetMagicalDamage ().ToString();
		itemAccuracyValue.text = "Acc: " + associatedWeapon.GetAccuracy ().ToString();
		ApplyElementToText (associatedWeapon.GetElement());
	}

	void GenerateWithArmour()
	{
		itemName.text = associatedArmour.GetName ();
		itemType.text = associatedArmour.GetArmourType ().ToString ();
		itemPhysicalValue.text = "P.Def: " + associatedArmour.GetPhysicalDefence ().ToString();
		itemMagicalValue.text = "M.Def: " + associatedArmour.GetMagicalDefence ().ToString();
		itemAccuracyValue.text = "Eva: " + associatedArmour.GetEvasion ().ToString();
		ApplyElementToText (associatedArmour.GetElement());
	}

	void GenerateWithNoEquipment()
	{
		itemName.text = "Nothing";
		itemType.text = "N/A";
		itemPhysicalValue.text = "0";
		itemMagicalValue.text = "0";
		itemAccuracyValue.text = "0";
		ApplyElementToText (Element.None);
	}

	void ApplyElementToText(Element element)
	{
		itemElement.text = "[" + element.ToString() + "]";
		itemElement.enabled = true;

		switch(element)
		{
		case Element.None:
			itemElement.enabled = false;
			break;
		case Element.Dark:
			itemElement.color = Color.gray;
			break;
		case Element.Flame:
			itemElement.color = Color.red;
			break;
		case Element.Frost:
			itemElement.color = Color.cyan;
			break;
		case Element.Spark:
			itemElement.color = Color.yellow;
			break;
		case Element.Heal:
			itemElement.color = Color.green;
			break;
		case Element.Holy:
			itemElement.color = Color.white;
			break;
		}
	}
}
