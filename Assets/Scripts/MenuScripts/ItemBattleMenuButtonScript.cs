﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemBattleMenuButtonScript : MonoBehaviour 
{
	private Item associatedItem;
	private Text nameText;
	private Text infoText;

	// Use this for initialization
	void Awake () 
	{
		nameText = GetComponentInChildren<Text> ();
		infoText = GetComponentsInChildren<Text> () [1];
	}

	void UpdateButton (int inventoryId) 
	{
		nameText.enabled = true;
		infoText.enabled = true;

		if(inventoryId < PartyDataHolderScript.reference.inventory.GetAllItems().Count)
		{
			associatedItem = PartyDataHolderScript.reference.inventory.GetItem (inventoryId);
		}
		else
		{
			associatedItem = null;
			nameText.text = "";
			infoText.text = "";
		}

		if(associatedItem != null)
		{
			nameText.text = associatedItem.GetName();

			if(typeof(Equipment).IsInstanceOfType(associatedItem))
			{
				// Get Equipped state
				if(associatedItem.IsEquipped())
				{
					infoText.text = "[E: " + ((Equipment)associatedItem).GetEquippedBy().characterName + "]";
				}
				else
				{
					infoText.text = "Unusable";
				}
			}
			else
			{
				infoText.text = "x"+associatedItem.GetQuantity();
			}
		}
	}

	public Item GetAssociatedItem()
	{
		return associatedItem;
	}
}
