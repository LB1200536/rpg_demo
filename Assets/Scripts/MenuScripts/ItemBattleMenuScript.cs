﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ItemBattleMenuScript : MonoBehaviour 
{
	int scrollCount = 1; // min = 1
	int maxScreens;
	Button[] itemButtons;
	// Use this for initialization
	void Start () 
	{
		scrollCount = 1;
		itemButtons = GetComponentsInChildren<Button> ();
	}

	void UpdateButtons()
	{
		maxScreens = (PartyDataHolderScript.reference.inventory.GetAllItems().Count / (itemButtons.Length - 3));
		if((PartyDataHolderScript.reference.inventory.GetAllItems().Count % (itemButtons.Length - 3)) != 0)
		{
			maxScreens += 1; // Allow excess items to be shown
		}

		for(int i = 0; i < itemButtons.Length; i++)
		{
			itemButtons[i].enabled = true;

			if(i < itemButtons.Length -3)
			{
				int itemIndex = i + (itemButtons.Length * (scrollCount - 1));
				
				if(itemIndex < PartyDataHolderScript.reference.inventory.GetAllItems().Count)
				{
					if(!PartyDataHolderScript.reference.inventory.GetItem(itemIndex).GetUsable())
					{
						itemButtons[i].GetComponentsInChildren<Text>()[0].color = Color.gray;
						itemButtons[i].GetComponentsInChildren<Text>()[1].color = Color.gray;
					}
					else
					{
						itemButtons[i].GetComponentsInChildren<Text>()[0].color = Color.white;
						itemButtons[i].GetComponentsInChildren<Text>()[1].color = Color.white;
					}
				}
				
				itemButtons[i].SendMessage("UpdateButton", itemIndex);
			}
		}
	}
	
	public void ScrollUp()
	{
		// If there are no more screens available above
		if(scrollCount != 1)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount--;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
		
	}
	
	public void ScrollDown()
	{
		if(scrollCount != maxScreens)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount++;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}
}
