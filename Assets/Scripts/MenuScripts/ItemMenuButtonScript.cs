﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemMenuButtonScript : MonoBehaviour 
{
	private Item associatedItem;
	private Image itemImage;
	private Text nameText;
	private Text infoText;

	// Use this for initialization
	void Awake () 
	{
		nameText = GetComponentInChildren<Text> ();
		infoText = GetComponentsInChildren<Text> () [1];
		itemImage = GetComponentsInChildren<Image> ()[1];
	}
	
	void UpdateButton (int inventoryId) 
	{
		nameText.enabled = true;
		infoText.enabled = true;

		if(inventoryId < PartyDataHolderScript.reference.inventory.GetAllItems().Count)
		{
			associatedItem = PartyDataHolderScript.reference.inventory.GetItem (inventoryId);
		}
		else
		{
			associatedItem = null;
			itemImage.sprite = null;
			itemImage.enabled = false;
			nameText.text = "";
			infoText.text = "";
		}

		if(associatedItem != null)
		{
			nameText.text = associatedItem.GetName();
			itemImage.enabled = true;
			itemImage.sprite = associatedItem.GetMenuImage();

			if(typeof(Equipment).IsInstanceOfType(associatedItem))
			{
				// Get Equipped state
				if(associatedItem.IsEquipped())
				{
					infoText.text = "[E: " + ((Equipment)associatedItem).GetEquippedBy().characterName + "]";
					infoText.color = Color.white;
				}
				else
				{
					if(((Equipment)associatedItem).IsWeapon())
					{
						infoText.text = "P.Atk: " + ((Weapon)associatedItem).GetPhysicalDamage().ToString() +
							" M.Atk: " + ((Weapon)associatedItem).GetMagicalDamage().ToString() + "\n" +
								"Acc: " + ((Weapon)associatedItem).GetAccuracy().ToString();
					}
					else
					{
						infoText.text = "P.Def: " + ((Armour)associatedItem).GetPhysicalDefence().ToString() +
							" M.Def: " + ((Armour)associatedItem).GetMagicalDefence().ToString() + "\n" +
								"Eva: " + ((Armour)associatedItem).GetEvasion().ToString();
					}
					infoText.color = Color.white;
				}
			}
			else
			{
				infoText.color = Color.white;
				infoText.text = "x" + associatedItem.GetQuantity().ToString() + "\n" +
					"Heals ";

				if (associatedItem.GetItemType () == ItemType.HP_Healing)
				{
					infoText.text += associatedItem.GetEffectAmount ().ToString () + "HP";
				}
				else if(associatedItem.GetItemType () == ItemType.MP_Healing)
				{
					infoText.text += associatedItem.GetEffectAmount ().ToString () + "MP";
				}
				else if(associatedItem.GetItemType () == ItemType.Status_Healing)
				{
					infoText.text += associatedItem.GetStatus ();
				}
			}
		}
	}

	public Item GetAssociatedItem()
	{
		return associatedItem;
	}
}
