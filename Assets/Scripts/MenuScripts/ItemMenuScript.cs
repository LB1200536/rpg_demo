﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ItemMenuScript : MonoBehaviour 
{
	private const int SCREEN_BUTTON_COUNT = 15;
	private int maxScreens;
	private int scrollCount; // keeps track of how many screens 'down' the player is in their inventory
	private Button sortButton;
	private Text pageCount;
	private Button[] itemButtons;
	private Item selectedItem;

	// Use this for initialization
	void Start () 
	{
		itemButtons = GetComponentsInChildren<Button> ();
		sortButton = GetComponentsInChildren<Button> ()[18];
		scrollCount = 1;

		pageCount = GetComponentsInChildren<Text> () [1];

		UpdateButtons ();
	}

	void UpdateButtons()
	{
		maxScreens = (PartyDataHolderScript.reference.inventory.GetAllItems().Count / (itemButtons.Length - 2));
		if((PartyDataHolderScript.reference.inventory.GetAllItems().Count % (itemButtons.Length - 2)) != 0)
		{
			maxScreens += 1; // Allow excess items to be shown
		}

		if(PartyDataHolderScript.reference.inventory.IsSortAscending())
		{
			sortButton.GetComponentInChildren<Text>().text = "Sort\nId: \n[Ascending]";
		}
		else
		{
			sortButton.GetComponentInChildren<Text>().text = "Sort\nId: \n[Descending]";
		}

		pageCount.text = scrollCount.ToString () + "/" + maxScreens.ToString();

		// magic numbers account for the scroll buttons and the close button, which should not be accounted for here
		for(int i = 2; i < itemButtons.Length - 2; i++)
		{
			itemButtons[i].SendMessage("UpdateButton", (i-2) + (SCREEN_BUTTON_COUNT * (scrollCount - 1)));
		}
	}

	public void ScrollUp()
	{
		// If there are no more screens available above
		if(scrollCount != 1)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount--;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}

	}

	public void ScrollDown()
	{
		if(PartyDataHolderScript.reference.inventory.GetAllItems().Count > SCREEN_BUTTON_COUNT * scrollCount 
		   || scrollCount != maxScreens)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount++;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public void SortInventory()
	{
		AudioManagerScript.reference.PlayMenuEffect("MenuButton");
		PartyDataHolderScript.reference.inventory.SortById ();
		UpdateButtons ();
	}

	public void ItemUsed()
	{
		if(selectedItem.GetUsable())
		{
			PartyDataHolderScript.reference.inventory.DeductItemFromTotal(selectedItem ,1);
		}

		UpdateButtons ();
	}

	public void UseItem(int buttonId)
	{
		selectedItem = itemButtons [buttonId + 2].GetComponent<ItemMenuButtonScript> ().GetAssociatedItem ();

		if(selectedItem != null && selectedItem.GetUsable())
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			transform.parent.gameObject.SendMessage("StartCharacterSelect", "UseItem");
			GetComponent<Canvas>().enabled = false;
		}
		else if(!selectedItem.GetUsable() && !selectedItem.IsKeyItem())
		{
			if(((Equipment)selectedItem).IsEquipped())
			{
				// Allow the player to unequip item
				AudioManagerScript.reference.PlayMenuEffect("ItemEquipped");
				PartyDataHolderScript.reference.characters[((Equipment)selectedItem).GetEquippedBy().characterId].UnequipItem((Equipment)selectedItem);
				UpdateButtons();
			}
			else
			{
				// Start the equipping process
				AudioManagerScript.reference.PlayMenuEffect("MenuButton");
				transform.parent.gameObject.SendMessage("StartCharacterSelect", "EquipItem");
				GetComponent<Canvas>().enabled = false;
			}
		}
		else // Item is a key item and cannot be used
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public Item GetItemToUse()
	{
		return selectedItem;
	}
}
