﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class NewSkillPopUpScript : MenuPopUpScript 
{
	private List<string> messagesToShow;
	private const float MESSAGE_DISPLAY_TIME = 1.5f; // How long a message should display before closing
	private const float DELAY_BETWEEN_MESSAGES = 0.25f; // Delay between closing one message, and opening another

	// Use this for initialization
	new protected void Start () 
	{
		base.Start ();
		messagesToShow = new List<string> ();
	}

	public void GenerateNewMessage(string characterName, Skill newSkill)
	{
		messagesToShow.Add (characterName + " has learned a new skill:\n\n" + newSkill.GetName ());

		if(messagesToShow.Count == 1)
		{
			StartMessage();
		}
	}

	private void StartMessage()
	{
		AudioManagerScript.reference.PlayMenuEffect ("PointGainComplete");
		OpenWithoutMessage ();

		message.text = messagesToShow[0];
		Invoke ("EndMessage", MESSAGE_DISPLAY_TIME);
	}

	private void EndMessage()
	{
		DisablePopUp ();

		messagesToShow.RemoveAt (0);
		if(messagesToShow.Count > 0)
		{
			Invoke("StartMessage", DELAY_BETWEEN_MESSAGES);
		}
	}
}
