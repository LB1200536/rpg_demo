﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsMenuScript : MonoBehaviour 
{
	private Canvas optionsCanvas;
	private Toggle oneMPToggle;
	private Toggle waitToggle;
	private Toggle bgmToggle;
	private Toggle analogueToggle;
	private Slider battleSpeedSlider;
	private Slider screenShakeStrengthSlider;
	private Slider textSpeedSlider;

	// Use this for initialization
	void Start () 
	{
		// Turn the menu audio source off while applying changes to the UI elements to better
		// represent the state they were left in while playing last.
		AudioManagerScript.reference.SetMenuSourceOn (false);
		optionsCanvas = GetComponent<Canvas>();
		battleSpeedSlider = optionsCanvas.GetComponentInChildren<Slider>();
		screenShakeStrengthSlider = optionsCanvas.GetComponentsInChildren<Slider>()[1];
		textSpeedSlider = optionsCanvas.GetComponentsInChildren<Slider>()[2];
		oneMPToggle = optionsCanvas.GetComponentsInChildren<Toggle>()[0];
		waitToggle = optionsCanvas.GetComponentsInChildren<Toggle>()[1];
		bgmToggle = optionsCanvas.GetComponentsInChildren<Toggle>()[2];
		analogueToggle = optionsCanvas.GetComponentsInChildren<Toggle> () [3];

		battleSpeedSlider.value = PlayerPrefs.GetFloat ("battleSpeed", 0.075f);
		screenShakeStrengthSlider.value = PlayerPrefs.GetFloat ("shakeIntensity", 1.0f);
		textSpeedSlider.value = PlayerPrefs.GetFloat ("textSpeed", 1.0f);
		
		if(PlayerPrefs.GetInt("oneMPCost") == 0)
		{
			oneMPToggle.isOn = true;
		}
		else
		{
			oneMPToggle.isOn = false;
		}
		
		if(PlayerPrefs.GetInt("waitMode") == 0)
		{
			waitToggle.isOn = true;
		}
		else
		{
			waitToggle.isOn = false;
		}

		if(PlayerPrefs.GetInt("useAnalogue", 1) == 0)
		{
			analogueToggle.isOn = true;
		}
		else
		{
			analogueToggle.isOn = false;
		}
		
		if(PlayerPrefs.GetInt("playBGM", 0) == 0)
		{
			bgmToggle.isOn = true;
			AudioManagerScript.reference.GetBGMSource ().volume = 0.5f;
		}
		else
		{
			bgmToggle.isOn = false;
			AudioManagerScript.reference.GetBGMSource ().volume = 0.0f;
		}

		// Turn the menu audio source back on after applying all the changes

		AudioManagerScript.reference.SetMenuSourceOn (true);
	}

	public void OpenMenu()
	{
		optionsCanvas.enabled = true;
		optionsCanvas.GetComponentInChildren<Animator>().SetBool("Open", true);
	}

	public void CloseMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");
		optionsCanvas.GetComponentInChildren<Animator>().SetBool("Open", false);
		optionsCanvas.enabled = false;
		//BroadcastMessage ("SetRotationSpeed", 0.001f);
		Camera.main.gameObject.BroadcastMessage ("SetRotationSpeed", 0.0005f, SendMessageOptions.DontRequireReceiver);

		PlayerPrefs.SetFloat ("battleSpeed", battleSpeedSlider.value);
		PlayerPrefs.SetFloat ("shakeIntensity", screenShakeStrengthSlider.value);
		PlayerPrefs.SetFloat ("textSpeed", textSpeedSlider.value);
		
		if(oneMPToggle.isOn)
		{
			PlayerPrefs.SetInt ("oneMPCost", 0);
		}
		else
		{
			PlayerPrefs.SetInt ("oneMPCost", 1);
		}
		
		if(waitToggle.isOn)
		{
			PlayerPrefs.SetInt ("waitMode", 0);
		}
		else
		{
			PlayerPrefs.SetInt ("waitMode", 1);
		}
	}
	
	public void ToggleOneMPCost()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		if(oneMPToggle.isOn)
		{
			PlayerPrefs.SetInt ("oneMPCost", 0);
		}
		else
		{
			PlayerPrefs.SetInt ("oneMPCost", 1);
		}
	}
	
	public void ToggleWaitMode()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		if(waitToggle.isOn)
		{
			PlayerPrefs.SetInt ("waitMode", 0);
		}
		else
		{
			PlayerPrefs.SetInt ("waitMode", 1);
		}
	}
	
	public void ToggleBGMOn()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		if(bgmToggle.isOn)
		{
			PlayerPrefs.SetInt("playBGM", 0);
			AudioManagerScript.reference.GetBGMSource ().volume = 0.5f;
		}
		else
		{
			PlayerPrefs.SetInt("playBGM", 1);
			AudioManagerScript.reference.GetBGMSource ().volume = 0.0f;
		}
	}

	public void ToggleAnalogueOn()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuButton");
		if(analogueToggle.isOn)
		{
			PlayerPrefs.SetInt("useAnalogue", 0);
		}
		else
		{
			PlayerPrefs.SetInt("useAnalogue", 1);
		}
	}
	
	public void SetBattleSpeedSlider()
	{
		PlayerPrefs.SetFloat ("battleSpeed", battleSpeedSlider.value);
	}

	public void SetScreenShakeStrengthSlider()
	{
		PlayerPrefs.SetFloat ("shakeIntensity", screenShakeStrengthSlider.value);
	}

	public void SetTextSpeedSlider()
	{
		PlayerPrefs.SetFloat ("textSpeed", textSpeedSlider.value);
	}
}
