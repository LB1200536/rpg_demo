﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SaveSlotScript : MonoBehaviour
{
	public int slotId = 0;

	private Canvas slotCanvas;

	private string locationValue;
	private string dateValue;
	private string timeValue;
	//private string nameValue; // Not currently in use

	private Text locationText;
	private Text dateText;
	private Text timeText;
	//private Text nameText; // Not currently in use
	private Text startButtonText;

	public DecisionPromptScript decisionPrompt;
	public CanvasGroup canvasGroup;

	void Awake()
	{
		slotCanvas = transform.parent.gameObject.GetComponent<Canvas> ();
		Text[] textFields = this.GetComponentsInChildren<Text>();
		dateText = textFields [1];
		timeText = textFields [2];
		locationText = textFields [3];
		startButtonText = this.GetComponentInChildren<Button> ().GetComponentInChildren<Text>();
	}

	// Use this for initialization
	void Start () 
	{
		if(slotId != 0)
		{
			FileData fileData = SavedDataManager.LoadFileData(slotId);
			AreaXMLParserScript areaParser = new AreaXMLParserScript();

			if(fileData != null)
			{
				locationValue = "Location: " + areaParser.GetAreaNameById(fileData.areaId);
				// Parse room Id in XML to find the name of the location
				dateValue = "Date: " + fileData.dateSaved;
				timeValue = "Time: " + fileData.timeSaved;
				//nameValue = fileData.mainCharacterName;

				// Assign data to the UI elements
				locationText.text = locationValue;
				dateText.text = dateValue;
				timeText.text = timeValue;
				startButtonText.text = "Continue";
			}
			else
			{
				// Disable UI elements due to no valid data
				locationText.enabled = false;
				dateText.enabled = false;
				timeText.enabled = false;
				startButtonText.text = "Begin";
			}
		}
		else
		{
			Debug.LogError("Save slot not identified correctly. Please review save slots in the inspector.");
		}
	}

	public void OpenSlot()
	{
		if(!slotCanvas.enabled)
		{
			slotCanvas.enabled = true;
		}
		GetComponent<Animator>().SetBool("Open", true);
	}

	public void CloseSlot()
	{
		if(slotCanvas.enabled)
		{
			slotCanvas.enabled = false;
		}
		GetComponent<Animator>().SetBool("Open", false);
	}

	public void StartDeletionPrompt()
	{
		if(SavedDataManager.SaveFileExists(slotId))
		{
			decisionPrompt.StartDecision ("Are you sure you want to delete this save? It cannot be recovered if deleted...", this.DeleteSave, null, canvasGroup);
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuError");
		}
	}

	private void DeleteSave()
	{
		if(SavedDataManager.SaveFileExists(slotId))
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
			SavedDataManager.DeleteSaveFile (slotId);
			Start ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect ("MenuError");
		}
	}

	public void StartGame()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		PlayerPrefs.SetInt ("saveSlot", slotId);
		FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black, "LoadLevel", this.gameObject);
	}
	
	private void LoadLevel()
	{
		if(startButtonText.text.Equals("Begin"))
		{
			SceneManager.LoadScene("character_generation_scene");
		}
		else
		{
			SceneManager.LoadScene("field_scene");
		}
	}
}
