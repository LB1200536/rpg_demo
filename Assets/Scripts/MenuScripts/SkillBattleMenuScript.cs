﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class SkillBattleMenuScript : MonoBehaviour 
{
	int scrollCount = 1; // min = 1
	int maxScreens;
	Button[] skillButtons;
	private BaseCharacter selectedCharacter;

	// Use this for initialization
	void Start () 
	{
		scrollCount = 1;
		skillButtons = GetComponentsInChildren<Button> ();
	}

	public void SetSelectedCharacter(BaseCharacter character)
	{
		selectedCharacter = character;
		maxScreens = (1 + (selectedCharacter.GetSkillList().Count / skillButtons.Length));
		if((selectedCharacter.GetSkillList().Count % (skillButtons.Length - 3)) != 0)
		{
			maxScreens += 1; // Allow excess skills to be shown
		}
		UpdateButtons ();
	}

	void UpdateButtons()
	{
		for(int i = 0; i < skillButtons.Length; i++)
		{
			skillButtons[i].enabled = true;

			if(i < skillButtons.Length -3)
			{
				int skillIndex = i + (6 * (scrollCount - 1));
				
				if(skillIndex < selectedCharacter.GetSkillList().Count)
				{
					if(selectedCharacter.GetSkillList()[skillIndex].GetMPCost() > selectedCharacter.GetCurrentMP())
					{
						skillButtons[i].GetComponentsInChildren<Text>()[0].color = Color.gray;
						skillButtons[i].GetComponentsInChildren<Text>()[1].color = Color.gray;
					}
					else
					{
						skillButtons[i].GetComponentsInChildren<Text>()[0].color = Color.white;
						skillButtons[i].GetComponentsInChildren<Text>()[1].color = Color.white;
					}

					skillButtons[i].SendMessage("UpdateButtonWithSkill", selectedCharacter.GetSkillList()[skillIndex]);
				}
				else
				{
					skillButtons[i].SendMessage("UpdateButtonWithoutSkill", null);
				}
			}
		}
	}
	
	public void ScrollUp()
	{
		// If there are no more screens available above
		if(scrollCount != 1)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount--;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}
	
	public void ScrollDown()
	{
		if(scrollCount != maxScreens)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount++;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}
}
