﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SkillMenuButtonScript : MonoBehaviour 
{
	private int id;
	private Skill associatedSkill;
	private Text nameText;
	private Text infoText;

	// Use this for initialization
	void Awake () 
	{
		nameText = GetComponentInChildren<Text> ();
		infoText = GetComponentsInChildren<Text> () [1];
	}

	public void UpdateButtonWithoutSkill ()
	{
		associatedSkill = null;
		nameText.text = "";
		infoText.text = "";
	}

	public void UpdateButtonWithSkill (Skill skill) 
	{
		nameText.enabled = true;
		infoText.enabled = true;
		associatedSkill = skill;
		
		if(associatedSkill != null)
		{
			nameText.text = associatedSkill.GetName();
			infoText.text = associatedSkill.GetMPCost().ToString() + "MP";
		}
	}

	public Skill GetAssociatedSkill()
	{
		return associatedSkill;
	}
}
