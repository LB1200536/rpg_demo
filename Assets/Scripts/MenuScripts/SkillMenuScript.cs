﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class SkillMenuScript : MonoBehaviour 
{
	private const int SCREEN_BUTTON_COUNT = 15;
	private int maxScreens = 10;
	private int scrollCount; // keeps track of how many screens 'down' the player is in their inventory
	private Text pageCount;
	private Button[] skillButtons;
	private BaseCharacter selectedCharacter;
	private Skill skillToUse;

	// Use this for initialization
	void Start () 
	{
		skillButtons = GetComponentsInChildren<Button> ();
		scrollCount = 1;

		pageCount = GetComponentsInChildren<Text> () [1];
	}

	void UpdateButtons()
	{
		pageCount.text = scrollCount.ToString () + "/" + maxScreens.ToString();

		// magic numbers account for the scroll buttons and the close button, which should not be accounted for here
		for(int i = 2; i < skillButtons.Length - 1; i++)
		{
			int skillIndex = (i-2) + (SCREEN_BUTTON_COUNT * (scrollCount - 1));
			if(skillIndex < selectedCharacter.GetSkillList().Count)
			{
				if(!selectedCharacter.GetSkillList()[skillIndex].GetUsableInField() ||
				   selectedCharacter.GetSkillList()[skillIndex].GetMPCost() > selectedCharacter.GetCurrentMP())
				{
					skillButtons[i].GetComponentsInChildren<Text>()[0].color = Color.gray;
					skillButtons[i].GetComponentsInChildren<Text>()[1].color = Color.gray;
				}
				else
				{
					skillButtons[i].GetComponentsInChildren<Text>()[0].color = Color.white;
					skillButtons[i].GetComponentsInChildren<Text>()[1].color = Color.white;
				}
				
				skillButtons[i].SendMessage("UpdateButtonWithSkill", selectedCharacter.GetSkillList()[skillIndex]);
			}
			else
			{
				skillButtons[i].SendMessage("UpdateButtonWithoutSkill", null);
			}
		}
	}

	public void ScrollUp()
	{
		// If there are no more screens available above
		if(scrollCount != 1)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount--;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}

	}

	public void ScrollDown()
	{
		if(scrollCount != maxScreens)
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			scrollCount++;
			UpdateButtons ();
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public void SetSelectedCharacter(BaseCharacter associatedCharacter)
	{
		selectedCharacter = associatedCharacter;
		maxScreens = (1 + (selectedCharacter.GetSkillList().Count / skillButtons.Length));
		GetComponentInChildren<Text> ().text = "Skills (" + selectedCharacter.GetName () + ")";
		UpdateButtons ();
	}

	public void SkillChosen(int buttonId)
	{
		skillToUse = skillButtons [buttonId + 2].GetComponent<SkillMenuButtonScript> ().GetAssociatedSkill ();
		if(skillToUse != null && skillToUse.GetUsableInField() && skillToUse.GetMPCost() < selectedCharacter.GetCurrentMP())
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuButton");
			transform.parent.gameObject.SendMessage("StartCharacterSelect", "UseSkill");
			GetComponent<Canvas>().enabled = false;
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect("MenuError");
		}
	}

	public void SkillUsed()
	{
		selectedCharacter.SetCurrentMP (selectedCharacter.GetCurrentMP () - skillToUse.GetMPCost ());
		//skillToUse = null;
	}

	public Skill GetSkillToUse()
	{
		return skillToUse;
	}

	public BaseCharacter GetSelectedCharacter()
	{
		return selectedCharacter;
	}
}
