﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BattleSystemScript : MonoBehaviour 
{
	/*
	public List<ParticleSystem> particleSystems;

	public List<Action> actionList;
	public bool actionUnderway = false;
	public int enemySpawnCount;

	public const int CAMERA_ATTACK_DELAY = 50;
	public const int CAMERA_MAGIC_CAST_DELAY = 100;
	public const int ENEMY_SPAWN_MAX = 3;
	public const int PARTY_SIZE_MAX = 4;

	void Awake()
	{
		enemySpawnCount = Random.Range(1, ENEMY_SPAWN_MAX + 1);
	}

	// Use this for initialization
	void Start () 
	{
		if(AudioManagerScript.reference)
		{
			AudioManagerScript.reference.PlayBGM ("BattleTheme");
		}

		actionList = new List<Action>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(actionList.Count > 0)
		{
			if(!actionList[0].GetStartedState())
			{
				// Remove any actions from inactive battle participants
				if((actionList[0].GetUser().GetComponent<CharacterScript>() && actionList[0].GetUser().GetComponent<CharacterScript>().GetTargetHP() <= 0)
				   || (actionList[0].GetUser().GetComponent<EnemyScript>() && actionList[0].GetUser().GetComponent<EnemyScript>().GetTargetHP() <= 0))
				{
					actionList.RemoveAt(0);
				}
				switch(actionList[0].GetActionType())
				{
				case ActionType.Attack:
					if(actionList[0].GetUser().GetComponent<CharacterScript>() && actionList[0].GetUser().GetComponent<CharacterScript>().inPosition)
					{
						actionList[0].SetStartedState(true);
						CharacterAttack();
					}
					if(actionList[0].GetUser().GetComponent<EnemyScript>())
					{
						actionList[0].SetStartedState(true);
						EnemyAttack();
					}
					break;
				case ActionType.Skill:
					if(actionList[0].GetUser().GetComponent<CharacterScript>() && actionList[0].GetUser().GetComponent<CharacterScript>().inPosition)
					{
						actionList[0].SetStartedState(true);
						CharacterSkill();
					}
					break;
				case ActionType.Item:
					if(actionList[0].GetUser().GetComponent<CharacterScript>() && actionList[0].GetUser().GetComponent<CharacterScript>().inPosition)
					{
						actionList[0].SetStartedState(true);
						CharacterItem();
					}
					break;
				case ActionType.Run:
					actionList[0].SetStartedState(true);
					break;
				default:
					actionList[0].SetStartedState(false);
					break;
				}
			}
			else
			{
				if(actionList[0].GetStartedState())
				{
					for(int i = 0; i < actionList[0].GetTargets().Count; i++)
					{
						if(actionList [0].GetTargets()[i].tag.Equals("Untagged") && !actionList[0].GetFinishedState())
						{
							CharacterReaction();
						}
						else if(!actionList [0].GetTargets()[i].tag.Equals("Untagged") && !actionList[0].GetFinishedState())
						{
							EnemyReaction();
						}
					}
				}

				if(actionList[0].GetFinishedState() && CameraManagerScript.reference.GetDelay() <= 0)
				{
					GameControllerScript.reference.battleFlow = true;
					GameControllerScript.reference.mainLight.enabled = true;
					if(actionList[0].GetActionType() == ActionType.Attack)
					{
						Debug.Log ("Setting camera to random location");
						CameraManagerScript.reference.SetDefaultCameraLocation(0);
					}
					if(actionList[0].GetUser().GetComponent<CharacterScript>())
					{
						Debug.Log (actionList[0].GetUser().name + " no longer selected.");
						actionList[0].GetUser().GetComponent<CharacterScript>().selected = false;
					}

					Debug.Log (actionList[0].GetUser().name + "'s action complete.");
					actionList.RemoveAt(0);

					if(GameControllerScript.reference.battleWon || GameControllerScript.reference.battleLost)
					{
						actionList.Clear();
					}
				}
			}
		}
	}

	private void CharacterAttack()
	{
		CharacterScript user = actionList[0].GetUser().GetComponent<CharacterScript>();
		if(user)
		{
			EnemyScript target = (EnemyScript)actionList[0].GetTargets()[0].GetComponent<EnemyScript>(); // Can only attack one target at a time
			if(target)
			{
				user.SetAttacking(true);
				if(PlayerPrefs.GetInt("waitMode") == 0)
				{
					GameControllerScript.reference.battleFlow = false;
				}

				Vector3 cameraPosition = user.transform.localPosition;
				cameraPosition.z += 3.0f;
				cameraPosition.y += user.GetComponentInChildren<Collider>().bounds.max.y/2;
				Vector3 cameraRotation = user.transform.localEulerAngles;
				cameraRotation.y += 180.0f;
				CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
				CameraManagerScript.reference.SetDelay(CAMERA_ATTACK_DELAY);
				CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);
				CameraManagerScript.reference.SetDelayOnArrival(CAMERA_ATTACK_DELAY);

				Debug.Log("Character" + user.GetId() + "Attacked!");
			}
			else
			{
				user.SetAttacking(true);
				
				SetCameraToCharacter(actionList[0].GetTargets()[0]);
				CameraManagerScript.reference.SetDelayOnArrival(CAMERA_ATTACK_DELAY);

				Debug.Log("Character" + user.GetId() + "Attacked!");
			}
		}
	}

	private void EnemyAttack()
	{
		EnemyScript user = actionList[0].GetUser().GetComponent<EnemyScript>();
		CharacterScript target = actionList[0].GetTargets()[0].GetComponent<CharacterScript>();

		if(user != null)
		{
			if(target != null)
			{
				SetCameraToCharacter(target.gameObject);
				CameraManagerScript.reference.SetDelayOnArrival(CAMERA_ATTACK_DELAY);

				actionList[0].SetStartedState(true);

				Debug.Log("Enemy Attacked!");
			}
		}
	}

	private void CharacterSkill()
	{
		CharacterScript user = actionList[0].GetUser().GetComponent<CharacterScript>();
		if(user)
		{
			if(PlayerPrefs.GetInt("waitMode") == 0)
			{
				GameControllerScript.reference.battleFlow = false;
			}
			GameControllerScript.reference.mainLight.enabled = false;
			user.SetCasting(true);

			user.PlayMagicEffect(GetParticleSystem("PrepareMagic"));
			AudioManagerScript.reference.PlayAttackEffect("PrepareMagic");

			Vector3 cameraPosition = user.transform.localPosition;
			cameraPosition.z += 3.0f;
			cameraPosition.y += user.GetComponentInChildren<Collider>().bounds.max.y/2;
			Vector3 cameraRotation = user.transform.localEulerAngles;
			cameraRotation.y += 180.0f;
			CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
			CameraManagerScript.reference.SetDelay(user.GetParticleSystem().duration * CAMERA_ATTACK_DELAY);
			CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);


			actionList[0].SetStartedState(true);
		}
	}

	private void CharacterItem()
	{
		CharacterScript user = actionList [0].GetUser ().GetComponent<CharacterScript>();
		if(user)
		{
			if(PlayerPrefs.GetInt("waitMode") == 0)
			{
				GameControllerScript.reference.battleFlow = false;
			}
			user.SetUsingItem(true);

			Vector3 cameraPosition = user.transform.localPosition;
			cameraPosition.z += 3.0f;
			cameraPosition.y += user.GetComponentInChildren<Collider>().bounds.max.y/2;
			Vector3 cameraRotation = user.transform.localEulerAngles;
			cameraRotation.y += 180.0f;
			CameraManagerScript.reference.SetCurrentLocation(cameraPosition, cameraRotation);
			CameraManagerScript.reference.SetDelay(CAMERA_ATTACK_DELAY);
			CameraManagerScript.reference.SetDefaultTargetLocation(0, 0.02f);

			actionList[0].SetStartedState(true);
		}
	}

	private void EnemyReaction()
	{ 
		CharacterScript user = actionList [0].GetUser ().GetComponent<CharacterScript> ();

		switch (actionList [0].GetActionType ()) 
		{
		case ActionType.Skill:
			if (!user.GetParticleSystem ()) 
			{
				//user.SetCasting (false);
				if (CameraManagerScript.reference.finishedMovement) 
				{
					for (int i = 0; i < actionList[0].GetTargets().Count; i++) 
					{
						EnemyScript target = actionList [0].GetTargets () [i].GetComponent<EnemyScript> (); 

						if (user.GetCasting() && !target.GetParticleSystem()) 
						{
							target.PlayMagicEffect (GetParticleSystem (actionList [0].GetSkill ().GetName ()));
							AudioManagerScript.reference.PlayAttackEffect (actionList [0].GetSkill ().GetName ());

							if (i == actionList [0].GetTargets ().Count - 1) 
							{
								user.SetCasting(false);
							}
						}
						
						if (!user.GetCasting () && !target.GetParticleSystem()) 
						{
							int calculatedDamage = CalculateDamage (user.GetMagicAttack (), target.GetMagicDefence (), target.GetWeakElement ());
							target.SendMessage ("SetTargetHP", target.GetCurrentHP () + calculatedDamage);
							user.SendMessage ("SetTargetMP", user.GetCurrentMP () - actionList [0].GetSkill ().GetMPCost ());

							actionList [0].GetUser ().SendMessage ("ActionComplete");
							target.SendMessage ("StartDamageDisplay", true);
							if(calculatedDamage < 0)
							{
								target.SetDamaged (true);
							}

							actionList [0].SetFinishedState (true);
						}
					}

				}
			}
			break;
		case ActionType.Attack:
			actionList [0].GetUser ().SendMessage ("ActionComplete");
			if (CameraManagerScript.reference.finishedMovement) 
			{
				CameraManagerScript.reference.shakeScript.BeginShake();
				EnemyScript target = actionList [0].GetTargets () [0].GetComponent<EnemyScript> ();
				int calculatedDamage = CalculateDamage (user.GetAttack (), target.GetDefence (), target.GetWeakElement ());
				target.SendMessage ("SetTargetHP", target.GetCurrentHP () + calculatedDamage);

				target.SendMessage ("StartDamageDisplay", true);
				target.SetDamaged (true);
				
				AudioManagerScript.reference.PlayAttackEffect (user.GetCurrentWeapon ().GetWeaponType ().ToString () + "Attack");
				target.PlayMagicEffect (GetParticleSystem (user.GetCurrentWeapon ().GetWeaponType ().ToString () + "Attack"));
				CameraManagerScript.reference.shakeScript.BeginShake();
				CameraManagerScript.reference.SetDelay (CAMERA_ATTACK_DELAY);

				actionList [0].SetFinishedState (true);
			}
			break;
		case ActionType.Item:
			if (CameraManagerScript.reference.finishedMovement) 
			{
				EnemyScript target = actionList [0].GetTargets () [0].GetComponent<EnemyScript> ();
				Item item = actionList [0].GetItem ();

				if (user.GetUsingItem ()) 
				{
					actionList[0].GetUser().SendMessage("ActionComplete");
					if(item.GetItemType() == ItemType.HP_Healing)
					{
						target.PlayMagicEffect(GetParticleSystem("Heal"));
						AudioManagerScript.reference.PlayAttackEffect("Heal");
					}
					else if(item.GetItemType() == ItemType.MP_Healing)
					{
						target.PlayMagicEffect(GetParticleSystem("MPHeal"));
						AudioManagerScript.reference.PlayAttackEffect("MPHeal");
					}

					user.SetUsingItem (false);
				}

				if (!user.GetUsingItem () && !!target.GetParticleSystem()) 
				{
					if(item.GetItemType() == ItemType.HP_Healing)
					{
						target.SendMessage("SetTargetHP", target.GetCurrentHP() + item.GetEffectAmount());
						target.SendMessage ("StartDamageDisplay", true);
					}
					else if(item.GetItemType() == ItemType.MP_Healing)
					{
						target.SendMessage("SetTargetMP", target.GetCurrentMP() + item.GetEffectAmount());
						target.SendMessage ("StartDamageDisplay", false);
					}

					actionList [0].GetUser ().SendMessage ("ActionComplete");
					actionList [0].SetFinishedState (true);

					PartyDataHolderScript.reference.inventory.DeductItemFromTotal(item, 1);
				}
			}
			break;
		}
	}

	private void CharacterReaction()
	{
		bool enemyUser = true;
		if(actionList[0].GetUser().GetComponent<CharacterScript>())
		{
			enemyUser = false;
		}
		
		GameObject user = actionList [0].GetUser ();
		switch(actionList[0].GetActionType())
		{
		case ActionType.Skill:
			if((enemyUser && !user.GetComponent<EnemyScript>().GetComponentInChildren<ParticleSystem>())
			   || (!enemyUser && !user.GetComponent<CharacterScript>().GetParticleSystem()))
			{
				//user.SendMessage("SetCasting", false);
				if(CameraManagerScript.reference.finishedMovement)
				{
					for(int i = 0; i < actionList[0].GetTargets().Count; i++)
					{
						CharacterScript target = actionList[0].GetTargets()[i].GetComponent<CharacterScript> (); 

						if(user.GetComponent<CharacterScript>().GetCasting() && !target.GetParticleSystem())
						{
							target.PlayMagicEffect(GetParticleSystem(actionList[0].GetSkill().GetName()));
							AudioManagerScript.reference.PlayAttackEffect(actionList[0].GetSkill().GetName());

							if(i == actionList[0].GetTargets().Count - 1)
							{
								user.SendMessage("SetCasting", false);
							}
						}
						else if(!user.GetComponent<CharacterScript>().GetCasting() && !target.GetParticleSystem())
						{
							if(enemyUser)
							{
								int calculatedDamage = CalculateDamage(user.GetComponent<EnemyScript>().GetMagicAttack(), target.GetMagicDefence(), Element.None);
								target.SendMessage("SetTargetHP", target.GetCurrentHP() + calculatedDamage);
						
								actionList[0].GetUser().SendMessage("ActionComplete");
								target.SendMessage("StartDamageDisplay", true);
								target.SetDamaged(true);
							}
							else
							{
								int calculatedDamage = CalculateDamage(user.GetComponent<CharacterScript>().GetMagicAttack(), target.GetMagicDefence(), Element.None);
								target.SendMessage("SetTargetHP", target.GetCurrentHP() + calculatedDamage);
								user.SendMessage("SetTargetMP", user.GetComponent<CharacterScript>().GetCurrentMP() - actionList[0].GetSkill().GetMPCost());
							
								actionList[0].GetUser().SendMessage("ActionComplete");
								target.SendMessage("StartDamageDisplay", true);
								if(calculatedDamage < 0)
								{
									target.SetDamaged (true);
								}
							}
							
							actionList[0].SetFinishedState(true);
						}
					}
				}
			}
			break;
		case ActionType.Attack:
			if(CameraManagerScript.reference.finishedMovement)
			{
				CharacterScript target = actionList[0].GetTargets()[0].GetComponent<CharacterScript> (); 
				if(enemyUser)
				{
					int calculatedDamage = CalculateDamage(user.GetComponent<EnemyScript>().GetAttack(), target.GetDefence(), Element.None);
					target.SendMessage("SetTargetHP", target.GetCurrentHP() + calculatedDamage);
					if(calculatedDamage <= 0)
					{
						actionList[0].GetUser().SendMessage("ActionComplete");
						target.SendMessage("StartDamageDisplay", true);
						CameraManagerScript.reference.shakeScript.BeginShake();
						target.SetDamaged(true);
					}
					else
					{
						actionList[0].GetUser().SendMessage("ActionComplete");
						target.SendMessage("StartDamageDisplay", false);
					}
					AudioManagerScript.reference.PlayAttackEffect("BiteAttack");
					target.PlayMagicEffect(GetParticleSystem("BiteAttack"));
				}
				else
				{
					int calculatedDamage = CalculateDamage(user.GetComponent<CharacterScript>().GetAttack(), target.GetDefence(), Element.None);
					target.SendMessage("SetTargetHP", target.GetCurrentHP() + calculatedDamage);

					target.SendMessage("StartDamageDisplay", true);
					CameraManagerScript.reference.shakeScript.BeginShake();
					target.SetDamaged(true);

					AudioManagerScript.reference.PlayAttackEffect(user.GetComponent<CharacterScript>().GetCurrentWeapon().GetWeaponType().ToString() + "Attack");
					target.PlayMagicEffect(GetParticleSystem(user.GetComponent<CharacterScript>().GetCurrentWeapon().GetWeaponType().ToString() + "Attack"));
				}

				CameraManagerScript.reference.SetDelay(CAMERA_ATTACK_DELAY);
				actionList[0].GetUser().SendMessage("ActionComplete");
				
				actionList[0].SetFinishedState(true);
			}
			break;
		case ActionType.Item:
			if(CameraManagerScript.reference.finishedMovement)
			{
				CharacterScript target = actionList[0].GetTargets()[0].GetComponent<CharacterScript> ();
				Item item = actionList[0].GetItem();

				if(user.GetComponent<CharacterScript>().GetUsingItem())
				{
					actionList[0].GetUser().SendMessage("ActionComplete");
					if(item.GetItemType() == ItemType.HP_Healing)
					{
						target.PlayMagicEffect(GetParticleSystem("Heal"));
						AudioManagerScript.reference.PlayAttackEffect("Heal");
					}
					else if(item.GetItemType() == ItemType.MP_Healing)
					{
						target.PlayMagicEffect(GetParticleSystem("MPHeal"));
						AudioManagerScript.reference.PlayAttackEffect("MPHeal");
					}
				}
				
				if(!user.GetComponent<CharacterScript>().GetUsingItem() && !target.GetParticleSystem())
				{
					if(item.GetItemType() == ItemType.HP_Healing)
					{
						target.SendMessage("SetTargetHP", target.GetCurrentHP() + item.GetEffectAmount());
						target.SendMessage("StartDamageDisplay", true);
					}
					else if(item.GetItemType() == ItemType.MP_Healing)
					{
						target.SendMessage("SetTargetMP", target.GetCurrentMP() + item.GetEffectAmount());
						target.SendMessage("StartDamageDisplay", false);
					}
										
					actionList[0].SetFinishedState(true);
					PartyDataHolderScript.reference.inventory.DeductItemFromTotal(item, 1);
					//item.SetQuantity(item.GetQuantity() - 1);
				}
			}
			break;
		}
	}

	public void SetCameraToCharacter(GameObject character)
	{
		Vector3 cameraPosition = character.transform.localPosition;
		cameraPosition.z += 3.0f;
		cameraPosition.y += character.GetComponentInChildren<Collider> ().bounds.max.y/2;
		Vector3 cameraRotation = character.transform.localEulerAngles;
		cameraRotation.y += 180.0f;
		CameraManagerScript.reference.SetDefaultCameraLocation (1);
		CameraManagerScript.reference.SetTargetLocation(cameraPosition, cameraRotation, 0.02f);
	}

	private void SetCameraToCharacterReverse(GameObject character)
	{
		Vector3 cameraPosition = character.transform.localPosition;
		cameraPosition.z += 3.0f;
		cameraPosition.y += character.GetComponentInChildren<Collider> ().bounds.max.y/2;
		Vector3 cameraRotation = character.transform.localEulerAngles;
		cameraRotation.y += 180.0f;
	
		CameraManagerScript.reference.SetCurrentLocation (cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDefaultTargetLocation (1, 0.02f);
	}

	public void SetCameraToEnemy(GameObject enemy)
	{
		Vector3 cameraPosition = enemy.transform.localPosition;
		cameraPosition.z -= enemy.GetComponentInChildren<Collider>().bounds.max.z * 0.95f;
		cameraPosition.y -= enemy.GetComponentInChildren<Collider>().bounds.min.y * 0.65f;
		Vector3 cameraRotation = enemy.gameObject.transform.localEulerAngles;

		CameraManagerScript.reference.SetCurrentLocation (cameraPosition, cameraRotation);
		CameraManagerScript.reference.SetDefaultTargetLocation (3, 0.02f);
		CameraManagerScript.reference.SetDelayOnArrival(CAMERA_ATTACK_DELAY);
	}

	private int CalculateDamage(float attack, float defence, Element weakness)
	{
		int calculatedDamage = 1;

		// 10% leeway for both attack and defence;
		if(actionList[0].GetActionType() == ActionType.Attack)
		{
			float calculatedDefence = Random.Range (defence - ((defence / 100) * 10), defence + ((defence / 100) * 10));
			float calculatedAttack = Random.Range (attack - ((attack / 100) * 10), attack + ((attack / 100) * 10));
			calculatedDamage = (int)calculatedDefence - (int)calculatedAttack;
			
			if(calculatedDamage > -1)
			{
				calculatedDamage = -1;
			}
		}
		else if(actionList[0].GetActionType() == ActionType.Skill && actionList[0].GetSkill().GetElement() != Element.Heal)
		{
			float calculatedDefence = Random.Range (defence - ((defence / 100) * 10), defence + ((defence / 100) * 10));
			float calculatedAttack = Random.Range (attack - ((attack / 100) * 10), attack + ((attack / 100) * 10));
			calculatedDamage = ((actionList[0].GetSkill().GetBaseDamage() * -1) /2 ) + ((int)calculatedDefence - (int)calculatedAttack);

			if(actionList[0].GetSkill().GetElement() == weakness)
			{
				calculatedDamage += calculatedDamage / 2; // Add 50% damage to successfully matched weakness
			}
			
			if(calculatedDamage > -1)
			{
				calculatedDamage = -1;
			}
		}
		else if(actionList[0].GetActionType() == ActionType.Skill && actionList[0].GetSkill().GetElement() == Element.Heal)
		{
			float calculatedAttack = Random.Range (attack - ((attack / 100) * 10), attack + ((attack / 100) * 10));
			calculatedDamage = actionList[0].GetSkill().GetBaseDamage()/2 + (int)calculatedAttack;
		}

		// Lower the amount of damage dealt based on how many targets were selected
		calculatedDamage = calculatedDamage / actionList[0].GetTargets().Count;

		return calculatedDamage;
	}

	public void FinishAction()
	{
		actionList[0].SetFinishedState(true);
	}

	public void AddAction(Action action)
	{
		Debug.Log ("Action Added: " + action.GetActionType());
		actionList.Add (action);
	}

	public List<Action> GetActionList()
	{
		return actionList;
	}

	public Action GetCurrentAction()
	{
		return actionList [0];
	}

	public ParticleSystem GetParticleSystem(string name)
	{
		foreach(ParticleSystem system in particleSystems)
		{
			if(system.name.Equals(name))
			{
				Debug.Log ("Found system called " + name);
				return system;
			}
		}

		return null;
	}

	public List<ParticleSystem> GetParticleSystemList()
	{
		return particleSystems;
	}
	*/
}
	