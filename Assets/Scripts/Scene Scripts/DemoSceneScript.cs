﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DemoSceneScript : MonoBehaviour 
{
	void OnApplicationQuit() 
	{
		// Allows threads to end even if the application is closed prematurely
		DatabaseHandler.CloseAllDatabases();
	}

	// Use this for initialization
	void Start () 
	{
		PartyDataHolderScript.reference.areaId = 1;
		PartyDataHolderScript.reference.ResetLastTransform ();
		SavedDataManager.Save (PartyDataHolderScript.reference.GetCurrentSaveSlot ());
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "", this.gameObject);
	}

	public void StartReturnToTitle()
	{
		FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, Color.black,
			"ReturnToTitleScene", this.gameObject);
	}

	public void ReturnToTitleScene()
	{
		Destroy (PartyDataHolderScript.reference.gameObject);
		SceneManager.LoadScene ("main_menu");
	}
}
