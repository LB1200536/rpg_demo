﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoseMenuScript : MonoBehaviour 
{
	void OnApplicationQuit() 
	{
		// Allows threads to end even if the application is closed prematurely
		DatabaseHandler.CloseAllDatabases();
	}

	public void Start()
	{
		AudioManagerScript.reference.PlayBGM ("LoseTheme", false);
	}
	
	public void StartGame()
	{
		SceneManager.LoadScene ("main_menu");
	}
}
