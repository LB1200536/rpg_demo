﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenuScript : MonoBehaviour 
{
	private Canvas mainCanvas;
	public OptionsMenuScript optionsMenu;
	public List<SaveSlotScript> saveSlots;
	private DecisionPromptScript decisionPrompt;
	public const float CAMERA_ORBIT_SPEED = 0.0005f;

	void OnApplicationQuit() 
	{
		// Allows threads to end even if the application is closed prematurely
		DatabaseHandler.CloseAllDatabases();
	}

	void Awake()
	{
		mainCanvas = GetComponentInChildren<Canvas> ();
	}

	void Start()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "DisplayMainMenu", this.gameObject);
		AudioManagerScript.reference.PlayBGM ("TitleTheme", false);
		AudioManagerScript.reference.SetBGMSourceLoopingState (true);
		decisionPrompt = FindObjectOfType<DecisionPromptScript> ();
		decisionPrompt.Enable (false);

		#if DEVELOPMENT_BUILD
		Debug.unityLogger.logEnabled=true;
		#else
		//Debug.unityLogger.logEnabled=false;
		#endif
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			//Camera.main.gameObject.SendMessage ("BeginShake");
		}
			
		if (Input.GetMouseButtonDown(0))
		{
			GetComponent<Animator> ().SetBool("Tapped", true);
		}
	}

	void DisplayMainMenu()
	{
		GetComponent<Animator> ().Play ("Start");
	}

	public void StartWhiteFlash()
	{
		FadeoutScript.reference.Flash(Color.white, FadeoutScript.PAUSE_FADEOUT, FadeoutScript.SCENE_TRANSITION_FADEOUT);
		GetComponent<Animator> ().speed = 0;
		Invoke ("ResumeOpeningAnimation", 0.2f);
	}

	void EndWhiteFlash()
	{
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "ResumeOpeningAnimation", this.gameObject);
	}

	void ResumeOpeningAnimation()
	{
		GetComponent<Animator> ().speed = 1;
		//AudioManagerScript.reference.PlayBGM ("TitleTheme", false);
		//AudioManagerScript.reference.SetBGMSourceLoopingState (true);
	}

	public void StartCameraRotation()
	{
		Camera.main.gameObject.SendMessage ("SetRotationSpeed", CAMERA_ORBIT_SPEED);
	}

	public void OpenOptionsMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");

		optionsMenu.OpenMenu ();

		Camera.main.gameObject.SendMessage ("SetRotationSpeed", 0.0f);
	}

	public void OpenSaveSlots()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");

		for(int i = 0; i < saveSlots.Count; i++)
		{
			saveSlots[i].OpenSlot();
		}

		mainCanvas.enabled = false;
	}

	public void CloseSaveSlots()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuCancel");

		for(int i = 0; i < saveSlots.Count; i++)
		{
			saveSlots[i].CloseSlot();
		}

		mainCanvas.enabled = true;
	}

	public void QuitGame()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");
		Application.Quit ();
	}

	private void DatabaseLoadFailure(string databaseName)
	{
		decisionPrompt.StartDecision ("Database (" + databaseName + ") failed to load. Closing application.", this.QuitGame, null, mainCanvas.GetComponent<CanvasGroup>(), "Okay");
	}
}
