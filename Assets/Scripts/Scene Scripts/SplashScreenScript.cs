﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreenScript : MonoBehaviour 
{
	Canvas splashScreen;
	Image disclaimerDisplay;
	Image authorDisplay;

	const float SPLASH_SCREEN_DURATION = 3.0f;

	// Use this for initialization
	void Start () 
	{
		splashScreen = GetComponent<Canvas> ();
		disclaimerDisplay = splashScreen.GetComponentInChildren<Image> ();
		authorDisplay = splashScreen.GetComponentsInChildren<Image> ()[1];
		authorDisplay.gameObject.SetActive(false);

		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "StartDisclaimerDisplay", this.gameObject);
	}

	void Update()
	{

	}

	void StartDisclaimerDisplay()
	{
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "", this.gameObject);
		disclaimerDisplay.gameObject.SetActive(true);
		authorDisplay.gameObject.SetActive(false);

		Invoke ("EndDisclaimerDisplay", SPLASH_SCREEN_DURATION);
	}

	void EndDisclaimerDisplay()
	{
		FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, 
		                                         Color.black, "StartAuthorDisplay", this.gameObject);
	}

	void StartAuthorDisplay()
	{
		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "", this.gameObject);

		disclaimerDisplay.gameObject.SetActive(false);
		authorDisplay.gameObject.SetActive(true);

		Invoke ("EndSplashScreen", SPLASH_SCREEN_DURATION);		
	}

	public void EndSplashScreen()
	{
		FadeoutScript.reference.StartTransition (ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, 
		                                         Color.black, "LoadTitleScene", this.gameObject);
	}

	void LoadTitleScene()
	{
		SceneManager.LoadScene("main_menu");
	}
	
}
