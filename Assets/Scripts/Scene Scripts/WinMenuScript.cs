﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class WinMenuScript : MonoBehaviour 
{
	int goldEarned;
	int experienceEarned;
	List<Item> itemsEarned;
	bool characterLeveledUp = false;

	Text itemText;
	Text goldText;
	BattleToWinningsDataHolder earningsData;
	public List<CharacterRewardDisplayScript> characterRewardDisplays;
	public List<GameObject> characterDisplays;

	public GameObject itemDisplay;

	void OnApplicationQuit() 
	{
		// Allows threads to end even if the application is closed prematurely
		DatabaseHandler.CloseAllDatabases();
	}

	public void Start()
	{
		if(AudioManagerScript.reference)
		{
			AudioManagerScript.reference.SetBGMSourceLoopingState(true);
			AudioManagerScript.reference.PlayBGM ("WinTheme", false);
		}

		earningsData = GameObject.Find ("BattleEarnings").GetComponent<BattleToWinningsDataHolder>();

		goldEarned = earningsData.GetGoldEarned ();
		experienceEarned = earningsData.GetExperienceEarned ();
		itemsEarned = earningsData.GetItemsEarned ();

		itemText = itemDisplay.GetComponentsInChildren<Text> () [1];
		goldText = itemDisplay.GetComponentsInChildren<Text> () [2];

		//characterRewardDisplays = GetComponentsInChildren<CharacterRewardDisplayScript> ().ToList();

		for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			// Text //
			Text[] textComponents = characterDisplays[i].GetComponentsInChildren<Text>();
			textComponents[0].text = PartyDataHolderScript.reference.characters[i].GetName();
			textComponents[1].text = "EXP: " + PartyDataHolderScript.reference.characters[i].GetCurrentExperiencePoints().ToString();
			textComponents[2].text = "LVL: " + PartyDataHolderScript.reference.characters[i].GetCurrentLevel().ToString();

			// Slider //
			Slider experienceSlider = characterDisplays[i].GetComponentInChildren<Slider>();
			experienceSlider.maxValue = PartyDataHolderScript.reference.characters[i].GetExperiencePointsForNextLevel();
			experienceSlider.value = PartyDataHolderScript.reference.characters[i].GetCurrentExperiencePoints();
		}

		FadeoutScript.reference.EndTransition (FadeoutScript.SCENE_TRANSITION_FADEOUT, "PostFadein", this.gameObject);
	}

	void PostFadein()
	{
		UpdateItems ();

		for (int i = 0; i < characterRewardDisplays.Count; i++)
		{
			characterRewardDisplays [i].InitialiseUpdate(PartyDataHolderScript.reference.characters[i], experienceEarned);
		}

		if(goldEarned > 0 || experienceEarned > 0)
		{
			InvokeRepeating ("UpdateWinnings", 0, 0.0167f);
			AudioManagerScript.reference.PlayLoopingMenuEffect ("SinglePointGain");
		}
		else
		{
			AudioManagerScript.reference.PlayMenuEffect ("PointGainComplete");
		}
	}

	void UpdateWinnings()
	{
		if(goldEarned > 0)
		{
			UpdateGold();
		}

		// FIXME: Character level up should pause the other character's exp gain//
		for (int i = 0; i < characterRewardDisplays.Count; i++)
		{
			characterRewardDisplays [i].UpdateExperience (ref characterLeveledUp);
		}
		if (!characterLeveledUp)
		{
			experienceEarned--;
		}
		AudioManagerScript.reference.SetMenuSourceOn (!characterLeveledUp);

		/*
		if(experienceEarned > 0)
		{
			UpdateExperience();
		}
		*/
		if(goldEarned == 0 && experienceEarned == 0)
		{
			AudioManagerScript.reference.PlayMenuEffect ("PointGainComplete");
			AudioManagerScript.reference.StopAllSFXLooping();
			CancelInvoke();
		}
	}

	private void UpdateItems()
	{
		for(int i = 0; i < itemsEarned.Count; i++)
		{
			itemText.text += itemsEarned[i].GetName() + "\n";
			PartyDataHolderScript.reference.inventory.AddItemToInventory(itemsEarned[i]);
		}
	}

	private void UpdateGold()
	{
		goldEarned--;
		PartyDataHolderScript.reference.goldCount++;

		goldText.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString () + " (" + goldEarned.ToString () + ")";
	}

	private void UpdateExperience()
	{
		experienceEarned--;

		for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			PartyDataHolderScript.reference.characters[i].AddExperiencePoints(1);
			// Text //
			characterDisplays[i].GetComponentsInChildren<Text>()[2].text = "LVL: " + PartyDataHolderScript.reference.characters[i].GetCurrentLevel().ToString();
			characterDisplays[i].GetComponentsInChildren<Text>()[1].text = "EXP: " + PartyDataHolderScript.reference.characters[i].GetTotalExperiencePoints().ToString() + " (" + experienceEarned.ToString() + ")";
			
			// Slider //
			characterDisplays[i].GetComponentInChildren<Slider>().value = PartyDataHolderScript.reference.characters[i].GetCurrentExperiencePoints();
			characterDisplays[i].GetComponentInChildren<Slider>().maxValue = PartyDataHolderScript.reference.characters[i].GetExperiencePointsForNextLevel();
		}

		if(goldEarned == 0 && experienceEarned == 0)
		{
			AudioManagerScript.reference.PlayMenuEffect ("PointGainComplete");
			AudioManagerScript.reference.StopAllSFXLooping();
		}
	}

	private void InstantlyApplyRewards()
	{
		AudioManagerScript.reference.PlayMenuEffect ("PointGainComplete");
		AudioManagerScript.reference.StopAllSFXLooping();

		// Gold // 
		PartyDataHolderScript.reference.goldCount += goldEarned;
		goldEarned = 0;
		goldText.text = "Gold: " + PartyDataHolderScript.reference.goldCount.ToString() + " (" + goldEarned.ToString() + ")";

		// Experience //
		for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
		{
			PartyDataHolderScript.reference.characters[i].AddExperiencePoints(experienceEarned);
			characterDisplays[i].GetComponentsInChildren<Text>()[2].text = "LVL: " + PartyDataHolderScript.reference.characters[i].GetCurrentLevel().ToString();
			characterDisplays[i].GetComponentsInChildren<Text>()[1].text = "EXP: " + PartyDataHolderScript.reference.characters[i].GetTotalExperiencePoints().ToString() + " (0)";
			characterDisplays[i].GetComponentInChildren<Slider>().value = PartyDataHolderScript.reference.characters[i].GetCurrentExperiencePoints();
			characterDisplays[i].GetComponentInChildren<Slider>().maxValue = PartyDataHolderScript.reference.characters[i].GetExperiencePointsForNextLevel();
		}
		experienceEarned = 0;

		CancelInvoke();
	}
	
	public void ReturnToMenu()
	{
		AudioManagerScript.reference.PlayMenuEffect ("MenuConfirm");

		if(goldEarned == 0 && experienceEarned == 0)
		{
			if (!earningsData.GetDemoCompleted ())
			{
				FadeoutScript.reference.StartTransition(ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, 
					Color.black, "LoadFieldScene", this.gameObject);
			}
			else
			{
				FadeoutScript.reference.StartTransition(ScreenTransitionType.Fadeout, FadeoutScript.SCENE_TRANSITION_FADEOUT, 
					Color.black, "EndDemo", this.gameObject);
			}
		}
		else
		{
			InstantlyApplyRewards();
		}
	}

	void LoadFieldScene()
	{
		SceneManager.LoadScene ("field_scene");
	}

	void EndDemo()
	{
		BattleToFieldDataHolder stateData = FindObjectOfType<BattleToFieldDataHolder> ();
		Destroy (earningsData);
		Destroy (stateData.gameObject);
		SceneManager.LoadScene ("demo_end_scene");
	}
}
