﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetBundles;

public class BattleAssetBundleLoader : MonoBehaviour 
{
	public string particleBundleName;
	public string soundEffectBundleName;

	public static BattleAssetBundleLoader reference;

	void Awake()
	{
		if(reference == null)
		{
			reference = this;
			DontDestroyOnLoad (this.gameObject);
			Initialize (particleBundleName, soundEffectBundleName);
		}
		else if(reference != this)
		{
			Destroy(gameObject);
		}
	}

	// Initialize the downloading url and AssetBundleManifest object.
	public void Initialize(string particleBundleName, string soundEffectBundleName)
	{
		this.particleBundleName = particleBundleName;
		this.soundEffectBundleName = soundEffectBundleName;
		// With this code, when in-editor or using a development builds: Always use the AssetBundle Server
		// (This is very dependent on the production workflow of the project. 
		// 	Another approach would be to make this configurable in the standalone player.)
		#if DEVELOPMENT_BUILD || UNITY_EDITOR
		AssetBundleManager.SetDevelopmentAssetBundleServer ();
		#elif ANDROID_BUILD
		AssetBundleManager.SetSourceAssetBundleURL(Application.streamingAssetsPath + "/");
		#else
		// Use the following code if AssetBundles are embedded in the project for example via StreamingAssets folder etc:
		AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/");
		// Or customize the URL based on your deployment or configuration
		//AssetBundleManager.SetSourceAssetBundleURL("http://www.MyWebsite/MyAssetBundles");
		#endif

		// Initialize AssetBundleManifest which loads the AssetBundleManifest object.
		var request = AssetBundleManager.Initialize();
		if (request != null)
		{
			StartCoroutine (request);
		}
		else
		{
			Debug.LogError ("Asset Bundle manifest could not initialise the Asset Bundle manager.");
		}
	}
	
	public ParticleSystem LoadParticlesAsync (string assetName)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;
	
		// Load asset from assetBundle.
		AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(particleBundleName, assetName, typeof(ParticleSystem) );
		if (request != null)
			StartCoroutine(request);
		
		// Get the asset.
		ParticleSystem prefab = request.GetAsset<GameObject> ().GetComponent<ParticleSystem>();
		
		if (prefab != null)
			return prefab;
		
		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log(assetName + (prefab == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime.ToString() + " seconds" );
		return null;
	}

	public AudioClip LoadSoundEffectAsync (string assetName)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;
		
		// Load asset from assetBundle.
		AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(soundEffectBundleName, assetName, typeof(AudioClip) );
		if (request != null)
			StartCoroutine(request);
		
		// Get the asset.
		AudioClip asset = request.GetAsset<AudioClip> ();
		
		if (asset != null)
			return asset;
		
		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log(assetName + (asset == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime.ToString() + " seconds" );
		return null;
	}

	public void Unload()
	{
		// Unload assetBundle.
		AssetBundleManager.UnloadAssetBundle (particleBundleName);
		AssetBundleManager.UnloadAssetBundle (soundEffectBundleName);
		Destroy (this.gameObject);
	}
}
