﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BattleToFieldDataHolder : SelfCleaningDataTransfer
{
	bool bossDefeated;
	int[] enemyIds;

	new void Awake()
	{
		base.Awake();
		bossDefeated = false;
	}
	// Use this for initialization
	new void Start () 
	{
		base.Start ();
	}
	
	// Update is called once per frame
	new void Update () 
	{
		base.Update ();
	}

	override protected void CheckForClean()
	{
		if (enemyIds == null) 
		{
			readyToClean = true;
		}
	}

	public int[] GetEnemyIds()
	{
		int[] tempIds = enemyIds;
		enemyIds = null;
		return tempIds;
	}

	public bool GetBossDefeated()
	{
		return bossDefeated;
	}

	public void SetBossDefeated(bool bossDefeated)
	{
		this.bossDefeated = bossDefeated;
	}

	public void SetEnemyIds(int[] enemyIds)
	{
		this.enemyIds = enemyIds;
	}
}
