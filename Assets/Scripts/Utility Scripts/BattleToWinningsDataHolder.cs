﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleToWinningsDataHolder : SelfCleaningDataTransfer 
{
	int goldEarned;
	int experienceEarned;
	bool demoCompleted;
	List<Item> itemsEarned;

	new void Awake()
	{
		base.Awake();
		goldEarned = 0;
		experienceEarned = 0;
		demoCompleted = false;
		itemsEarned = new List<Item> ();
	}
	// Use this for initialization
	new void Start () 
	{
		base.Start ();
	}
	
	// Update is called once per frame
	new void Update () 
	{
		base.Update ();
	}

	override protected void CheckForClean()
	{
		if(itemsEarned == null && goldEarned == 0 && experienceEarned == 0)
		{
			readyToClean = true;
		}
	}

	public int GetGoldEarned()
	{
		int tempInt = goldEarned;
		goldEarned = 0;
		return tempInt;
	}

	public int GetExperienceEarned()
	{
		int tempInt = experienceEarned;
		experienceEarned = 0;
		return tempInt;
	}

	public List<Item> GetItemsEarned()
	{
		List<Item> tempList = itemsEarned;
		itemsEarned = null;
		return tempList;
	}

	public bool GetDemoCompleted()
	{
		return demoCompleted;
	}

	public void AddGoldEarned(int goldEarned)
	{
		this.goldEarned += goldEarned;
	}

	public void AddExperienceEarned(int experienceEarned)
	{
		this.experienceEarned += experienceEarned;
	}

	public void AddItemToEarnings(Item itemEarned)
	{
		itemsEarned.Add (itemEarned);
	}

	public void SetDemoCompleted(bool completed)
	{
		demoCompleted = completed;
	}
}
