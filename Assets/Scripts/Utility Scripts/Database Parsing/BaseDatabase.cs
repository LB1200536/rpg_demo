﻿using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite; 
using System.Data;
using System.IO;

public class BaseDatabase
{
	protected string connection;
	protected IDbConnection dbconn;
	protected IDbCommand dbcmd;
	protected IDataReader reader;

	protected void SetupDatabase(string filename)
	{
		connection = "URI=file:";
		#if UNITY_EDITOR_WIN
		connection += Application.streamingAssetsPath + "/" + filename; //Path to database.
		#elif PLATFORM_ANDROID
		connection += Application.persistentDataPath + "/" + filename; //Path to database.
		RebuildDatabaseLocally(filename);
		#endif

		//TestDatabaseConnection (callingObject);
	}

	protected IDataReader BasicQuery(string query) // run a basic Sqlite query
	{ 
		dbcmd = dbconn.CreateCommand(); // create empty command
		dbcmd.CommandText = query; // fill the command
		reader = dbcmd.ExecuteReader(); // execute command which returns a reader
		return reader; // return the reader
	}

	protected void RebuildDatabaseLocally(string filename) // Rebuilds the database file in an accessible format for Android builds
	{
		// check if file exists in Application.persistentDataPath
		string filepath = Application.persistentDataPath + "/" + filename;

		if(!File.Exists(filepath))
		{
			// open StreamingAssets directory and load the db ->

			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + filename);  // this is the path to your StreamingAssets in android

			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check

			// then save to Application.persistentDataPath
			File.WriteAllBytes(filepath, loadDB.bytes);
		}
	}

	protected void CloseReader()
	{
		if (reader != null)
		{
			reader.Close ();
			reader = null;
		}
	}

	public void OpenDatabase()
	{
		Debug.Log("Establishing connection to: " + connection);
		dbconn = new SqliteConnection(connection);
		dbconn.Open();
	}

	public void CloseDatabase()
	{
		if (dbconn.State == ConnectionState.Open)
		{
			if (dbcmd != null)
			{
				dbcmd.Dispose();
				dbcmd = null;
			}

			if (dbconn != null)
			{
				dbconn.Close();
				dbconn = null;
			}
		}
	}

	public bool IsOpen()
	{
		return dbconn != null;
	}
}


