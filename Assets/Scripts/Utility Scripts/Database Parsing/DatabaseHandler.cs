﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Mono.Data.Sqlite; 
using System.Data;
using System.IO;
using System;

public static class DatabaseHandler
{
	private static ItemDatabase itemDatabase;
	private static EnemyDatabase enemyDatabase;
	private static SkillDatabase skillDatabase;
	private static DialogueDatabase dialogueDatabase;

	public static void SetupEnemyDatabase(string filename)
	{
		if (enemyDatabase == null)
		{
			enemyDatabase = new EnemyDatabase (filename);
		}
	}
		
	public static void SetupItemDatabase(string filename)
	{
		if (itemDatabase == null)
		{
			itemDatabase = new ItemDatabase (filename);
		}
	}

	public static void SetupSkillDatabase(string filename)
	{
		if (skillDatabase == null)
		{
			skillDatabase = new SkillDatabase (filename);
		}
	}

	public static void SetupDialogueDatabase(string filename)
	{
		if (dialogueDatabase == null)
		{
			dialogueDatabase = new DialogueDatabase (filename);
		}
	}

	public static void OpenItemDatabase()
	{
		if (!itemDatabase.IsOpen())
		{
			itemDatabase.OpenDatabase ();
		}
	}

	public static void OpenEnemyDatabase()
	{
		if (!enemyDatabase.IsOpen ())
		{
			enemyDatabase.OpenDatabase ();
		}
	}

	public static void OpenSkillDatabase()
	{
		if (!skillDatabase.IsOpen ())
		{
			skillDatabase.OpenDatabase ();
		}
	}

	public static void OpenDialogueDatabase()
	{
		if (!dialogueDatabase.IsOpen ())
		{
			dialogueDatabase.OpenDatabase ();
		}
	}

	public static void CloseAllDatabases()
	{
		if (itemDatabase != null)
		{
			itemDatabase.CloseDatabase ();
		}

		if (enemyDatabase != null)
		{
			enemyDatabase.CloseDatabase ();
		}

		if (skillDatabase != null)
		{
			skillDatabase.CloseDatabase ();
		}

		if (dialogueDatabase != null)
		{
			dialogueDatabase.CloseDatabase ();
		}
	}

	public static void LoadSkill(Skill skillToFill)
	{
		skillDatabase.LoadSkill (skillToFill);
	}

	public static void LoadEnemy(EnemyScript enemyToFill) // FIXME: Allow a list of enemies to be parsed at once
	{
		enemyDatabase.LoadEnemy (enemyToFill);
	}

	public static int GetEnemyCourageStat(int enemyId)
	{
		return enemyDatabase.LoadEnemyStat (enemyId, "courage");
	}

	public static void LoadItem(Item itemToFill)
	{
		if (itemToFill is Weapon)
		{
			itemDatabase.LoadWeapon ((Weapon)itemToFill);
		}
		else if (itemToFill is Armour)
		{
			itemDatabase.LoadArmour ((Armour)itemToFill);
		}
		else if (itemToFill is Item)
		{
			itemDatabase.LoadItem (itemToFill);
		}
	}

	public static void LoadConversation(int conversationId, ref Conversation conversationToFill)
	{
		dialogueDatabase.LoadConversation (conversationId, ref conversationToFill);
	}

	public static int FindNextConversationId(int npcId, int lastConversationId)
	{
		return dialogueDatabase.LoadNextConversationId (npcId, lastConversationId);
	}
}

