﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Mono.Data.Sqlite; 
using System.Data;
using System.IO;

public class DialogueDatabase : BaseDatabase
{
	public DialogueDatabase (string filename)
	{
		SetupDatabase (filename);
	}

	public int LoadNextConversationId(int npcId, int lastConversationId)
	{
		string sqlQuery = "SELECT * FROM Conversation WHERE npc_id =" + npcId.ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				if(reader.GetInt32(0) > lastConversationId || 
					(reader.GetInt32(0) == lastConversationId && reader.GetInt32(2) == 0))
				{
					return reader.GetInt32(0);
				}
			}
		}
		catch
		{
			Debug.Log("NPC not found. (id: " + npcId.ToString () + ")");
		}

		return 0;
	}

	public void LoadConversation(int conversationId, ref Conversation conversationToFill)
	{
	 	conversationToFill = null;
		string sqlQuery = "SELECT * FROM Conversation WHERE conversation_id=" + conversationId.ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				//FIXME: Create the Item (Weapon/Armour) object here)
				//int gift = int.Parse(reader.GetString(4).Remove(0, 1)); 
				int gift = 0;
				conversationToFill = new Conversation(conversationId, reader.GetInt32(2) == 1, reader.GetInt32(3),
																	gift, reader.GetInt32(5));

				LoadDialogueList(ref conversationToFill);
			}
		}
		catch
		{
			Debug.Log("Conversation not found. (id: " + conversationId.ToString () + ")");
		}
	}

	private void LoadDialogueList(ref Conversation conversation)
	{
		List<Dialogue> dialogueList = new List<Dialogue>();
		string sqlQuery = "SELECT * FROM Dialogue WHERE conversation_id=" + conversation.GetId().ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				Dialogue newDialogue = new Dialogue(reader.GetInt32(1), reader.GetString(2),
					"-1", "-1");

				dialogueList.Add(newDialogue);
			}
		}
		catch
		{
			Debug.Log("Dialogue not found. (id: " + conversation.GetId().ToString() + ")");
		}

		// Add the parsed dialogue to the conversation
		conversation.SetDialogue (dialogueList);
	}

	public bool IsConversationUnique(int conversationId)
	{
		string sqlQuery = "SELECT * FROM Conversation WHERE conversation_id=" + conversationId.ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				return reader.GetInt32(3) == 1;
			}
		}
		catch
		{
			Debug.Log("Conversation not found. (id: " + conversationId.ToString () + ")");
		}

		return false;
	}

}

	


