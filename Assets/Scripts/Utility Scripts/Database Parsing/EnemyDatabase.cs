﻿using System;
using UnityEngine;

public class EnemyDatabase : BaseDatabase
{
	public EnemyDatabase (string filename)
	{
		SetupDatabase (filename);
	}

	public int LoadEnemyStat(int enemyId, string statName)
	{
		string sqlQuery = "SELECT " + statName + " FROM Enemies WHERE id=" + enemyId.ToString();
		int statValue = -1;
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				try
				{
					statValue = reader.GetInt32(0);
				}
				catch
				{
					Debug.Log("Stat type mismatched.");
				}
			}
		}
		catch
		{
			Debug.Log("Enemy stat could not be found.");
		}

		return statValue;
	}

	public void LoadEnemy(EnemyScript enemyToFill)
	{
		string sqlQuery = "SELECT * FROM Enemies WHERE id=" + enemyToFill.GetId().ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				enemyToFill.SetName(reader.GetString(1));
				enemyToFill.SetIsBoss(reader.GetInt32(2) == 1);
				enemyToFill.SetMaxHP(reader.GetInt32(3));
				enemyToFill.SetMaxMP(reader.GetInt32(4));
				enemyToFill.SetCurrentHP(enemyToFill.GetMaxHP());
				enemyToFill.SetCurrentMP(enemyToFill.GetMaxMP());
				enemyToFill.SetTargetHP(enemyToFill.GetMaxHP());
				enemyToFill.SetTargetMP(enemyToFill.GetMaxMP());
				enemyToFill.SetWeakElement((Element)System.Enum.Parse(typeof(Element), reader.GetString(5)));
				// FIXME: Add a strong element
				// enemyToFill.SetStrongElement((Element)System.Enum.Parse(typeof(Element), reader.GetString(6)));
				enemyToFill.SetAttack(reader.GetInt32(7));
				enemyToFill.SetMagicAttack(reader.GetInt32(8));
				enemyToFill.SetDefence(reader.GetInt32(9));
				enemyToFill.SetMagicDefence(reader.GetInt32(10));
				enemyToFill.SetSpeed(reader.GetInt32(11));
				enemyToFill.SetLuck(reader.GetInt32(12));
				enemyToFill.SetAccuracy(reader.GetInt32(13));
				enemyToFill.SetEvasion(reader.GetInt32(14));
				enemyToFill.SetCourage(reader.GetInt32(15));
				enemyToFill.SetAttackType((EnemyAttackType)System.Enum.Parse(typeof(EnemyAttackType), reader.GetString(16)));
				enemyToFill.SetGoldDrop(reader.GetInt32(17));
				enemyToFill.SetTotalExperiencePoints(reader.GetInt32(18));
				enemyToFill.SetItemDrop(reader.GetInt32(19), reader.GetInt32(20));
				enemyToFill.SetItemSteal(reader.GetInt32(21), reader.GetInt32(22));

				string skillString = reader.GetString(23);
				if(!skillString.Equals("\"\""))
				{
					string[] idArray = reader.GetString(23).Split(',');
					for(int i = 0; i < idArray.Length; i++)
					{
						enemyToFill.AddSkillToList(new Skill(int.Parse(idArray[i]), SkillType.EnemySkills));
					}
				}

				Debug.Log("Enemy found: " + enemyToFill.GetName());
			}

			CloseReader();
		}
		catch
		{
			if (enemyToFill == null)
			{
				Debug.LogError ("Object to be filled was null.");
			}
			else if (reader == null)
			{
				Debug.LogError ("Reader could not be established.");
			}
			else
			{
				Debug.LogError ("Unidentified Enemy Database access error.");
			}

		}
	}
}


