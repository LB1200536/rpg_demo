﻿using System;
using UnityEngine;

public class ItemDatabase : BaseDatabase
{
	public ItemDatabase (string filename)
	{
		SetupDatabase (filename);
	}

	public void LoadItem(Item itemToFill)
	{
		string sqlQuery = "SELECT * FROM Items WHERE item_id=" + itemToFill.GetItemId().ToString();
		//string sqlQuery = "SELECT * FROM Items";
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				itemToFill.SetName(reader.GetString(1));
				itemToFill.SetItemType((ItemType)System.Enum.Parse(typeof(ItemType), reader.GetString(2)));
				itemToFill.SetEffectAmount(reader.GetInt32(3));
				itemToFill.SetKeyState(reader.GetInt32(4) == 1); // If the value is 1, then the item is a key item.
				itemToFill.SetBuyAmount(reader.GetInt32(5));
				itemToFill.SetSellAmount(reader.GetInt32(6));
				itemToFill.SetUsable(reader.GetInt32(7) == 1);
				itemToFill.SetStatus((Status)System.Enum.Parse(typeof(Status), reader.GetString(8)));

				Debug.Log("Item found: " + itemToFill.GetName());
			}

			CloseReader();
		}
		catch
		{
			Debug.LogError ("Item not found in database.");
		}
	}

	public void LoadWeapon(Weapon weaponToFill)
	{
		string sqlQuery = "SELECT * FROM Weapons WHERE id = " + weaponToFill.GetItemId().ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				weaponToFill.SetName(reader.GetString(1));
				weaponToFill.SetWeaponType((WeaponType)System.Enum.Parse(typeof(WeaponType), reader.GetString(2)));
				weaponToFill.SetPhysicalDamage(reader.GetInt32(3));
				weaponToFill.SetMagicalDamage(reader.GetInt32(4)); 
				weaponToFill.SetAccuracy(reader.GetInt32(5));
				weaponToFill.SetBuyAmount(reader.GetInt32(6));
				weaponToFill.SetSellAmount(reader.GetInt32(7));
				weaponToFill.SetElement((Element)System.Enum.Parse(typeof(Element), reader.GetString(8)));
				weaponToFill.SetKeyState(reader.GetInt32(9) == 1);
			}

			CloseReader();
		}
		catch
		{
			Debug.LogError ("Database not found. No calling object assigned.");
		}
	}

	public void LoadArmour(Armour armourToFill)
	{
		string sqlQuery = "SELECT * FROM Armours WHERE id = " + armourToFill.GetItemId().ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				armourToFill.SetName(reader.GetString(1));
				armourToFill.SetArmourType((ArmourType)System.Enum.Parse(typeof(ArmourType), reader.GetString(2)));
				armourToFill.SetPhysicalDefence(reader.GetInt32(3));
				armourToFill.SetMagicalDefence(reader.GetInt32(4)); 
				armourToFill.SetEvasion(reader.GetInt32(5));
				armourToFill.SetElement((Element)System.Enum.Parse(typeof(Element), reader.GetString(6)));
				armourToFill.SetBuyAmount(reader.GetInt32(7));
				armourToFill.SetSellAmount(reader.GetInt32(8));
				armourToFill.SetKeyState(reader.GetInt32(9) == 1);
			}

			CloseReader();
		}
		catch
		{
			Debug.LogError ("Database not found. No calling object assigned.");
		}
	}
}

