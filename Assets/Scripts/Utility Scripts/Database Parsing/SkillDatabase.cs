﻿using System;
using UnityEngine;

public class SkillDatabase : BaseDatabase
{
	public SkillDatabase (string filename)
	{
		SetupDatabase (filename);
	}

	public void LoadSkill(Skill skillToFill)
	{
		string sqlQuery = "SELECT * FROM " + skillToFill.GetSkillType ().ToString () +  " WHERE id=" + skillToFill.GetSkillId().ToString();
		try
		{
			reader = BasicQuery (sqlQuery);
			while (reader.Read())
			{
				skillToFill.SetName(reader.GetString(1));
				skillToFill.SetBaseDamage(reader.GetInt32(2));
				skillToFill.SetElement((Element)System.Enum.Parse(typeof(Element), reader.GetString(3)));
				skillToFill.SetMPCost(reader.GetInt32(4));
				skillToFill.SetAssociatedStatus((Status)System.Enum.Parse(typeof(Status), reader.GetString(5)));
				skillToFill.SetStatusPercentageChance(reader.GetInt32(6));
				skillToFill.SetTargetType((TargetType)System.Enum.Parse(typeof(TargetType), reader.GetString(7)));
				skillToFill.SetUsableInField(reader.GetInt32(8) == 1);

				Debug.Log("Skill found: " + skillToFill.GetName());
			}

			CloseReader();
		}
		catch
		{
			Debug.LogError ("Database not found. No calling object assigned.");
		}
	}
}


