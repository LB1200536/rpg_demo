﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BattleEntranceType {Normal, PreEmptive, SurpriseAttack};
public enum BattlefieldType {GrassyMountain, DarkForest, HouseBattlefield};

public class FieldToBattleDataHolder : SelfCleaningDataTransfer 
{
	private int[] enemyIds;
	private string bgmName;
	private bool isWinnable;
	private bool isRunnable;
	private BattleEntranceType entranceType;
	private string battlefieldName;

	new void Awake()
	{
		base.Awake ();
		enemyIds = new int[0];
		bgmName = "";
		isWinnable = true;
		isRunnable = true;
		entranceType = BattleEntranceType.Normal;
		battlefieldName = "";
	}
	// Use this for initialization
	new void Start () 
	{
		base.Start ();
	}
	
	// Update is called once per frame
	new void Update () 
	{
		base.Update ();
	}

	override protected void CheckForClean()
	{
		if(enemyIds == null && bgmName == null)
		{
			readyToClean = true;
		}
	}

	public int[] GetEnemyIds()
	{
		int[] tempArray = enemyIds;
		enemyIds = null;
		return tempArray;
	}

	public string GetBattlefieldName()
	{
		string tempName = battlefieldName;
		battlefieldName = null;
		return tempName;
	}

	public string GetBGMName()
	{
		string tempName = bgmName;
		bgmName = null;
		return tempName;
	}

	public bool GetIsWinnable()
	{
		bool tempBoolean = isWinnable;
		return tempBoolean;
	}

	public bool GetIsRunnable()
	{
		bool tempBoolean = isRunnable;
		return tempBoolean;
	}

	public BattleEntranceType GetBattleEntranceType()
	{
		BattleEntranceType tempType = entranceType;
		entranceType = 0;
		return tempType;
	}

	public void SetEnemyIds(int[] enemyIds)
	{
		this.enemyIds = enemyIds;
	}

	public void SetBattlefieldName(string battlefieldName)
	{
		this.battlefieldName = battlefieldName;
	}

	public void SetBGMName(string bgmName)
	{
		this.bgmName = bgmName;
	}

	public void SetIsWinnable(bool isWinnable)
	{
		this.isWinnable = isWinnable;
	}

	public void SetIsRunnable(bool isRunnable)
	{
		this.isRunnable = isRunnable;
	}

	public void SetBattleEntranceType(BattleEntranceType entranceType)
	{
		this.entranceType = entranceType;
	}
}
