﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HUDFPS : MonoBehaviour 
{
	public float updateInterval = 0.5f;

	private float accum = 0;
	private int frames = 0;
	private float timeLeft;
	private Text fpsText;

	void Awake()
	{
		if(GameObject.Find("FPS Counter") != this.gameObject)
		{
			Destroy(this.gameObject);
		}
		else
		{
			DontDestroyOnLoad (this);
		}
	}

	// Use this for initialization
	void Start () 
	{
		fpsText = GetComponentInChildren<Text> ();
		if (!fpsText)
		{
			Debug.LogError ("HUDFPS requires a UI Text child component to work as intended.");
			enabled = false;
			return;
		}
		else
		{
			timeLeft = updateInterval;
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		timeLeft -= Time.deltaTime;
		accum += Time.timeScale / Time.deltaTime;
		++frames;

		if (timeLeft <= 0.0f)
		{
			float fps = accum / frames;
			string format = System.String.Format ("{0:F2} FPS", fps);
			fpsText.text = format;

			if (fps < 30)
			{
				fpsText.color = Color.yellow;
			}
			else
			{
				if (fps < 10)
				{
					fpsText.color = Color.red;
				}
				else
				{
					fpsText.color = Color.green;
				}
			}

			timeLeft = updateInterval;
			accum = 0.0f;
			frames = 0;
		}
	}

	public void ToggleDisplay()
	{
		fpsText.enabled = !fpsText.enabled;
	}
}
