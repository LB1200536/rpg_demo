﻿using UnityEngine;
using System.Collections;

public class ModelLoaderScript
{
	public GameObject LoadCharacterModel(string name)
	{
		GameObject returnObject;
		returnObject = Resources.Load ("Models/Characters/" + name) as GameObject;
		if(!returnObject.GetComponent<CharacterScript>())
		{
			returnObject.AddComponent<CharacterScript> ();
		}

		return returnObject;
	}

	public GameObject LoadEnemyModel(string name)
	{
		GameObject returnObject;
		returnObject = Resources.Load ("Models/Enemies/" + name) as GameObject;
		if(!returnObject.GetComponent<EnemyScript>())
		{
			returnObject.AddComponent<EnemyScript> ();
		}
		
		return returnObject;
	}

	public GameObject LoadWeaponModel(string name)
	{
		GameObject returnObject;
		returnObject = Resources.Load ("Models/Weapons/" + name) as GameObject;
		if(!returnObject.GetComponent<WeaponScript>())
		{
			returnObject.AddComponent<WeaponScript> ();
		}
		
		return returnObject;
	}

	public void Destroy()
	{
		Destroy ();
		Resources.UnloadUnusedAssets ();
	}
}
