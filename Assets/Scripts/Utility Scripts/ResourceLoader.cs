﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AssetBundles;

public static class ResourceLoader
{
	public static Sprite LoadSprite(string name)
	{
		return (Sprite)Resources.LoadAsync<Sprite> ("Sprites/" + name).asset;
	}

	public static ParticleSystem LoadAttackParticles(string name)
	{
		return (ParticleSystem)Resources.LoadAsync<ParticleSystem> ("Prefabs/ParticleEffects/Attacks/" + name).asset;
	}

	public static ParticleSystem LoadStatusParticles(string name)
	{
		return (ParticleSystem)Resources.LoadAsync<ParticleSystem> ("Prefabs/ParticleEffects/Status/" + name).asset;
	}

	public static ParticleSystem LoadParticleSystem(string name)
	{
		return (ParticleSystem)Resources.LoadAsync<ParticleSystem> ("Prefabs/ParticleEffects/" + name).asset;
	}

	public static AudioClip LoadAudioBGM(string name)
	{
		return (AudioClip)Resources.LoadAsync<AudioClip> ("Audio/BGM/" + name).asset;
	}

	public static AudioClip LoadMenuSFX(string name)
	{
		return (AudioClip)Resources.LoadAsync<AudioClip> ("Audio/SFX/Menu/" + name).asset;
	}

	public static AudioClip LoadFieldSFX(string name)
	{
		return (AudioClip)Resources.LoadAsync<AudioClip> ("Audio/SFX/Field/" + name).asset;
	}

	public static AudioClip LoadBattleSFX(string name)
	{
		return (AudioClip)Resources.LoadAsync<AudioClip> ("Audio/SFX/Battle/" + name).asset;
	}

	public static GameObject LoadWeapon(string name)
	{
		return (GameObject)Resources.LoadAsync<GameObject> ("Prefabs/Weapons/" + name).asset;
	}

	public static GameObject LoadEnemy(string name)
	{
		return (GameObject)Resources.LoadAsync<GameObject>("Prefabs/Enemies/" + name).asset;
	}

	public static GameObject LoadFieldEnemy(string name)
	{
		return (GameObject)Resources.LoadAsync<GameObject>("Prefabs/FieldEnemies/" + name).asset;
	}

	public static GameObject LoadCharacter(string name)
	{
		return (GameObject)Resources.LoadAsync<GameObject> ("Prefabs/Characters/" + name).asset;
	}

	public static GameObject LoadArea(int areaId)
	{
		return (GameObject)Resources.LoadAsync<GameObject> ("Prefabs/Areas/" + areaId.ToString ()).asset;
	}

	public static GameObject LoadBattlefield(string name)
	{
		return (GameObject)Resources.LoadAsync<GameObject> ("Prefabs/Battlefields/" + name).asset;
	}

	public static Material LoadMaterial(string name)
	{
		return (Material)Resources.LoadAsync("Materials/" + name).asset;
	}

	public static GameObject LoadWorldMap(int worldId)
	{
		return (GameObject)Resources.LoadAsync<GameObject> ("Prefabs/Map/World" + worldId.ToString ()).asset;
	}
}
