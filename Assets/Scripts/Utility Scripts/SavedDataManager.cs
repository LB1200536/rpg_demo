﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;

public static class SavedDataManager
{
	public static void Save(int saveFile)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		FileStream stream = File.Create(Application.persistentDataPath + "/playerData"+saveFile.ToString()+".dat");

		if(PartyDataHolderScript.reference)
		{
			PlayerData data = new PlayerData();
			for(int i = 0; i < PartyDataHolderScript.reference.characters.Count; i++)
			{
				data.characters.Add(PartyDataHolderScript.reference.characters[i].GetSerializable());
			}

			data.inventory = PartyDataHolderScript.reference.inventory;
			data.goldCount = PartyDataHolderScript.reference.goldCount;
			data.areaId = PartyDataHolderScript.reference.areaId;
			data.timeElapsed = PartyDataHolderScript.reference.timeElapsed;
			data.position[0] = PartyDataHolderScript.reference.lastPosition.x;
			data.position[1] = PartyDataHolderScript.reference.lastPosition.y;
			data.position[2] = PartyDataHolderScript.reference.lastPosition.z;
			data.rotation[0] = PartyDataHolderScript.reference.lastRotation.x;
			data.rotation[1] = PartyDataHolderScript.reference.lastRotation.y;
			data.rotation[2] = PartyDataHolderScript.reference.lastRotation.z;
			formatter.Serialize (stream, data);
		}
		else
		{
			Debug.LogError("PartyDataHolder could not be found. Save operation was aborted.");
		}

		stream.Close ();

		SaveFileData (saveFile);
		SaveGameStateData (saveFile);
	}

	public static void LoadPlayerData(int saveFile)
	{
		if(File.Exists(Application.persistentDataPath + "/playerData"+saveFile.ToString()+".dat"))
		{
			if(PartyDataHolderScript.reference)
			{
				BinaryFormatter formatter = new BinaryFormatter();
				FileStream stream = File.Open(Application.persistentDataPath + "/playerData"+saveFile.ToString()+".dat", FileMode.Open);
				PlayerData data = (PlayerData)formatter.Deserialize(stream);
				stream.Close();

				for(int i = 0; i < data.characters.Count; i++)
				{
					PartyDataHolderScript.reference.characters.Add(PartyDataHolderScript.reference.CreateNewBaseCharacter());
					PartyDataHolderScript.reference.characters[i].FillDataWithSerializable(data.characters[i]);
					PartyDataHolderScript.reference.characters[i].gameObject.name = data.characters[i].characterName;
				}

				PartyDataHolderScript.reference.inventory = data.inventory;
				PartyDataHolderScript.reference.goldCount = data.goldCount;
				PartyDataHolderScript.reference.areaId = data.areaId;
				PartyDataHolderScript.reference.timeElapsed = data.timeElapsed;
				PartyDataHolderScript.reference.lastPosition = new Vector3(data.position[0], data.position[1], data.position[2]);
				PartyDataHolderScript.reference.lastRotation = new Vector3(data.rotation[0], data.rotation[1], data.rotation[2]);
			}
			else
			{
				Debug.LogError("PartyDataHolder could not be found. Load operation was aborted.");
			}
		}
	}

	private static void SaveFileData(int saveFile)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		FileStream stream = File.Create(Application.persistentDataPath + "/fileData"+saveFile.ToString()+".dat");

		FileData data = new FileData();
		data.dateSaved = System.DateTime.Now.Date.Day + "/" + System.DateTime.Now.Date.Month.ToString() + "/" + System.DateTime.Now.Date.Year.ToString();
		data.timeSaved = System.DateTime.Now.Hour.ToString() + ":" + System.DateTime.Now.Minute.ToString() + ":" + System.DateTime.Now.Second.ToString();
		data.areaId = PartyDataHolderScript.reference.areaId;
		data.mainCharacterName = PartyDataHolderScript.reference.characters [0].GetName ();
		formatter.Serialize (stream, data);
		stream.Close ();
	}

	private static void SaveGameStateData(int saveFile)
	{
		if(saveFile != -1)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = File.Create(Application.persistentDataPath + "/gameStateData"+saveFile.ToString()+".dat");
			
			GameStateData data = new GameStateData();
			data.alteredAreaList = PartyDataHolderScript.reference.GetCurrentGameStateData().alteredAreaList;
			formatter.Serialize (stream, data);
			stream.Close ();
		}

	}

	public static FileData LoadFileData(int saveFile)
	{
		if(File.Exists(Application.persistentDataPath + "/fileData"+saveFile.ToString()+".dat"))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = File.Open(Application.persistentDataPath + "/fileData"+saveFile.ToString()+".dat", FileMode.Open);
			FileData data = (FileData)formatter.Deserialize(stream);
			stream.Close();

			return data;
		}
		else
		{
			return null;
		}
	}

	public static GameStateData LoadGameStateData(int saveFile)
	{
		if(File.Exists(Application.persistentDataPath + "/gameStateData"+saveFile.ToString()+".dat"))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = File.Open(Application.persistentDataPath + "/gameStateData"+saveFile.ToString()+".dat", FileMode.Open);
			GameStateData data = (GameStateData)formatter.Deserialize(stream);
			stream.Close();
			
			return data;
		}
		else
		{
			return null;
		}
	}

	public static void DeleteSaveFile(int saveFile)
	{
		if(File.Exists(Application.persistentDataPath + "/playerData"+saveFile.ToString()+".dat"))
		{
			File.Delete(Application.persistentDataPath + "/playerData"+saveFile.ToString()+".dat");
		}
		if(File.Exists(Application.persistentDataPath + "/fileData"+saveFile.ToString()+".dat"))
		{
			File.Delete(Application.persistentDataPath + "/fileData"+saveFile.ToString()+".dat");
		}
		if(File.Exists(Application.persistentDataPath + "/gameStateData"+saveFile.ToString()+".dat"))
		{
			File.Delete(Application.persistentDataPath + "/gameStateData"+saveFile.ToString()+".dat");
		}
	}

	public static void DeleteAllData()
	{
		for(int i = 0; i < 10; i++)
		{
			File.Delete(Application.persistentDataPath + "/playerData"+i.ToString()+".dat");
			File.Delete(Application.persistentDataPath + "/fileData"+i.ToString()+".dat");
			File.Delete(Application.persistentDataPath + "/gameStateData"+i.ToString()+".dat");
		}
	}

	public static bool SaveFileExists(int saveFile)
	{
		return File.Exists (Application.persistentDataPath + "/playerData" + saveFile.ToString() + ".dat");
	}

	[System.Serializable]
	private class PlayerData 
	{
		public List<SerializableCharacter> characters = new List<SerializableCharacter>();
		public Inventory inventory;
		public int goldCount;
		public int areaId;
		public float timeElapsed;
		public float[] position = new float[3];
		public float[] rotation = new float[3];
	}
}

[System.Serializable]
public class GameStateData
{
	public GameStateData()
	{
		alteredAreaList = new List<SerializableArea> ();
	}
	public List<SerializableArea> alteredAreaList;
}

[System.Serializable]
public class FileData
{
	public string dateSaved;
	public string timeSaved;
	public int areaId;
	public string mainCharacterName;
}
