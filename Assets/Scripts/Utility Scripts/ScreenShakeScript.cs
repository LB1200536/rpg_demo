﻿using UnityEngine;
using System.Collections;

public class ScreenShakeScript : MonoBehaviour 
{
	private Quaternion originRotation;
	private Vector3 originEuler;
	public float shakeTime;
	public float shakeIntensity;
	public float damping = 1;
	public bool shaking = false;

	// Update is called once per frame
	void LateUpdate () 
	{
		if (shaking) 
		{
			Shake ();
		}
	}

	public void BeginShake()
	{
		shaking = true;
		originRotation = transform.localRotation;
		originEuler = transform.localEulerAngles;
		//shakeIntensity = 0.10f;
		//shakeDecay = 0.01f;
		shakeIntensity = 1.6f * PlayerPrefs.GetFloat("shakeIntensity", 1.0f);
		shakeTime = 0.01f;
		damping = 0.5f;
	}

	public void BeginShake(float intensity)
	{
		shaking = true;
		originRotation = transform.localRotation;
		originEuler = transform.localEulerAngles;
		shakeIntensity = intensity * PlayerPrefs.GetFloat("shakeIntensity", 1.0f);

		damping = 0.75f;
	}

	// Method to update the origin rotation outside of the component. Useful for when the camera moves position, but the 
	// shake effect needs to know where the new center of rotation is.

	public void SetOriginRotation(Quaternion newRotation)
	{
		originRotation = newRotation;
		originEuler = newRotation.eulerAngles;
	}
	
	void Shake()
	{
		if (damping > 0)
		{
			/*
			transform.localRotation = new Quaternion(
				originRotation.x + Random.Range (-shakeIntensity,shakeIntensity) * .2f,
				originRotation.y + Random.Range (-shakeIntensity,shakeIntensity) * .2f,
				originRotation.z + Random.Range (-shakeIntensity,shakeIntensity) * .2f,
				originRotation.w + Random.Range (-shakeIntensity,shakeIntensity) * .2f);
			*/
			transform.localEulerAngles = new Vector3(
				originEuler.x + Random.Range (-shakeIntensity,shakeIntensity) * damping,
				originEuler.y + Random.Range (-shakeIntensity,shakeIntensity) * damping,
				originEuler.z + Random.Range (-shakeIntensity,shakeIntensity) * damping);
			damping -= Time.deltaTime;
			//shakeTime -= Time.deltaTime;
		}
		else
		{
			// Ensure rotation is correct
			transform.localRotation = originRotation;
			shaking = false;
		}
		
	}
}
