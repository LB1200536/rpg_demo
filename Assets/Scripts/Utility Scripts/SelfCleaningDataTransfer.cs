﻿using UnityEngine;
using System.Collections;

// Abstract class with which represents an object which will store data, then null the stored data once retrieved through 
// Get() methods. Once all stored data is nulled (checked with CheckForClean, which must be implemented), the object will 
// Destroy() itself.

public abstract class SelfCleaningDataTransfer : MonoBehaviour 
{
	protected bool readyToClean;

	protected void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
	}

	// Use this for initialization
	protected void Start () 
	{
		readyToClean = false;
	}
	
	// Update is called once per frame
	protected void Update () 
	{
		CheckForClean ();
		if(readyToClean)
		{
			Destroy(this.gameObject);
		}
	}

	protected abstract void CheckForClean ();
}
