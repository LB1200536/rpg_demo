﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Collections;

public class AreaXMLParserScript : BaseXMLParserScript 
{
	public AreaXMLParserScript()
	{
	}
	
	public void ParseArea(Area areaToFill)
	{
		ParseXMLDocument (areaToFill);
	}
	
	public void ParseXMLDocument(Area areaToFill)
	{
		finishedParsing = false;
		CreateXmlReader ("AreaXML.xml");

		using (xmlReader)
		{
			while(!finishedParsing)
			{
				xmlReader.ReadToFollowing("Area"+areaToFill.areaId.ToString());
				xmlReader.MoveToFirstAttribute();

				areaToFill.SetAreaName(xmlReader.GetAttribute("name"));
				areaToFill.SetBGM(xmlReader.GetAttribute("bgm"));
				areaToFill.SetInsideFlag(bool.Parse(xmlReader.GetAttribute("inside")));

				// Parse elements in the following order, due to XML file structure:
				// 1st. EnemyList, 2nd. DoorList, 3rd. CollectableList
				ParseEnemyList(areaToFill);
				ParseDoorList(areaToFill);
				ParseCollectableList(areaToFill);

				finishedParsing = true;
				
				if(xmlReader.EOF)
				{
					finishedParsing = true;
				}
			}
		}
	}

	private void ParseEnemyList(Area areaToFill)
	{
		if(!xmlReader.GetAttribute("enemyIds").Equals(""))
		{
			string[] listStringArray = xmlReader.GetAttribute("enemyIds").Split(']');
			for(int i = 0; i < listStringArray.Length - 1; i++)
			{
				listStringArray[i] = listStringArray[i].Remove(0,1);
				string[] idStringArray = listStringArray[i].Split(',');
				int[] intArray = new int[idStringArray.Length];

				for(int j = 0; j < idStringArray.Length; j++)
				{
					if(j > 3)
					{
						break;
					}

					intArray[j] = int.Parse(idStringArray[j]);
				}

				areaToFill.AddEncounterArrayToList(intArray);
			}
		}
	}

	private void ParseDoorList(Area areaToFill)
	{
		foreach(Door door in areaToFill.GetDoors())
		{
			if(door.doorId != -1)
			{
				xmlReader.ReadToFollowing("Door"+door.doorId.ToString());
				door.SetKeyId(int.Parse(xmlReader.GetAttribute("keyId")));
				door.SetLocked(bool.Parse(xmlReader.GetAttribute("locked")));
			}
		}
	}

	private void ParseCollectableList(Area areaToFill)
	{
		for (int index = 0; index < areaToFill.GetCollectables ().Count; index++)
		{
			xmlReader.ReadToFollowing("Collectable"+index.ToString());
			areaToFill.GetCollectables ()[index].SetCollected(bool.Parse(xmlReader.GetAttribute("collected")));
		}

		/*
		foreach(Collectable collectable in areaToFill.GetCollectables())
		{
			if(collectable.associatedItemId != -1)
			{
				xmlReader.ReadToFollowing("Collectable"+collectable.associatedItemId.ToString());
				collectable.SetCollected(bool.Parse(xmlReader.GetAttribute("collected")));
			}
		}
		*/
	}

	public string GetAreaNameById(int areaId)
	{
		CreateXmlReader ("AreaXML.xml");
		xmlReader.ReadToFollowing("Area"+areaId.ToString());
		return xmlReader.GetAttribute("name");
	}
}
