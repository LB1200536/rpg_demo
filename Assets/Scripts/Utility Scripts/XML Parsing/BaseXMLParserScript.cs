﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections;

public abstract class BaseXMLParserScript
{
	protected XmlDocument baseDocument;
	protected TextAsset textDocument;
	protected XmlReader xmlReader;
	public bool finishedParsing = true;
	
	public BaseXMLParserScript()
	{
		
	}
	
	protected virtual void ParseXMLDocument(string documentName)
	{
		LoadXMLDocument (documentName);
	}
	
	// Method which retrieves the XMLDocument from the Resources folder
	protected void LoadXMLDocument(string xmlName)
	{
		baseDocument = new XmlDocument();
		if(System.IO.File.Exists(GetPath(xmlName)))
		{
			baseDocument.LoadXml(System.IO.File.ReadAllText(GetPath(xmlName)));
		}
		else
		{
			textDocument = (TextAsset)Resources.Load("LevelFiles/"+xmlName, typeof(TextAsset));
			baseDocument.LoadXml(textDocument.text);
		}
	}

	protected virtual void CreateXmlReader(string xmlFileName)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			WWW androidUrlReader = new WWW (GetPath (xmlFileName));
			while(true)
			{
				if(androidUrlReader.isDone)
				{
					break;
				}
			}
			StringReader stringReader = new StringReader (androidUrlReader.text);
			
			xmlReader = XmlReader.Create(stringReader);
		}
		else
		{
			xmlReader = XmlReader.Create(GetPath (xmlFileName));
		}
	}
	
	// Following method is used to retrive the relative path as device platform
	// Taken from - //www.theappguruz.com/blog/unity-xml-parsing-unity#sthash.kteTDU9e.dpuf
	protected string GetPath(string fileName)
	{
		#if UNITY_EDITOR
		//return Application.dataPath + "/StreamingAssets/XMLFiles/"+fileName;
		return Application.streamingAssetsPath + "/XMLFiles/" + fileName;
		#elif UNITY_ANDROID
		return Application.streamingAssetsPath + "/XMLFiles/" + fileName;
		#elif UNITY_IPHONE
		return GetiPhoneDocumentsPath()+ "/Resources/XMLFiles/" +fileName;
		#else
		return Application.streamingAssetsPath + "/XMLFiles/" + fileName;
		#endif
	}
}
