﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Collections;

public class EnemyXMLParserScript : BaseXMLParserScript 
{
	public EnemyXMLParserScript()
	{
	}
	
	public void ParseEnemy(EnemyScript enemyToFill)
	{
		ParseXMLDocument (enemyToFill);
	}
	
	private void ParseXMLDocument(EnemyScript enemyToFill)
	{
		finishedParsing = false;
		
		CreateXmlReader ("EnemyXML.xml");
		using (xmlReader)
		{
			while(!finishedParsing)
			{
				xmlReader.ReadToFollowing("id"+enemyToFill.GetId().ToString());
				xmlReader.MoveToFirstAttribute();

				enemyToFill.SetName(xmlReader.GetAttribute("name"));
				enemyToFill.SetAttackType((EnemyAttackType)System.Enum.Parse(typeof(EnemyAttackType), xmlReader.GetAttribute("attackType")));
				enemyToFill.SetMaxHP(int.Parse(xmlReader.GetAttribute("hp")));
				//enemyToFill.SetMaxHP(1);
				enemyToFill.SetMaxMP(int.Parse(xmlReader.GetAttribute("mp")));
				enemyToFill.SetWeakElement((Element)System.Enum.Parse(typeof(Element), xmlReader.GetAttribute("weakElement")));
				enemyToFill.SetCurrentHP(enemyToFill.GetMaxHP());
				enemyToFill.SetCurrentMP(enemyToFill.GetMaxMP());
				enemyToFill.SetTargetHP(enemyToFill.GetMaxHP());
				enemyToFill.SetTargetMP(enemyToFill.GetMaxMP());
				enemyToFill.SetIsBoss(bool.Parse(xmlReader.GetAttribute("isBoss")));
				enemyToFill.SetAttack(int.Parse(xmlReader.GetAttribute("physicalAttack")));
				enemyToFill.SetMagicAttack(int.Parse(xmlReader.GetAttribute("magicalAttack")));
				enemyToFill.SetDefence(int.Parse(xmlReader.GetAttribute("physicalDefence")));
				enemyToFill.SetMagicDefence(int.Parse(xmlReader.GetAttribute("magicalDefence")));
				enemyToFill.SetSpeed(int.Parse(xmlReader.GetAttribute("speed")));
				enemyToFill.SetLuck(int.Parse (xmlReader.GetAttribute ("luck")));
				enemyToFill.SetAccuracy(int.Parse(xmlReader.GetAttribute("accuracy")));
				enemyToFill.SetEvasion(int.Parse(xmlReader.GetAttribute("evasion")));
				enemyToFill.SetCourage(int.Parse(xmlReader.GetAttribute("courage")));
				enemyToFill.SetGoldDrop(int.Parse(xmlReader.GetAttribute("goldDrop")));
				enemyToFill.SetTotalExperiencePoints(int.Parse(xmlReader.GetAttribute("experienceDrop")));
				enemyToFill.SetItemDrop(int.Parse(xmlReader.GetAttribute("itemDrop")), 
				                        int.Parse(xmlReader.GetAttribute("itemDropRate")));
				enemyToFill.SetItemSteal(int.Parse(xmlReader.GetAttribute("itemSteal")), 
				                         int.Parse(xmlReader.GetAttribute("itemStealRate")));

				if(!xmlReader.GetAttribute("skillIds").Equals(""))
				{
					string[] idArray = xmlReader.GetAttribute("skillIds").Split(',');
					for(int i = 0; i < idArray.Length; i++)
					{
						Skill newSkill = new Skill(int.Parse(idArray[i]), SkillType.EnemySkills);
						enemyToFill.AddSkillToList(newSkill);
					}
				}

				finishedParsing = true;
			}
		}
	}
}
