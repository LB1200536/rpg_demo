﻿using UnityEngine;
using System.Xml;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ItemXMLParserScript : BaseXMLParserScript 
{
	public ItemXMLParserScript()
	{
	}
	
	public void ParseItem(Item itemToFill)
	{
		ParseXMLDocumentForItem (itemToFill);
	}

	private void ParseXMLDocumentForItem(Item itemToFill)
	{
		finishedParsing = false;
		CreateXmlReader ("ItemXML.xml");

		using (xmlReader)
		{
			while(!finishedParsing)
			{
				xmlReader.ReadToFollowing("id"+itemToFill.GetItemId().ToString());
				xmlReader.MoveToFirstAttribute();

				// Standard parameters can be set regardless of the type of item
				itemToFill.SetName(xmlReader.GetAttribute("name"));
				itemToFill.SetBuyAmount(int.Parse(xmlReader.GetAttribute("buyAmount")));
				itemToFill.SetSellAmount(int.Parse(xmlReader.GetAttribute("sellAmount")));
				itemToFill.SetKeyState(bool.Parse(xmlReader.GetAttribute("key")));

				// Fill the rest of the instance, determined by the item type
				if(System.Enum.IsDefined(typeof(ItemType), xmlReader.Value))
				{
					ParseItemObject(itemToFill);
				}
				else if(System.Enum.IsDefined(typeof(WeaponType), xmlReader.Value))
				{
					ParseWeaponObject(((Weapon)itemToFill));
				}
				else if(System.Enum.IsDefined(typeof(ArmourType), xmlReader.Value))
				{
					ParseArmourObject(((Armour)itemToFill));
				}
				else
				{
					Debug.LogError("Could not parse item type during XML parsing.");
				}

				finishedParsing = true;
			}
		}
	}

	private void ParseItemObject(Item itemToFill)
	{
		itemToFill.SetItemType((ItemType)System.Enum.Parse(typeof(ItemType), xmlReader.Value));
		itemToFill.SetEffectAmount(int.Parse(xmlReader.GetAttribute("effectAmount")));
		itemToFill.SetUsable(bool.Parse(xmlReader.GetAttribute("usable")));
		if(xmlReader.GetAttribute("status") != null)
		{
			itemToFill.SetStatus((Status)System.Enum.Parse(typeof(Status), xmlReader.GetAttribute("status")));
		}
	}

	private void ParseWeaponObject(Weapon itemToFill)
	{
		itemToFill.SetWeaponType((WeaponType)System.Enum.Parse(typeof(WeaponType), xmlReader.Value));
		itemToFill.SetPhysicalDamage(int.Parse (xmlReader.GetAttribute("physicalDamage")));
		itemToFill.SetMagicalDamage(int.Parse (xmlReader.GetAttribute("magicalDamage")));
		itemToFill.SetAccuracy(int.Parse (xmlReader.GetAttribute("accuracy")));
		itemToFill.SetElement((Element)System.Enum.Parse(typeof(Element), xmlReader.GetAttribute("element")));
	}

	private void ParseArmourObject(Armour itemToFill)
	{
		itemToFill.SetArmourType((ArmourType)System.Enum.Parse(typeof(ArmourType), xmlReader.Value));
		itemToFill.SetPhysicalDefence(int.Parse (xmlReader.GetAttribute("physicalDefence")));
		itemToFill.SetMagicalDefence(int.Parse (xmlReader.GetAttribute("magicalDefence")));
		itemToFill.SetEvasion(int.Parse (xmlReader.GetAttribute("evasion")));
		itemToFill.SetElement((Element)System.Enum.Parse(typeof(Element), xmlReader.GetAttribute("element")));
	}
}
