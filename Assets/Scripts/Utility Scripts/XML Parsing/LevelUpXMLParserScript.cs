﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections;

public class LevelUpXMLParserScript : BaseXMLParserScript 
{
	string fileName;
	public void ParseNewLevel(BaseCharacter character)
	{
		switch(character.GetSkillType())
		{
		case SkillType.Thievery:
			fileName = "ThiefLevelsXML.xml";
			break;
		case SkillType.Gallantry:
			fileName = "KnightLevelsXML.xml";
			break;
		case SkillType.WhiteMagic:
			fileName = "WhiteMageLevelsXML.xml";
			break;
		case SkillType.BlackMagic:
			fileName = "BlackMageLevelsXML.xml";
			break;
		default:
			fileName = "";
			break;
		}

		if(!fileName.Equals(""))
		{
			ParseXMLDocumentForLevel (character);
		}
	}

	private void ParseXMLDocumentForLevel(BaseCharacter character)
	{
		finishedParsing = false;
		CreateXmlReader (fileName);

		using (xmlReader)
		{
			while(!finishedParsing)
			{
				xmlReader.ReadToFollowing("level"+character.GetCurrentLevel().ToString());
				xmlReader.MoveToFirstAttribute();

				// Apply new stat increases
				character.SetMaxHP(character.GetMaxHP() + int.Parse(xmlReader.GetAttribute("maxHp")));
				character.SetMaxMP(character.GetMaxMP() + int.Parse(xmlReader.GetAttribute("maxMp")));
				character.SetAttack(character.GetAttack() + int.Parse(xmlReader.GetAttribute("attack")));
				character.SetMagicAttack(character.GetMagicAttack() + int.Parse(xmlReader.GetAttribute("magicAttack")));
				character.SetDefence(character.GetDefence() + int.Parse(xmlReader.GetAttribute("defence")));
				character.SetMagicDefence(character.GetMagicDefence() + int.Parse(xmlReader.GetAttribute("magicDefence")));
				character.SetSpeed(character.GetSpeed() + int.Parse(xmlReader.GetAttribute("speed")));
				character.SetLuck (character.GetLuck() + int.Parse(xmlReader.GetAttribute("luck")));

				if(!xmlReader.GetAttribute("newSkillId").Equals(""))
				{
					Skill newSkill = new Skill(int.Parse(xmlReader.GetAttribute("newSkillId")), character.GetSkillType());
					GameObject.FindObjectOfType<NewSkillPopUpScript>().GenerateNewMessage(character.GetName(), newSkill);
					character.AddSkillToList(newSkill);
				}
				
				finishedParsing = true;
			}
		}
	}
}
