﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Collections;

public class SkillXMLParserScript : BaseXMLParserScript 
{
	public SkillXMLParserScript()
	{
	}
	
	public void ParseSkill(Skill skillToFill, SkillType type)
	{
		ParseXMLDocument (skillToFill, type);
	}
	
	public void ParseXMLDocument(Skill skillToFill, SkillType type)
	{
		finishedParsing = false;
		CreateXmlReader ("SkillXML.xml");

		using (xmlReader)
		{
			while(!finishedParsing)
			{
				xmlReader.ReadToFollowing(type.ToString());
				xmlReader.ReadToFollowing("id"+skillToFill.GetSkillId().ToString());
				xmlReader.MoveToFirstAttribute();
				if(System.Enum.IsDefined(typeof(SkillType), xmlReader.Value))
				{
					// Build new Skill object
					skillToFill.SetName(xmlReader.GetAttribute("name"));
					skillToFill.SetBaseDamage(int.Parse(xmlReader.GetAttribute("baseDamage")));
					skillToFill.SetElement((Element)System.Enum.Parse(typeof(Element), xmlReader.GetAttribute("element")));
					skillToFill.SetMPCost(int.Parse(xmlReader.GetAttribute("mpCost")));
					skillToFill.SetAssociatedStatus((Status)System.Enum.Parse(typeof(Status), xmlReader.GetAttribute("status")));
					skillToFill.SetStatusPercentageChance(int.Parse(xmlReader.GetAttribute("statusPercentageChance")));
					skillToFill.SetTargetType((TargetType)System.Enum.Parse(typeof(TargetType), xmlReader.GetAttribute("target")));
					skillToFill.SetUsableInField(bool.Parse(xmlReader.GetAttribute("usableInField")));

					finishedParsing = true;
				}
				else
				{
					Debug.LogError("Could not parse skill type during XML parsing.");
				}
				
				if(xmlReader.EOF)
				{
					finishedParsing = true;
				}
			}
		}
	}
}
